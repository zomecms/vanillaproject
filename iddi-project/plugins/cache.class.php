<?php

class iddiXmlIddi_Cache extends iddiXmlIddiNode{
    var $_key,$_cache_data='';

    function parse(){
        $this->processAVT();
        //Generate a cache key
        if($this->attributes['LEVEL']=='page'){
            //Caching at page level
            if($this->attributes['KEY']){
                $this->_key=base64_encode($_GET['p']).$this->attributes['KEY'];
            }else{
                $this->_key=$_GET['p'].$this->_node_id;
            }
        }else{
            //Otherwise at template level
            $template=$this->owner->_t->filename;
            if($this->attributes['KEY']){
                $this->_key=$this->attributes['KEY'];
            }else{
                $this->_key=base64_encode($template).$this->_node_id;
            }
        }

        if(iddiRequest::$current->getMode()=='edit'){
            //In edit mode we destroy the cache so that it gets regenerated
            //In edit mode we DO NOT regenerate the cache - otherwise it'll have editors attached to it
            iddiCache::flush_item($this->_key);
            return parent::parse();
        }


        $this->_cache_data=iddiCache::get($this->_key,intval($this->attributes['TIMEOUT']));

        if($this->_cache_data=='') return parent::parse();
    }

    function output(){
        if(iddiRequest::$current->getMode()=='edit') return parent::output();
        if($this->_cache_data!='') return $this->_cache_data;

        $output=parent::output();
        iddiCache::save($this->_key, $output,intval($this->attributes['TIMEOUT']*60));
        return $output;
    }
}
