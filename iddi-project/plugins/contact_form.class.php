<?php
/**
 * Generic Contact Form
 *
 * Validates and then sends a contact form
 *
 * Note : Uses a template : emails/contact_form_submission
 *
 * 1. Validation
 * 2. Processing
 *
 * @author Jonathan Patchett
 * @depends email
 */
class iddiXmlIddi_Contact_Form extends iddiXmlIddi_Form_Base{
    const ERROR_BBCODE='bbcode',
          ERROR_LINKS='links';

    var $thank_you_page='/contact/thank_you',
        $customer_email_to_send='/emails/contact-form-thank-you',
        $merchant_email_to_send='/emails/contact-form-submission';

    /**
     * 1. Validation -----------------------------------------------------------
     */

    /**
     * Check that the first name and last name are valid
     */
    function validate_first_name($v){
        $valid=$this->validate_string($v);
        return $valid;
    }

    function validate_last_name($v){
        $valid=$this->validate_string($v);
        return $valid;
    }

    function validate_address1($v){
        return true;
    }

    function validate_address2($v){
        return true;
    }

    function validate_town($v){
        return true;
    }

    function validate_postcode($v){
        return true;
    }

    function validate_comments($v){
        return true;
    }


    /**
     * Check email entries are valid
     */
    function validate_email($v){
        $valid=$this->validate_string($v);
        return $valid;
    }
    function validate_confirm_email($v){
        $valid=$this->validate_string($v);
        return $valid;
    }


    /**
     * Check the message doesn't have anything bad in it
     */
    function validate_message($v){
        //$valid=$this->validate_string($v);
        //if(!$valid) return false;
        //Prevent BBCode / Links
        $link_count+=substr_count($v,'http://');
        if ($link_count>2) $this->add_error(self::ERROR_LINKS);

        if (substr_count($v,'[url')>0) $this->add_error(self::ERROR_BBCODE);
        return true;
    }

    /**
     * Phone number is the last required field
     */
    function validate_telephone($v){
        $valid=$this->validate_string($v);
        return $valid;
    }


    /**
     * Rest of these fields are not required
     */
    function validate_send(){return true;}
    function validate_mobile(){return true;}
    function validate_date_from(){return true;}
    function validate_date_to(){return true;}

    /**
     * 2. Processing -----------------------------------------------------------
     */

    /**
     * Send the email
     */
    function post_process() {
        email::send($this->email, $this->customer_email_to_send, $this);
        email::send('', $this->merchant_email_to_send, $this);
        header('Location: '.$this->thank_you_page);
    }


}