<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Cookie_Info extends iddiXmlIddiNode{
    var $output=false;

    function parse(){
        if(!isset($_COOKIE['cookie-info'])){
            setcookie('cookie-info',1,time()+(3600*24*365));
            $this->output=true;
        }
    }

    function output(){
        if($this->output) return parent::output();
    }
}