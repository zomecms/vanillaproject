<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of form_error
 *
 * @author jpatchett
 */
class iddiXmlIddi_Form_Error extends iddiXmlIddi_If_Base {

    function test_if(){
        $error_form_node=$this->getParentOfType("iddiXmlIddi_Form_Base");
        $code=$this->getAttribute('code');
        if($code==''){
            return (sizeof($error_form_node->errors)>0);
        }else{
            return (isset($error_form_node->errors[$code]));
        }
    }

}