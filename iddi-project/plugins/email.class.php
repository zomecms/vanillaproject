<?php

class Tas_Email{

    static function send($to_user,$message_vf,$data_object=null){
        if($to_user==''){
            $to=iddiConfig::GetValue('contact-details', 'merchant-email');
        }else{
            if(is_object($to_user)){
                $to=$to_user->email;
            }else{
                $to=$to_user;
            }
        }
        if($data_object===null) $data_object=$to_user;

        //Load in the message
        $message_entity=new iddiEntity();
        $message_entity->loadByVirtualFilename($message_vf);

        $message_body=self::insert_data($message_entity->emailcontent,$data_object);
        $message_subject=self::insert_data($message_entity->emailcontenttitle,$data_object);
        $from=iddiConfig::GetValue('contact-details', 'merchant-email');
        $fromname=iddiConfig::GetValue('site', 'site-name');

        $headers="From: \"$fromname\" <$from>\r\nReply-To: \"$fromname\" <$from>\r\nContent-Type: text/html; charset=UTF-8\r\n";
        mail($to, $message_subject, $message_body,$headers);
    }

    static function insert_data($content,$data_object){
        $vars=get_object_vars($data_object);
        $vars['baseurl']='http://'.$_SERVER['SERVER_NAME'];
        foreach($vars as $k=>$v){
            if(is_string($v) || is_numeric($v)){
                $content=str_replace('['.$k.']',$v,$content);
            }
            if(is_array){
                foreach($v as $k2=>$v2){
                    if(is_string($v2) || is_numeric($v2)){
                        $content=str_replace('['.$k2.']',$v2,$content);
                    }
                }
            }
        }
        return $content;
    }

}