<?php

/**
 * The parse method of this class is going to run through the post and look for
 * a method called validate_fieldname for each posted value, that should return
 * true or false.
 *
 * If any given method doesn't exist then the entire submission fails as something
 * has been added to the form that shouldn't be there
 *
 * If any errors are encountered call $this->add_error($fieldname,$message)
 *
 * If the form validates then it will run through looking for process_fieldname
 *
 * This node extends a datasource - all the posted data will be available using this datasource
 * Errors are available as {error_fieldname}
 *
 * If any fields are going to accept HTML you should first call $this->allow_html($fieldname,$tags)
 * otherwise html is automatically stripped
 */
class iddiXmlIddi_Form_Base extends iddiDataSource{
    var $errors=array();
    var $validation=array();
    var $html_fields=array();
    var $bypass_firewall=array();
    var $default_processor='default_processor';

    function parse(){
        if($_POST){
            $this->populate();
            $this->validate();
            if(sizeof($this->errors)==0) $this->valid=true;
            if($this->valid) $this->process();
        }

        $_SESSION['iddi_form_token']=rand(500000,10000000).sha1(rand(2000,800000));
        $_SESSION['iddi_form_sent']=get_class();

        $this->dbfields['iddi_form_token']=$_SESSION['iddi_form_token'];
        $this->iddi_form_token=$_SESSION['iddi_form_token'];

        parent::parse();
    }

    /**
     * Carries out some basic checks on the post
     */
    function firewall($k,$v){
        if(!$this->firewall_email_injection($k, $v)) return false;
        return true;
    }

    function firewall_email_injection($k,$v){
        return true;
    }

    function populate($array=null){
        if($array===null) $array=$_POST;
        foreach($array as $k=>$v){
            if($this->html_fields[$k]){
                $v=strip_tags($v,$this->html_fields[$k]);
            }else{
                $v=strip_tags($v);
            }
            $this->dbfields[$k]=$v;
            if(!isset($this->$k)) $this->$k=$v;
        }
    }

    function pre_validate(){
        return true;
    }

    function validate($array=null){
        $this->valid=true;

        if(!$this->pre_validate()) $this->valid=false;

        if($array===null) $array=$_POST;
        foreach($array as $k=>$v){
            //Run the firewall on the field first
            if(!$this->bypass_firewall[$k]){
                if(!$this->firewall($k, $v)) $this->valid=false;
            }
            //Now run any custom validation
            $validation_method_name='validate_'.$k;
            if(method_exists($this,$validation_method_name)){
                $result=$this->$validation_method_name($v);
                $this->validation[$k]=$result;
                //If the method returned false then the form has failed
                if(!$result){
                    $this->add_error($k);
                    $this->valid=false;
                }
            }else{
                throw new iddiException($k.' is not an expected form field', 'iddi.form_base.unexpected_field');
            }
        }

        if(!$this->post_validate()) $this->valid=false;

        return $this->valid;
    }

    function post_validate(){
        return true;
    }

    function pre_process(){

    }

    function process($array=null){
        if($array===null) $array=$_POST;
        $this->pre_process();
        foreach($array as $k=>$v){
            $process_method_name='process_'.$k;
            if(method_exists($this,$process_method_name)){
                $this->$process_method_name($v);
            }else{
                if($this->default_processor){
                    $process_method_name=$this->default_processor;
                    $this->$process_method_name($k,$v);
                }else{
                    throw new Exception('Method process_'.$k.' is not implemented', 'iddi.form_base.unimplemented_field');
                }
            }
        }
        $this->post_process();
    }

    function default_processor($k,$v){
        //Can't do anything here as we don't know what entity to use
        //if you want to use this you'll need to override it in your form class
    }

    function post_process(){

    }

    function add_error($code){
        if($code!=''){
            $this->valid=false;
            $error_field='error_'.$code;
            $this->errors[$code]=$code;
            $this->dbfields[$error_field]=$code;
            $this->$error_field=$code;
        }
    }

    /**
     * Default validation methods ----------------------------------------------
     */
    function validate_token($v){
        $token=$_SESSION['iddi_form_token'];
        unset($_SESSION['iddi_form_token']);
        return ($v==$token);
    }

    function validate_string($v){
        return (is_string($v) && trim($v)!='');
    }

    function validate_number($v){
        return (is_numeric($v));
    }

    function validate_date($v){
        return (strtotime($v)!=0);
    }

    function validate_email($v){
        return filter_var($v, FILTER_VALIDATE_EMAIL);
    }
}