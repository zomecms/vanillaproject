<?php

//ini_set('display_errors',1);

if($_GET['memcachestatus'] || $_GET['memcachekey']){
    iddiCache::memcache_connect();
    $stats = iddiCache::$_memcache->getExtendedStats();
    die('<pre>'.print_r($stats,1).'</pre>');
}

if($_GET['memcachekey']){
    $data=iddiCache::get($_GET['memcachekey']);
    die();
}

class iddiCache {
    const MEMCACHED=1,MEMCACHE=2,SQL=3,DISK=4,NONE=5;
    static $method=self::NONE;
    static $_local_cache;
    static $_memcache=null;
    static $_items_regenerated=0;

    function __destruct() {
        die('ended');
    }

    static function memcache_connect(){
        self::$_memcache = new Memcache;
        self::$_memcache->pconnect("localhost",11211); # You might need to set "localhost" to "127.0.0.1"
    }

    static function memcached_connect(){
        self::$_memcache = new Memcached;
        self::$_memcache->pconnect("localhost",11211); # You might need to set "localhost" to "127.0.0.1"
    }

    static function save($key,$data,$timeout=6000){
        $key=iddiConfig::GetValue('site', 'short-name').':'.$key;
        switch(self::$method){
            case self::MEMCACHE:
                if(!self::$_memcache) self::memcache_connect();
                self::$_memcache->set($key,serialize($data),false,$timeout);
                break;
            case self::MEMCACHED:
                if(!self::$_memcache) self::memcached_connect();
                self::$_memcache->set($key,serialize($data),false,$timeout);
                break;
            case self::SQL:
                $sql='REPLACE INTO `iddi_cache` (`key`,`data`,`timestamp`) VALUES (\''.mysql_escape_string($key).'\',\''.mysql_escape_string($data).'\',now())';
                iddiMySql::query($sql);
                break;
            case self::DISK:
                return self::save_to_file($key, $data);
                break;
        }
    }
    static function save_to_file($key,$data){
        $filename=IDDI_CACHE_PATH.'/filecache_'.$key;
        $fp=fopen($filename,'w');
        fwrite($fp,$data);
        fclose($fp);
    }

    static function request_cache($key,$data){
        self::$_local_cache[$key]=$data;
    }

    static function flush_item($key){
        if(self::$method==self::MEMCACHE){
            if(!self::$_memcache) self::memcache_connect();
            self::$_memcache->delete($key);
        }
    }

    static function flush_all(){
        if(!self::$_memcache) self::memcache_connect();
        self::$_memcache->flush();
    }
    
    static function get($key,$timeout=0){
        $key=iddiConfig::GetValue('site', 'short-name').':'.$key;
        if(!$_GET['recache']){
            //Don't bother with local caching for memcache as it's just duplicating memory usage
            if(self::$method==self::MEMCACHE){
                if(!self::$_memcache) self::memcache_connect();
                $data=unserialize(self::$_memcache->get($key));
                return $data;
            }

            if(self::$_local_cache[$key]) return self::$_local_cache[$key];

            switch(self::$method){
                case self::SQL:
                    if($timeout==0){
                        $timeout=15;
                    }
                    $sql='SELECT `data` FROM `iddi_cache` WHERE `key`=\''.mysql_escape_string($key).'\'';
                    if(self::$_items_regenerated<25){
                       $sql.=' AND TIMESTAMP>DATE_SUB(NOW(),INTERVAL '.$timeout.' MINUTE)';
                    }
                    $result=iddiMySql::query($sql);
                    if($result->hasData()){
                        foreach($result as $row){
                            $data=$row->data;
                        }
                    }else{
                        self::$_items_regenerated++;
                    }
                    break;
                case self::DISK:
                    $data=self::get_from_file($key,$timeout);
                    break;
            }
            //self::request_cache($key, $data);
        }
        return $data;
    }
    static function get_from_file($key,$timeout=0){
        $filename=IDDI_CACHE_PATH.'/filecache_'.$key;
        if(file_exists($filename)){
            //See if data has timed out if required
            if($timeout>0){
                if(filectime($filename)+($timeout*60) > time()){
                    return file_get_contents($filename);
                }
            }else{
                return file_get_contents($filename);
            }
        }
    }

}
