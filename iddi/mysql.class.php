<?php

   class iddiMySql extends iddiEvents{
       static $default;
       static $_c,$_defer=false,$_qs,$query_count,$db_time;

       const BEFORECONNECT='BeforeConnect';
       const AFTERCONNECT='AfterConnect';
       const CONNECTFAILURE='ConnectFailure';

       var $dbresource;
       var $host;
       var $user;
       var $pass;
       var $dbname;
       var $connected=false;
       var $tableprefix;
       var $connectionname;

       //events
       var $onbuildentitytable;

       static $_cache;

       function iddimysql()
       {
       }

       static function startup(iddiconfig $config)
       {
           $databases=$config->config->databases;

            if ($databases){
               foreach($databases as $d){
                   $db=new iddimysql();
                   $db->connect($d->database->host,$d->database->user,$d->database->pass,$d->database->database,$d->database->name);
                   $db->tableprefix=$d->database->tableprefix;
               }
           }
       }

       static function shutdown(){
          if(self::$_c){
            foreach(self::$_c as $connection){
              $_c->close();
            }
          }
       }

       function connect($host,$user,$pass,$db,$name='') {
           $this->host=strval($host);
           $this->user=strval($user);
           $this->pass=strval($pass);
           $this->dbname=strval($db);
           $this->connectionname=strval($name);
           $this->connected=false;
           if ($this->connectionname=='') self::$default=$this;
           self::$_c[$this->connectionname]=$this;
       }

       function _do_connect(){
           $e=$this->trigger(self::BEFORECONNECT);
           if (!$e->cancelled)
           {
               $this->dbresource=mysql_connect($this->host,$this->user,$this->pass);
               if ($this->dbresource) {
                   if(mysql_select_db($this->dbname)){
                    $this->connected=true;
                    $this->trigger(self::AFTERCONNECT);
                   }else{
                       //attempt to make the database
                       @mysql_query('CREATE DATABASE `'.$this->dbname.'`');
                       if(mysql_select_db($this->dbname)){
                           $this->connected=true;
                           $this->trigger(self::AFTERCONNECT);
                       }else{
                           $this->trigger(self::CONNECTFAILURE);
                           throw new iddiException('Connection to named database failed','iddi.mysql.connect.dbnoexiste');
                       }
                   }
               } else {
                   $this->trigger(self::CONNECTFAILURE);
                   throw new iddiException('Connection to database server','iddi.mysql.connect.failure');
               }
           }
           return $this;
       }

       function close(){
         @mysql_close($this->dbresource);
       }

       static function cache_template($template){
         $data=mysql_escape_string(serialize($template->data));
         $sql="UPDATE {PREFIX}systemplates SET cache='{$data}' WHERE name='{$template->filename}'";
         $d=self::query($sql);
         if($d->hasError){

           die($d->error);
         }

       }

       static function get_template($template_name){
         $r=new iddiMySqlResult();
         $sql="SELECT * FROM {PREFIX}systemplates WHERE name='{$template_name}'";
         $data=self::query($sql);
         if($data->hasData()){
           $template=unserialize($data->getFirstRow()->cache);
           return $template;
         }
       }

       static function get_connection($connectionname=''){
            $connection=($connectionname=='')?self::$default:self::$_c[$connectionname];
            if(!$connection->connected) $connection->_do_connect();
            return $connection;
       }

       /**
       * @desc Runs a query against either the default connection or the named connection if using multiple databases
       * @return iddiMySqlResult
       */
       static function query($sql,$connectionname='',$stoponerror=false)
       {
           $starttime=microtime(true);
           $connection=($connectionname=='')?self::$default:self::$_c[$connectionname];
           if ($connection)
           {
              if(!$connection->connected) $connection->_do_connect();
               $sql=str_replace('{PREFIX}',$connection->tableprefix,$sql);
               //echo "<li>$sql";
               if (self::$_defer){
                    self::$_qs[$sql]=$sql;
               }else{
                   $explain=false;
                   if($explain){
                    echo "\n$sql";
                     if(substr(strtoupper($sql),0,6)=='SELECT'){
                        $result1=@mysql_query('EXPLAIN '.$sql,$connection->dbresource);
                        while($e=mysql_fetch_assoc($result1)){
                            print_r($e);
                        }
                     }
                    $t1=microtime();
                    $result=@mysql_query($sql,$connection->dbresource);
                    $t2=microtime()-$t1;
                    echo "\nDone:".$t2*1000;
                   }else{
                     if(isset(self::$_cache[base64_encode($sql)])){
                       return self::$_cache[base64_encode($sql)];
                     }else{
                        ++self::$query_count;
                        $result=@mysql_query($sql,$connection->dbresource);
                     }
                   }
                    if ($stoponerror && mysql_error($connection->dbresource)){
                        if(iddiRequest::$currentresponse) iddiRequest::$currentresponse->addError(mysql_error($connection->dbresource).' in '.$sql,'iddi.data.mysqlerror','fatal');
                        //die(mysql_error($connection->dbresource).' in '.$sql);
                    }
                    $r=new iddiMySqlResult($result);
                    if(IDDI_USE_QUERY_CACHING) self::$_cache[base64_encode($sql)]=$r;
                    if (mysql_insert_id($connection->dbresource)){
                        $r->isNew=true;
                        $r->newId=mysql_insert_id($connection->dbresource);
                    }
                    if (mysql_error($connection->dbresource)){
                        $r->hasError=true;
                        $r->error=mysql_error($connection->dbresource);
                        //echo $r->error.' in '.$sql;
                    }
                    iddiDebug::dumpvar('Running SQL '.$sql.' See details for result data',$r->dumptable());
                    iddiDebug::dumpvar('Running SQL '.$sql.' See details for result object',$r);
                    if (mysql_error($connection->dbresource)) iddiDebug::dumpvar('Error on previous query '.mysql_error($connection->dbresource),$sql);
                    $endtime=microtime(true);
                    self::$db_time+=($endtime-$starttime);
                    return $r;
               }
           }else{
               throw new iddiException('No database connection','iddi.mysql.nodbconnection');
           }
       }
       static function ProcessQueue(){
           self::$_defer=false;
           foreach(self::$_qs as $sql){
               self::query($sql);
           }
           unset(self::$_qs);
       }
       static function getDbResource($name=''){
           return ($name=='')?self::$default:self::$_c[$name];
       }
       static function loadpagebyvf($invirtualfilename,$targetentity=null,$langslice=true){
           if ($targetentity==null) $targetentity=new iddiPage();
           //Locate the item, then use loadpagebyid to load it

           //First up see if first slice is a language
           $slices=explode('/',$invirtualfilename);

           $sql="SELECT entityname,id FROM `{PREFIX}sysfilenames` WHERE virtualfilename='{$invirtualfilename}'";
           $result=self::query($sql);
           if ($result->hasData()){
               $r=$result->getFirstRow();
               $result->populateEntity($targetentity);
               return self::loadpagebyid($r->entityname,$r->id,$targetentity);
           }else{
               //Atempt to find without the lang switch
               if($langslice){
                 array_shift($slices);
                 array_shift($slices);
                 $newvf='/'.implode('/',$slices);
                 $e=self::loadpagebyvf($newvf,$targetentity,false);
                 return $e;
               }else{
                throw new iddiCodingException('Page '.$invirtualfilename.' Not found','iddi.data.loadpagebyvf.pagenotfound');
               }
           }
       }

       /**
       * @desc Loads the given page by it's ID - Works multilingually by loading the language and the base language and using the base language values for any values that are blank
       * @param string $entityname The entity type to load - i.e. webpage, event
       * @param string $pageid The id of the entity to load
       * @param iddiEntity $targetentity The target entity object to be populated with the result
       * @param string $language The language to load. Leave blank to use the current session language
       * @param int $depth SYSTEM USE : System uses this to prevent recursion going too deep
       * @return iddiEntity returns the entity that has been populated
       */
       static function loadpagebyid($entityname,$pageid,$targetentity=null,$language='',$depth=0){
           if ($pageid=='') throw new iddiException('No Entity ID Provided','iddi.sql.loadPageById.NoEntityId');
           if ($entityname=='') throw new iddiException('No Entity Name Provided','iddi.sql.loadPageById.NoEntityName');
           if ($language=='') $language=iddiRequest::$current->language;
           if ($baselanguage=='') $baselanguage=iddiRequest::$current->baselanguage;
           $entityname=iddiMySql::tidyname($entityname);

           //Ultimate fallback - can't not have a language
           if ($language=='') $language='en-gb';
           if ($baselanguage=='') $baselanguage='en-gb';

/*
           if(self::$_cache[$pageid][$language]){
               return self::$_cache[$pageid][$language];
           }
           self::$_cache[$pageid][$language]=$targetentity;
*/


           if (substr($entityname,0,3)=='sys'){
                $sql="select * from {PREFIX}{$entityname} d where d.id={$pageid} AND entityname='{$entityname}'";
           }else{
                $sql="select d.*,f.virtualfilename,f2.parentid,f.pagetitle,f.language,f.entityname,f.rootlanguageitemid,f2.deleted,f2.odr
                      FROM {PREFIX}{$entityname} d
                      LEFT JOIN {PREFIX}sysfilenames f ON d.id=f.id
                      LEFT JOIN {PREFIX}sysfilenames f2 ON f.rootlanguageitemid=f2.id                       
                      WHERE (d.id={$pageid} OR (f.rootlanguageitemid={$pageid} and f.language='{$language}'))";
                      
                      //echo ($sql);
           }
           $result=self::query($sql);
           if ($result->hasData()){
               $rootitem=$result->rootlanguageitemid;
               if ($rootitem>0 && $result->id!=$rootitem && $depth<10){
                   //If we are looking at a non-root item, load the root item instead (we'll still merge this items data in when we do that)
                   return self::loadpagebyid($entityname,$rootitem,$targetentity,$language,$depth+1);
               }else{
                  foreach($result as $r){
                    $result->populateEntity($targetentity,true,true);
                  }
               }
               if($targetentity->rootlanguageitemid==0){
                 $targetentity->rootlanguageitemid=$targetentity->id;
                 $targetentity->rawDbData['rootlanguageitemid']= $targetentity->rootlanguageitemid;
               }
           }else{
              throw new iddiException('Entity '.$pageid.' of type '.$entityname.' Not found','iddi.data.loadpagebyid.entitynotfound');
           }

           if($targetentity->deleted>0){
              throw new iddiException('Entity '.$pageid.' of type '.$entityname.' is deleted','iddi.data.loadpagebyid.entitydeleted');
           }

           /* We need three different filenames from this:
            * virtualfilename = whatever is in the database
            * basefilename = whatever is in the database, but without the language
            * link = the current language + basefilename
            */

           //First up see if first slice is a language
           $slices=explode('/',$targetentity->virtualfilename);
           $langslice=false;

           //Get the basefilename - If the first slice of virtualfilename is a language, remove it
           if(file_exists(IDDI_PROJECT_PATH.'/languages/'.$slices[1].'.xml')){
             //Loose the first two slices (slice 0 is empty because of the initial /)
             array_shift($slices);
             array_shift($slices);
             $targetentity->basefilename='/'.implode('/',$slices);
           }else{
             $targetentity->basefilename=$targetentity->virtualfilename;
           }
           //Get the link url - add the language unless it's the base language
           if(iddiRequest::$current->language!=iddiRequest::$current->baselanguage){
             $targetentity->link='/'.iddiRequest::$current->language.$targetentity->basefilename;
           }else{
             $targetentity->link=$targetentity->basefilename;
           }

           //Store the values so they can be used by the xpath system
           $targetentity->rawDbData['virtualfilename']=$targetentity->virtualfilename;
           $targetentity->rawDbData['basefilename']=$targetentity->basefilename;
           $targetentity->rawDbData['link']=$targetentity->link;
           $targetentity->dbfields['basefilename']=$targetentity->basefilename;
           $targetentity->dbfields['link']=$targetentity->link;



           return $targetentity;
       }

       static function load_model_by_id($entity_name,$entity_id,$target_entity=null,$language='',$depth=0){
           if ($entity_id=='') throw new iddiException('No Entity ID Provided','iddi.sql.loadPageById.NoEntityId');
           if ($entity_name=='') throw new iddiException('No Entity Name Provided','iddi.sql.loadPageById.NoEntityName');
           if ($language=='') $language=iddiRequest::$current->language;
           if ($baselanguage=='') $baselanguage=iddiRequest::$current->baselanguage;
           $entityname=iddiMySql::tidyname($entityname);

           //Ultimate fallback - can't not have a language
           if ($language=='') $language='en-gb';
           if ($baselanguage=='') $baselanguage='en-gb';

            $sql="select * from {PREFIX}{$entity_name} d WHERE d.id={$entity_id}";

            if($target_entity===null) $target_entity=new iddiEntity();
            
           $result=self::query($sql);
           if ($result->hasData()){
                foreach($result as $r){
                  $result->populateEntity($target_entity,true,true);
                }
           }else{
              throw new iddiException('Entity '.$pageid.' of type '.$entityname.' Not found','iddi.data.loadpagebyid.entitynotfound');
           }

           if($target_entity->deleted>0){
              throw new iddiException('Entity '.$pageid.' of type '.$entityname.' is deleted','iddi.data.loadpagebyid.entitydeleted');
           }

           return $target_entity;
       }



       static function getEntityType($id){
           $id=intval($id);
           $sql='SELECT entityname FROM {PREFIX}sysfilenames WHERE id='.$id;
           $result=self::query($sql);
           if ($result->hasData()){
               return $result->getFirstRow()->entityname;
           }else{
               throw new iddiException('Entity '.$id.' Not found','iddi.data.getEntityType.entitynotfound');
           }
       }

       static function get_entity_by_id($entityname,$pageid,$targetentity=null,$language=null){

       }

        /**
        * @desc This is the main save method - really only for internal use, it takes posted data and inserts it into the tables - This method actually reads data from the session - you cannot supply data to this method. This is the secure posting method used by the core IDDI system using field aliases that get looked up when posted, hiding any field information from would-be hackers. If you need to save your own entities, just create an entity object and run the save method on those, or just use good old php mysql extension or mysqli extension.
        * @version 2
        */
        static function saveFromPost(){
             $overall_changed=false;
             $p=iddirequest::getform();
             //Create a multidimensional lookup array grouped by entity name and then by entity id so we can process each posted entity seperately
             foreach($p->values as $value) $entities[$value->entityname][$value->entityid]=$value->entityname;
             foreach($entities as $entityname=>$entityitems)
             {
                 //Now run through each entity of this type
                 foreach($entityitems as $entityid=>$entityname){
                     $changed=false;
                     //Start a new array of values to send to the database
                     $data=array();
                     //Go through all posted values
                     foreach($p->values as $value)
                     {
                         //Fetch out the ones that are for this entity
                         if ($value->entityname==$entityname && $value->entityid==$entityid && $value->changed)
                         {
                             if (iddi::$debug) iddiDebug::dumpvar("Changed {$entityid}",$value);
                             $changed=true;
                             $overall_changed=true;
                             $value->changed=false;
                             $fieldname=self::tidyname($value->fieldname);
                             $rawvalue=$value->value;

                             //Populate the data array
                             $data[$fieldname]=$rawvalue;
                         }
                     }
                     if (iddi::$debug) iddiDebug::dumpvar("Updating entity {$entityname} {$entityid}",$data);
                     $data['id']=$entityid;
                     //Create an entity, set the data and save it
                     //$ent=new iddiEntity();
                     $ent=iddiRequest::$current->getDataSource($entityid);
                     if($changed && $entityname!=''){
                         if($entityid>0) $ent->loadById($entityname,$entityid);
                         $ent->entityname=$entityname;
                         $ent->entityid=$entityid;
                         $ent->setData($data);
                         try{
                            $ent->save();
                         }catch(Exception $e){}
                     }
                 }
             }
            //Finally check that all data is accessible
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET rootlanguageitemid=id WHERE rootlanguageitemid is null');
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET rootlanguageitemid=id WHERE rootlanguageitemid=0');
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET deleted=0 WHERE deleted is null');
             if (!$overall_changed) iddiRequest::$currentresponse->addError('	','iddi.data.saved','warning');
        }

        static function saveEntity(iddiDataSource $entity){
            if(iddi::$debug) iddiDebug::dumpvar('Saving Entity '.$entity->id.'. Details contain entity',$entity);
            $entity->modified=date('Y-m-d H:i:s',time());
            $entity->moduser=iddiUser::$current_user->id;
            if($entity->created==''){
              $entity->created=date('Y-m-d H:i:s',time());
              $entity->owner=iddiUser::$current_user->id;
            }
            $data=$entity->getAllFields();
            //Negative id's need to be cleared so that a new id can be assigned
            if($data['id']<0) $data['id']='';
            //Remove certain values that only exist on root items
            if($data['language']!='en-gb'){
              $data['parentid']=0;
            }
            if ($entity->virtualfilename){
                //Get a hook to the filenames table
                $tblfilenames=new iddiMySqlEntityTable('sysfilenames');
                iddiDebug::dumpvar('Saving Data to filenames table',$data);
                $r=$tblfilenames->saveData($data);
                //If we have created a new item, we need to pass this onto the entity table
                if ($r->isNew){
                  $data['id']=$r->newId;
                  //$data['rootlanguageitemid']=$data['id'];
                  //$tblfilenames->saveData($data);
                  if($id>0) mysql_query('UPDATE iddi_sysfilenames SET rootlanguageitemid='.$r->newId.' WHERE id='.$r->newId.' AND rootlanguageitemid=0');
                }
            }
            iddiDebug::dumpvar('Saving Data to entity table',$data);
            //Load the table definition in
            $entitytable=new iddiMySqlEntityTable($entity->entityname);
            $r2=$entitytable->saveData($data);
            $entity->id=mysql_insert_id();
            if(iddiRequest::$currentresponse) iddiRequest::$currentresponse->addError('Your work has been saved','iddi.data.saved','Success');
            try{
              self::saveEntityIndexes($entity);
            }catch(Exception $e){
            }
            //Finally check that all data is accessible
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET rootlanguageitemid=id WHERE rootlanguageitemid is null');
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET rootlanguageitemid=id WHERE rootlanguageitemid=0');
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET deleted=0 WHERE deleted is null');
        }

        public function save_model(iddiDataSource $entity){
            if(iddi::$debug) iddiDebug::dumpvar('Saving Entity '.$entity->id.'. Details contain entity',$entity);
            $entity->modified=date('Y-m-d H:i:s',time());
            $entity->moduser=iddiUser::$current_user->id;
            if($entity->created==''){
              $entity->created=date('Y-m-d H:i:s',time());
              $entity->owner=iddiUser::$current_user->id;
            }
            $data=$entity->getAllFields();
            //Negative id's need to be cleared so that a new id can be assigned
            if($data['id']<0) $data['id']='';
            //Remove certain values that only exist on root items
            if($data['language']!='en-gb'){
              $data['parentid']=0;
            }
            iddiDebug::dumpvar('Saving Data to entity table',$data);
            //Load the table definition in
            $entitytable=new iddiMySqlEntityTable($entity->entityname);
            $r2=$entitytable->saveData($data);
            $entity->id=mysql_insert_id();
            if(iddiRequest::$currentresponse) iddiRequest::$currentresponse->addError('Your work has been saved','iddi.data.saved','Success');
            try{
              self::saveEntityIndexes($entity);
            }catch(Exception $e){
            }
        }

        static function saveEntityIndexes(iddiDataSource $entity){
          foreach($entity->fields as $fieldname=>$v){
            if(is_string($v)){
              $id=$entity->id;
              $entityname=$entity->entityname;
              $v=mysql_escape_string(substr(preg_replace('/[^A-Za-z0-9\s]/','',$v),0,255));
              $sql="REPLACE INTO {PREFIX}sysfullindex (`entityid`,`entityname`,`fieldname`,`value`) VALUES($id,'$entityname','$fieldname','$v')";
              try{
                iddiMySql::query($sql);
              }catch(Exception $e){
              }
            }
          }
        }

       static function savefrompost_001()
       {
             $p=iddirequest::getform();
             foreach($p->values as $value) $entities[$value->entityname][$value->entityid]=$value->entityname;
             foreach($entities as $entityitems)
             {
                 foreach($entityitems as $entityid=>$entity){
                     $virtualfilename='';
                     $fieldlist='';
                     $valuelist='';
                     $entityname='';
                     $itemchanged=false;
                     $comma='';
                     foreach($p->values as $value)
                     {
                         if ($value->entityname==$entity && $value->entityid==$entityid)
                         {
                             $bypassfield=false;
                            $fieldname=self::tidyname($value->fieldname);
                            $rawvalue=mysql_escape_string($value->value);
                            $outvalue=$rawvalue;
                            if ($fieldname=='id') $bypassfield=true;
                            if ($fieldname=='entityname') $bypassfield=true;
                            if ($fieldname=='name') $bypassfield=true;
                            if ($fieldname=='title') $bypassfield=true;
                            if ($fieldname=='language') $bypassfield=true;
                            if ($fieldname=='rootlanguageitemid') $bypassfield=true;
                            if ($fieldname=='virtualfilename') $bypassfield=true;
                            if ($fieldname=='parentid') $bypassfield=true;
                            if ($fieldname=='parentpageid') $bypassfield=true;
                            if ($fieldname=='pagetitle') $bypassfield=true;
                            if ($fieldname=='odr') $bypassfield=true;
                            if ($value->type!='number'){
                                $outvalue="'{$outvalue}'";
                            }else{
                                if ($outvalue=='') $outvalue='NULL';
                            }
                            if (!$bypassfield){
                                $fieldlist.=$comma."`{$fieldname}`";
                                $valuelist.=$comma.$outvalue;
                                $comma=',';
                            }
                            if ($value->changed) $itemchanged=true;
                            if ($fieldname=='virtualfilename') $virtualfilename=$outvalue;
                            if ($fieldname=='parentpageid') $parentpageid=$outvalue;
                            if ($fieldname=='pagetitle') $title=$outvalue;
                            if ($fieldname=='entityname') $entityname=$outvalue;
                            if ($fieldname=='language') $language=$outvalue;
                            if ($fieldname=='rootlanguageitemid') $rootlanguageitemid=$outvalue;
                            if ($fieldname=='rootlanguageitemid') $odr=$outvalue;
                         }
                         $value->changed=false;
                     }
                     $entityname=strtolower(preg_replace('/[^A-Za-z0-9\-_]*/','',$entity));
                     $tablename=iddiMySql::$default->tableprefix.$entityname;
                     $itemchanged=true;
                     if ($itemchanged){
                         //Store in the file lookup
                         if ($virtualfilename!=''){
                             $sql='SELECT id FROM `'.iddiMySql::$default->tableprefix.'sysfilenames` WHERE `virtualfilename`='.$virtualfilename;
                             $r=self::query($sql);
                             $existingid='NULL';
                             if ($r->HasData()){
                                 foreach($r as $row){
                                    $existingid=$row->id;
                                 }
                             }
                            $sql='REPLACE INTO `'.iddiMySql::$default->tableprefix."sysfilenames` (`id`,`virtualfilename`,`entityname`,`entityid`,`title`,`parentid`,`language`,`rootlanguageitemid`,`odr`) VALUES({$existingid},{$virtualfilename},'{$entityname}',$entityid,$title,$parentpageid,$language,$rootlanguageitemid,$odr)";
                            self::query($sql,'',true);
                            if ($entityid=='' || $entityid<0) {
                                $entityid=mysql_insert_id();
                                $fieldlist.=$comma.'created';
                                $valuelist.=$comma.'now()';
                                $comma=',';
                            }
                            $fieldlist.=$comma.'modified';
                            $valuelist.=$comma.'now()';


                            $sql="REPLACE INTO `".iddiMySql::tidyname($tablename)."` (`id`,{$fieldlist}) VALUES({$entityid},{$valuelist})";
                            self::query($sql,'',true);
                         }
                         $savedsomething=true;
                         if (iddiRequest::$currentresponse) iddiRequest::$currentresponse->addError('Saved '.$entity.' : '.$title,'iddi.data.saved','ok');
                     }
                 }
             }
             if (!$savedsomething && iddiRequest::$currentresponse) iddiRequest::$currentresponse->addError('Nothing to Save','iddi.data.unchanged','ok');
       }

       static function populateTable($tablename,$data){
       }


       static function savePage(iddiPage $page){

           //Put in it's own entity table
           $entityname=strtolower(preg_replace('/[^A-Za-z0-9\-_]*/','',$page->entityname));
           $tablename=iddiMySql::$default->tableprefix.$entityname;
           $virtualfilename=mysql_escape_string($page->virtualfilename);
           $templatefile=mysql_escape_string($page->templatefile);

           //How this works is we load the table def, then populate an sql statement based on the field definitions to ensure that everything goes in correctly.
           $rs=self::query("SHOW FIELDS FROM `{$tablename}`",'',true);



           //Generate the fieldlist for additional fields on the page
           foreach($page->dbfields as $k=>$v){
                if ($k!='virtualfilename' && $k!='templatefile' && $k!='created' && $k!='modified' && $k!='id' && $k!='parentid'){
                    $k=self::tidyname($k);
                    if (!is_numeric($v)) $v="'".mysql_escape_string($v)."'";
                    $fieldlist.=",`{$k}`";
                    $fieldvalues.=",{$v}";
                }
           }
           if ($page->id<1){
                $sql="INSERT INTO `{$tablename}` (`modified`,`created`,`templatefile`,{$fieldlist}) VALUES(now(),now(),'{$templatefile}',{$fieldvalues})";
                self::query($sql,'',true);
                $page->id=mysql_insert_id();
           }else{
                $sql="REPLACE INTO `".iddiMySql::tidyname($tablename)."` (`id`,`modified`,`templatefile`,{$fieldlist}) VALUES({$page->id},now(),'{$templatefile}',{$fieldvalues})";
                self::query($sql,'',true);
           }
           //Store in the file lookup
           if ($virtualfilename!=''){
                $sql='REPLACE INTO `'.iddiMySql::$default->tableprefix."sysfilenames` (`virtualfilename`,`entityname`,`entityid`,`title`,`parentid`) VALUES('{$virtualfilename}','{$entityname}','{$page->id}','{$page->pagetitle}',".intval($page->parentpageid).')';
                self::query($sql,'',true);
           }
           $page->changed=false;
       }
       static function savefromarray($intablename,$inarray){
             $intablename=iddiMySql::$default->tableprefix.$intablename;
             foreach($inarray as $fieldname=>$value)
             {
                $value=mysql_escape_string($value);
                $fieldlist.=$comma."`{$fieldname}`";
                $valuelist.=$comma."'{$value}'";
                $comma=',';
             }
             $sql="REPLACE INTO `".iddiMySql::tidyname($intablename)."` ({$fieldlist}) VALUES({$valuelist})";
             self::query($sql);
       }
       /**
       * @desc Useful static function for sanitising names - mainly used for entity names
       */
       static function tidyname($name) { return strtolower(preg_replace('/[^A-Za-z0-9\-_]/','',$name)); }
       static function buildentitytable(iddiDataSource $entity){
           if (!is_a($entity,'iddiDataSource')) throw new iddiException('First parameter must be an iddiXmlIddi_Entity node object','iddi.database.buildentitytable.notanentity');

           $table=new iddiMySqlEntityTable($entity->getName());

           if ($entity->fields){
               foreach($entity->fields as $k=>$v){
                   $type='';$size='';
                   if ($v->getType()=='text') { $type='VARCHAR'; $size='255'; }
                   if ($v->getType()=='html') $type='TEXT';
                   if ($v->getType()=='date') $type='DATETIME';
                   if ($v->getType()=='number') { $type='INT'; $size='11'; }
                   if ($v->getType()=='image') { $type='VARCHAR'; $size='255'; }
                   if ($v->getType()=='lookup') { $type='INT'; $size='11'; }
                   if ($v->getType()=='bool') { $type='TINYINT'; $size='1'; }

                   if ($v->getAttribute('MAXLENGTH')) $size=$v->getAttribute('MAXLENGTH');

                   //Extra things are : format, group, minlength


                   if ($type!=''){
                       echo "<li>Field $k=$type $size";
                       $f=new iddiMySqlTableField($k,$type,$size,'');
                       $f->minlength=$v->getAttribute('MINLENGTH');
                       $f->group=$v->getAttribute('GROUP');
                       $f->format=$v->getAttribute('FORMAT');
                       $f->lookup=$v->getAttribute('LOOKUP');
                       $f->help=$v->getAttribute('HINT');
                       $f->hidden=($v->getAttribute('HIDDEN')=='hidden')?1:0;
                       $f->caption=$v->getCaption();
                       $f->idditype=$v->getType();
                       $f->entityname=iddiMySql::tidyname($entity->getName());
                       $table->addField($f);
                   }
               }
           }
           $table->build();
       }
       static function deleteentitytable($entityname){
           $table=new iddiMySqlEntityTable($entityname);
           $table->drop();
       }

       //---- DAO Methods ----//

       /**
       * @desc Permanantly deletes an entity
       */
       static function deleteEntity($inEntity){
           //$sql='UPDATE `{PREFIX}sysfilenames` SET deleted=1 WHERE id='.$inEntity->id;
           $sql='DELETE FROM `{PREFIX}sysfilenames` SET deleted=1 WHERE id='.$inEntity->id;
           self::query($sql);
       }

       static function restoreEntity($inEntity){
           $sql='UPDATE `{PREFIX}sysfilenames` SET deleted=null WHERE id='.$inEntity->id;
           self::query($sql);
       }

       /**
       * @desc Obtains an entity definition from the database
       */
       static function getEntityDefinition($entityname){
           $entityname=self::tidyname($entityname);
           $sql="SELECT * from `{PREFIX}sysentityfields` WHERE `entityname`='{$entityname}'";
           $rs=self::query($sql);

           $fieldlist=array();
           foreach($rs as $record){
               $f=new iddiEntityField();
               $rs->populateObject($f);
               $fieldlist[$f->fieldname]=$f;
           }
           return $fieldlist;
       }

       static function getEntityInfo($entityname){
           $entityname=self::tidyname($entityname);
           $sql="SELECT * from `{PREFIX}sysentities` WHERE `tidyname`='{$entityname}'";
           $rs=self::query($sql);
           return $rs->getFirstRow();
       }
   }

   class iddiMySqlTable{ var $_t; var $_f; var $_i, $_gotdef,$_connection;
       function iddiMySqlTable($intablename,$connection=null){
           if ($connection==null) $connection=iddiMySql::$default;
           $prefix=$connection->tableprefix;
           $intablename=$prefix.preg_replace('/[^A-Za-z0-9\-_]*/','',$intablename);
           $this->_t=$intablename;
           $this->_connection=$connection;
       }
       function build(){
         $this->_t=strtolower($this->_t);
           iddiMySql::query('CREATE TABLE `'.$this->_t."` (`id` int(11) NOT NULL auto_increment,
                             PRIMARY KEY  (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8 ");
           if ($this->_f) foreach($this->_f as $loopf) $loopf->build();
           if ($this->_i) foreach($this->_i as $loopi) $loopi->build();
       }
       function drop(){
           iddiMySql::query("DROP TABLE `".$this->_t."`");
       }
       function getName(){return $this->_t;}
       function getFields(){return $this->_f;}
       function addField($infield){$infield->settable($this);$this->_f[]=$infield;return $this;}
       function getIndexes(){return $this->_i;}
       function addIndex($inindex){$inindex->settable($this);$this->_i[]=$inindex;return $this;}
       function getDefinition(){

            if($this->_t=='iddi_') throw new iddiException('No table defined','iddi.mysql.table.getdefinition.no_table');
            $sql="SHOW COLUMNS FROM `".iddiMySql::tidyname($this->_t)."`";
            $result = mysql_query($sql,iddiMySql::get_connection()->dbresource);
            if (!$result)
                throw new iddiException('Could not run query: ' . mysql_error(),'iddi.mysql.table.getdefinition_failed.');

            if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_assoc($result)) {
                    $type=$row['Type'];
                    $size='';
                    if(strstr($type,'(')){
                        $t=explode('(',$type);
                        $b=explode(')',$t[1]);
                        $type=$t[0];
                        $size=$b[0];
                    }
                    $f=new iddiMySqlTableField($row['Field'],$type,$size,$row['Extra']);
                    $f->settable($this);
                    if ($row['Key']=='PRI') $this->primarykey=$f;
                    $this->_f[]=$f;
                }
            }
       }
       /**
       * @desc Saves the data provided using this table as it's definition
       * @param array $data An associative array containing the data as [$fieldname]=$value
       */
       function saveData($data){
           $this->clearRow();
           $this->loadRow($data);
           $r=$this->saveRow();
           $this->clearRow();

           return $r;
       }
       /**
       * @desc Removes an data from this table definition
       */
       function clearRow(){
           if ($this->_f){
               foreach($this->_f as $f){
                   $f->valueset=false;
                   unset($f->value);
               }
           }
       }
       /**
       * @desc Loads $data into the table definition. Ideally you need to call clearRow when you're done
       * @param array $data An associative array containing the data as [$fieldname]=$value
       */
       function loadRow($data){
           if(!$this->_gotdef) $this->getDefinition();
            if ($this->_f){
               foreach($this->_f as $field){
                   $fname=strtolower($field->_f);
                       $field->value=$data[$fname];
                       $field->valueset=true;
               }
            }
       }
       /**
       * @desc Saves the current data loaded into this table definition back to the database
       */
       function saveRow(){
           $sql=$this->buildUpdateSql();
           $result=iddiMySql::query($sql);
           if($result->error) throw new iddiException($result->error.' in '.$sql);
           return $result;
       }

       function buildUpdateSql(){
           foreach($this->_f as $field){
               if ($field->valueset){
                   $value=$field->value;
                   $t=strtoupper($field->_ty);
                   if($t=='TEXT') $value="'".mysql_escape_string(stripslashes($value))."'";
                   if($t=='CHAR' || $t=='VARCHAR') $value="'".mysql_escape_string(strip_tags(stripslashes($value)))."'";
                   if($t=='DATE' || $t=='TIME' || $t=='DATETIME') $value="'".date('Y-m-d H:i:s',strtotime($value))."'";
                   if($t=='INT' || $t=='TINYINT') $value="'".intval($value)."'";

                   $fieldlist.=$comma."`".$field->_f."`";
                   $valuelist.=$comma.$value;

                   $comma=',';
               }
           }
           $sql="REPLACE INTO `".iddiMySql::tidyname($this->_t)."` ($fieldlist) VALUES($valuelist)";
           return $sql;
       }
   }
   class iddiMySqlEntityTable extends iddiMySqlTable{
       function build($sfx=''){
         $this->_t=strtolower($this->_t);
           iddiMySql::query("CREATE TABLE `".$this->_t."$sfx` (`id` int(11) NOT NULL auto_increment,
                                                       `created` datetime default NULL,
                                                       `modified` datetime default NULL,
                                                       `moduser` varchar(17) default NULL,
                                                       `templatefile` varchar(255) default NULL,
                                                       `pagetitle` varchar(255) default NULL,
                                                       `owner` varchar(17) default NULL, PRIMARY KEY  (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ");
           $f1=new iddiMySqlTableField('virtualfilename','varchar','255','');
           $f1->drop();
           $f2=new iddiMySqlTableField('sys_language','VARCHAR',5);
           $f2->drop();
           $f3=new iddiMySqlTableField('sys_rootlanguageitemid','INT',11);
           $f3->drop();
           $f4=new iddiMySqlTableField('parentid','INT',11);
           $f4->drop();
           $f5=new iddiMySqlTableField('pagetitle','VARCHAR',255);
           $f5->drop();
           $this->addField($f1);
           $this->addField($f2);
           $this->addField($f3);
           $this->addField($f4);
           $this->addField($f5);


           if ($this->_f) foreach($this->_f as $loopf) $loopf->build($sfx);
           if ($this->_i) foreach($this->_i as $loopi) $loopi->build($sfx);
           /*if ($sfx==''){
            $this->build('_future');
            $this->build('_history');
            $this->build('_deleted');
           }
           */
       }
   }

   class iddiMySqlTableField{ var $_t; var $_f; var $_ty; var $_s; var $_o;
        function iddiMySqlTableField($infieldname,$intype,$insize='',$inoptions=''){
            $infieldname=preg_replace("/[^A-Za-z0-9\-_ ]*/","",$infieldname);
            $this->_f=$infieldname;
            $this->_ty=$intype;
            $this->_s=$insize;
            $this->_o=$inoptions;
        }
        function settable($t){$this->_t=$t;return $this;}
        function build($sfx){
            $s=($this->_s=='')?'':'('.$this->_s.')';
            if ($this->_drop){
                iddiMySql::query("ALTER TABLE `".$this->_t->getName().$sfx.'` DROP COLUMN `'.$this->_f.'`');
            }else{
                iddiMySql::query("ALTER TABLE `".$this->_t->getName().$sfx.'` ADD COLUMN `'.$this->_f.'` '.$this->_ty.$s.' '.$this->_o);
                iddiMySql::query("ALTER TABLE `".$this->_t->getName().$sfx.'` MODIFY `'.$this->_f.'` '.$this->_ty.$s.' '.$this->_o);
                //Add to the entityfield table
                $minlength=($this->minlength)?$this->minlength:0;
                $maxlength=($this->_s)?$this->_s:0;
                $fieldname=iddiMySql::tidyname($this->_f);
                $bsq="REPLACE INTO `{PREFIX}sysentityfields` (`entityname`,`fieldname`,`minlength`,`maxlength`,`format`,`type`,`group`,`hidden`,`lookup`,`help`,`caption`)
                                                                   VALUES('".iddiMySql::tidyname($this->entityname)."','$fieldname',
                                                                          $minlength,$maxlength,'$format','$this->idditype','$this->group',$this->hidden,'$this->lookup','$this->help','$this->caption')";
                //echo "<li>$bsq";
                iddiMySql::query($bsq,'',true);
            }
            if ($this->_newname){
                iddiMySql::query("ALTER TABLE `".$this->_t->getName().$sfx.'` CHANGE `'.$this->_f.'` `'.$this->_newname.'` '.$this->_ty.$s.' '.$this->_o);
            }
        }
        function drop(){
            $this->_drop=true;
        }
        function changename($newname){
            $this->_newname=$newname;
        }
   }
   class iddiMySqlIndex{
        function settable($t){$this->_t=$t;return $this;}
   }
   class iddiMySqlUniqueKey extends iddiMySqlIndex{
       var $_t;
       var $_n;
       var $_f;
       function iddiMySqlUniqueKey($inkeyname,$infieldlist){
           $this->_n=$inkeyname;
           $this->_f=$infieldlist;
       }
       function build($sfx=''){
           if (is_array($this->_f))
           {
              $sql="ALTER TABLE `".$this->_t->getName().$sfx.'` ADD UNIQUE KEY `'.$this->_n.'` (';
              foreach($this->_f as $f) {
                  $sql.=$comma.'`'.$f.'`';
                  $comma=',';
              }
              $sql.=')';
           }
           else
           {
              $sql="ALTER TABLE `".$this->_t->getName().$sfx.'` ADD UNIQUE KEY `'.$this->_n.'` (`'.$this->_f.'`)';
           }
           iddiMySql::query($sql);
       }
   }

    class iddiMySqlResult extends iddiDataSource implements Iterator
    {
        protected $_resultset;
        protected $_currentposition=0;
        protected $_currentrow;
        protected $_pagelength=1000000;
        protected $_currentpage;
        protected $_stopatendofpages=true;
        protected $_currentrowdata;

        var $id;
        var $keepafteruse=false;
        var $isNew=false,$newId=null;

        public function __construct($in_resultset=null){if ($in_resultset) $this->LoadResultSet($in_resultset);}
        public function LoadResultSet($in_resultset){
            $this->_resultset=$in_resultset;
            $this->rewind();
            if($this->_currentrowdata['entityname']) $this->entityname=$this->_currentrowdata['entityname'];
        }
        public function rewind(){ if($this->_resultset) { if (mysql_num_rows($this->_resultset) > 0) $this->GetRow(0); return $this;}}
        /**
        * @desc Returns the current record - or a field value from the current record if a fieldname is supplied
        * @param string $fieldname Optional field name to return instead of the entire record
        */
        public function current($fieldname=''){
            if ($this->_resultset){
                if ($fieldname!=''){
                    return $this->_currentrow->$fieldname;
                }else{
                    return $this->_currentrow;
                }
            }
        }
        public function key(){if ($this->_currentrow) return $this->_currentrow->id;}
        public function next(){if ($this->_resultset) return $this->GetRow();}
        public function valid(){if ($this->_resultset) return $this->current() !== null;}
        public function count(){return ($this->_resultset)?mysql_num_rows($this->_resultset):0;}
        public function item($id){return $this->GetColumn($id);}
        public function SetPageLength($length){$this->_pagelength=$length;}
        public function HasData(){return $this->count() > 0;}
        function getFirstRow(){return $this->GetRow(0);}
        public function JumpTopage($page){
            $this->_currentpage=$page-1;
            $this->rewind();
        }
        public function NextPage(){
            $this->_currentpage++;
            $this->rewind();
            return $this;
        }
        public function PreviousPage(){
            if ($this->_currentpage > 0){
                $this->_currentpage--;
                $this->rewind();
                return $this;
            }else{
                return false;
            }
        }
        public function FirstPage(){
            $this->_currentpage=0;
            $this->rewind();
        }
        public function get_pos(){ return $this->_currentposition; }
        public function get_page(){ return $this->_currentpage; }
        private function GetRow($gcolnumber=NULL){
            if (is_numeric($gcolnumber)){
                $gcolnumber=($this->_currentpage * $this->_pagelength) + $gcolnumber;
                mysql_data_seek($this->_resultset,$gcolnumber);
                $this->_currentposition=$gcolnumber;
            }

            if ($this->_currentposition <= $this->_pagelength || $this->_stopatendofpages==false){
                $this->_currentrowdata=mysql_fetch_assoc($this->_resultset);
                if ($this->_currentrowdata){
                    $this->_currentrow=$this->toEntity();
                    $this->_currentposition++;
                }else{
                    $this->_currentrow=null;
                }
            }else{
                $this->_currentrow=null;
            }
            return $this->_currentrow;
        }
        function toEntity(){
            $entity=($this->entityname)?'iddiEntity_'.$this->entityname:'iddiMySqlRow';
            if (!class_exists($entity)) $entity='iddiMySqlRow';
            $e=new $entity;
            $this->populateEntity($e,true,true);
            $e->recordset=$this;
            return $e;
        }
        /**
        * @desc Populates the provided entity with the contents of the current record
        * @param iddiDataSource $entity The entity to populate
        * @param bool $buildentityvars Set to true to build the entity variables
        * @param bool $mergevalues Set to true to merge the values with those already in the entity - i.e. do not overwrite existing values in the entity
        */
        function populateEntity($entity,$buildentityvars=false,$mergevalues=false){
            if($this->_currentrowdata) foreach($this->_currentrowdata as $k=>$v){
                $k=strtolower($k);
                $v=str_replace("&gt;",">",$v);
                $v=str_replace("&lt;","<",$v);
                $v=str_replace("&newline;","\n",$v);
                $v=stripslashes(urldecode($v));

                //$v=str_replace("%u","&#",$v);
                if($k[0]!='_'){
                  if($v!='' || $mergevalues==false){
                      $entity->setDbValue($k,$v,$buildentityvars);
                      $entity->rawDbData[$k]=$v;
                  }
                }
            }
        }

        function populateObject($object){
            if($this->_currentrowdata) foreach($this->_currentrowdata as $k=>$v){
                $k=iddiMySql::tidyname($k);
                $k=strtolower($k);
                $v=str_replace("&gt;",">",$v);
                $v=str_replace("&lt;","<",$v);
                $v=str_replace("&newline;","\n",$v);
                //$v=str_replace("%u","&#",$v);
                $v=stripslashes(urldecode($v));
                $object->$k=$v;
            }
        }
        function dumpTable(){
            $r=$this->getFirstRow();
            $output='<style>td {valign:top;max-height:90px;overflow:scroll;}</style><table border="1"><tr><th>#</th>';

            foreach($this->_currentrowdata as $k=>$v){
                $output.="<th>$k</th>";
            }
            $output.="</tr>";
            $p=1;
            foreach($this as $row){
                $output.="<tr><td>$p</td>";
                foreach($this->_currentrowdata as $k=>$v){
                    $output.="<td>$v</td>";
                }
                $output.="</tr>";
                ++$p;
            }
            $output.="</table>";
            $this->rewind();
            return $output;
        }
    }

    class iddiMySqlRow extends iddiDataSource{
            var $recordset,$rawDbData;
            function __destruct(){
                $this->recordset=null;
            }
          function xpathinternal($strxpath){
            $resultset=new iddiXpathResultSet();

            $xpath=new iddiXpath($strxpath);


          //Get the nodes to work on from the selected axis
            $axisdata=$this->getAxisData($xpath);

            //Process the nodes in the axis to see which ones match our selector and any predicates
            if ($xpath->functionname && $xpath->nodetest!='node()'){
                $fn=$xpath->classfunctionname;
                if ($method_target=$this->getMethod($fn)){
                    $thisresultset=$method_target->$fn(explode(',',$xpath->functionparams),$xpath);
                    //Process the items in the resultset
                     if ($xpath->nextbit==''){
                        //End of the line, so add to result set
                        $resultset->addResults($thisresultset);
                    } else {
                        //More processing, so pass the xslt on
                        foreach($thisresultset as $result){
                            if (method_exists($result,'xpathinternal')) $result->xpathinternal($xpath->nextbit,$includeiddinamespace,$resultset);
                        }
                    }
                }else{
                    throw new iddiException("Function $fn Not supported on ".get_class($this),"iddi.xml.functionnotsupported",$this);
                }
            }else{
                if ($axisdata){
                    foreach($axisdata as $axisnode){
                        if ($xpath->nodetest==$axisnode->nodename || $xpath->nodetest=='*' || $xpath->nodetest=='node()'){
                            //Check the predicates
                            if ($xpath->nodepredicates=='' || $axisnode->processPredicates($xpath->nodepredicates)){
                                if ($xpath->nextbit==''){

                                        //End of the line, so add to result set
                                        $resultset->addResult($axisnode);
                                } else {
                                    //More processing, so pass the xslt on
                                    if (method_exists($axisnode,'xpathinternal')) $axisnode->xpathinternal($xpath->nextbit,$includeiddinamespace,$resultset);
                                }
                            }
                        }
                    }
                }
            }
            return $resultset;
      }

      function getAxisData($xpath){
            if(substr($xpath->nodetest,0,1)=='$'){
                //Not supported
            }else{
                switch($xpath->axis){
                    case "child":$axisdata=$this->getChildren();break;
                    case "self":$axisdata[]=$this;break;
                    case "parent":$axisdata[]=$this->recordset;break;
                    case "attribute":$axisdata=$this->pchildren;break;
                    case "descendant"://not supported
                    case "descendant-or-self":
                    case "ancestor":
                    case "ancestor-or-self":
                    case "following-preceding":
                    case "preceeding-sibling":
                    case "following":
                    case "preceeding":
                    default:throw new iddiException('XPath Axis '.$thisaxis.' not supported','iddi.mysqlrow.xpath.unsupportedaxis',$this);
                }
            }
            return $axisdata;
        }
        function processPredicates($predicates){
            return true;
        }
        function getChildren(){
            foreach($this->rawDbData as $f=>$v){
                $p=new iddiMySqlRowValue($this->recordset,$f,$this,null,$v);
                $r[]=$p;
            }
            return $r;
        }
        function expandData($id=null){
            if ($this->entityid){
              if ($this->entityid != $this->id){
                  return iddimysql::loadpagebyid($this->entityname,$this->entityid,$this);
              }else{
                  return iddimysql::loadpagebyid($this->entityname,$this->id,$e);
              }
            }else{
                return $this;
            }
        }

      function xmlFunction_pos($params){
        return iddiXpathResultSet::quickResult('pos',$this->recordset->get_pos());
      }

      function xmlFunction_count($params){
        return iddiXpathResultSet::quickResult('count',$this->recordset->count());
      }


    }
    class iddiMySqlRowValue extends iddiXmlCData{
    }
