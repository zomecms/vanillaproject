<?php
    /**
    * iddiTemplateException Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiTemplateException extends iddiException{}
    /**
    * @desc Request exceptions can be handled by request level only
    */
   