<?php
    /**
    * iddiEvent Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiEvent{
      var $sourceobject;
      var $cancelled=false;
      function iddiEvent($insourceobject=null){$this->sourceobject=$insourceobject;}
      function cancel(){$this->cancelled=true;}
    }
   