<?php
    /**
    * Api_error Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class Api_error{

        function __construct($code,$message){
            $this->code=$code;
            $this->message=$message;
        }

    }
    /*
     * Alias for IDDI_Api_Result - please use IDDI_Api_Result instead
     */
   