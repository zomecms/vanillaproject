<?php

/**
 * iddiForm Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 * */
class iddiForm {

    var $fieldlinks, $values, $itemid = 1;

    function iddiForm() {
        if (iddi::$debug)
            iddiDebug::message('Creating a new form');
        $_SESSION['iddi-form'] = $this;
        $this->timestamp = time();
    }

    function processeditpost() {
        foreach ($_POST['IDDIVALUES'] as $k => $v) {
            $v = stripslashes($v);
            $this->values[$k]->value = $v;
        }
        return $this;
    }

    function store($var) {
        $fn = iddiMySql::tidyname($var->fieldname);
        if (!$this->valuestore[$var->entityid . ':' . $fn]) {
            $this->itemid++; //=rand(9,99);
            $var->postfieldid = md5(session_id() . iddiRequest::$current->id . $this->itemid);
            $this->values[$var->postfieldid] = $var;
            $this->valuestore[$var->entityid . ':' . $fn] = $var;
        } else {
            $v = $this->valuestore[$var->entityid . ':' . $fn];
            $var->postfieldid = $v->postfieldid;
        }
        return $this->valuestore[$var->entityid . ':' . $fn];
    }

    function findfield($entityid, $fieldname) {
        $fieldname = iddiMySql::tidyname($fieldname);
        $f = $this->valuestore[$entityid . ':' . $fieldname];
        //if(!$f)  throw new iddiException('Field '.$fieldname.' not found for entity id '.$entityid,'iddi.form.fieldnotfound');
        return $f;
    }

    function json() {
        return json_encode($this);
    }

    function getfield($name) {
        foreach ($this->values as $val) {
            if ($val->fieldname == $name)
                return $val;
        }
    }

    function linkVar($id, $var) {
        $this->fieldlinks[$id] = $var;
    }

    function updateEntityName($entityid, $entityname) {
        foreach ($this->values as $v) {
            if ($v->entityid == $entityid)
                $v->entityname = $entityname;
        }
        foreach ($this->valuestore as $v) {
            if ($v->entityid == $entityid)
                $v->entityname = $entityname;
        }
    }

}
