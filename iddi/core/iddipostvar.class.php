<?php
    /**
    * iddiPostVar Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiPostVar{
        var $entityname,$fieldname,$value,$entityid,$links,$definition;
        function iddiPostVar($entityname='',$fieldname='',$value='',$entityid='',$definition=''){
            if (!is_object($entityname) && !is_object($value)){
                $this->entityname=$entityname;
                $this->fieldname=$fieldname;
                $this->value=$value;
                $this->entityid=$entityid;
                $this->definition=$definition;
                iddiRequest::getform()->store($this);
            }
        }
        function addAttributes($array){
            foreach($array as $k=>$v) $this->$k=$v;
        }
        function saveable(){
            //if (iddiRequest::getDataSource($this->entityid)) iddiRequest::getDataSource($this->entityid)->setDbValue($this->fieldname,$this->value,true);
        }

    }
