<?php
    /**
    * iddiBase Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiBase{

      function dump($maxdepth=4,$level=0){
          if ($level<$maxdepth){
              $output.= '<ul><li><strong>'.$this->getClassName().'</strong>';
              $output.= '<ul>';
              foreach($this->getDefinedVars() as $k=>$v){
                  if (is_array($v)){
                      $output.= "<li>{$k} (Array) [".sizeof($v)."]";
                      $output.= '<ul>';
                      $output.=$this->dumparray($v,$maxdepth,$level);
                      $output.= '</ul></li>';
                  }
                  elseif(is_object($v)){
                      $output.= "<li>{$k} (".get_class($v)." Object)";
                      if (method_exists($v,'dump')) $output.=$v->dump($maxdepth,$level+1);
                  }
                  else{
                      $output.= "<li>{$k}=[{$v}]</li>";
                  }
              }
              $output.= '</ul></li></ul>';
          }
          return $output;
      }
      private function dumparray($va,$maxdepth=4,$level=0){
          if ($level<$maxdepth){
              foreach($va as $k=>$v){
                  if (is_array($v)){
                      $output.= "<li>{$k} (Array) [".sizeof($v)."]<ul>";
                      $output.=$this->dumparray($v,$maxdepth,$level);
                      $output.= '</ul>';
                  }
                  elseif(is_object($v)){
                      $output.= "<li>{$k} (".get_class($v)." Object)";
                      if (method_exists($v,'dump')) $output.=$v->dump($maxdepth,$level+1);
                  }
                  else{
                      $output.= "<li>[$k]=[$v]</li>";
                  }
              }
          }
          return $output;
      }
      function getClassName(){return get_class($this);}
      function getDefinedVars(){return get_object_vars($this);}
    }
