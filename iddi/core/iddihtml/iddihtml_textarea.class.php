<?php
/**
 * iddiHtml_Input class.
 *
 * Just sets input tags as void and self closing (depending on doctype)
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Textarea extends iddiXmlNode {
    function __construct() {
        $this->setnodename('textarea');
    }
}
