<?php
/**
 * iddiHtml_Img class.
 *
 * Sets img tags as void and self closing (depending on doctype) and also ensures
 * that they have an alt tag so that it validates
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Select extends iddiXmlNode {
    
    function add_option($option){
        $this->appendChild($option);
    }
    
    function __construct() {
        $this->setnodename('select');
    }
}
