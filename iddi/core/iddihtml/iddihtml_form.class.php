<?php
/**
 * iddiHtml_Img class.
 *
 * Sets img tags as void and self closing (depending on doctype) and also ensures
 * that they have an alt tag so that it validates
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_form extends iddiXmlNode {
    function __construct() {
        $this->setnodename('form');
    }

    function output($clean = false, $level = 0) {
        /**
         * Ensure that we always have an alt tag so output is valid
         */
        if ($this->attributes['ACTION']){
            $this->attributes['ACTION']=str_replace('&','&amp;',$this->attributes['ACTION']);
            $this->attributes['ACTION']=str_replace('&amp;amp;','&',$this->attributes['ACTION']);
        }

        return parent::output();
    }

}
