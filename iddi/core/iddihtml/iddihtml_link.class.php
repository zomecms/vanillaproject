<?php
/**
 * iddiHtml_Link class.
 *
 * Just sets link tags as void and self closing (depending on doctype)
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Link extends iddiHtml_Void {}
