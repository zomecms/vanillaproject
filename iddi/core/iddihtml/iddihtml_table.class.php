<?php
/**
 * iddiHtml_Img class.
 *
 * Sets img tags as void and self closing (depending on doctype) and also ensures
 * that they have an alt tag so that it validates
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Table extends iddiXmlNode {

    function __construct() {
        $this->setnodename('table');
    }

    function set_border($border=null){
        $this->attributes['BORDER']=$border;
        return $this;
    }
}
