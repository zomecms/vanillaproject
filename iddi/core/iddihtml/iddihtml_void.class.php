<?php
/**
 * iddiHtml_Void class.
 * 
 * Base class for void and self closing (depending on doctype) tags
 * 
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Void extends iddiXmlNode {
    var $is_void_html_tag = true;
    var $is_self_closing_tag = true;
}
