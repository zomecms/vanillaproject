<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_save_changes extends Api_Result {

    function output() {
        /**
         * Using good old $_POST here because we don't know what we're getting (yoinks!)
         * Each entity class will need to make sure it's data is clean
         */
        foreach ($_POST as $k => $v) {
            $parts = explode('/', $k, 4);
            if (sizeof($parts) < 3)
                throw new iddiException('Invalid data', 'yendoi.api.save_changes.invalid_data');

            $entity_name = $parts[0];
            $entity_id = $parts[1];
            $entity_field = $parts[2];
            $data = $v;

            if (!is_array($entities[$entity_name]))
                $entities[$entity_name] = array();
            if (!is_array($entities[$entity_name][$entity_id]))
                $entities[$entity_name][$entity_id] = array();
            $entities[$entity_name][$entity_id][$entity_field] = $data;
        }

        $updates = 0;
        foreach ($entities as $entity_name => $entityids) {
            $entity_class = 'iddiEntity_' . $entity_name;
            if (class_exists($entity_class)) {
                $entity = new $entity_class();
            } else {
                $entity = new iddiEntity();
            }
            foreach ($entityids as $entity_id => $entity_fields) {
                if ($entity_id > 0) {
                    $entity->get_by_id($entity_id, $entity_name);
                } else {
                    $entity_fields['id'] = -1;
                    $entity->language = 'en-gb';
                    $entity->entityname = $entity_name;
                    //Grab the latest entry of this type from the database to use as a template
                    $sql = 'SELECT * FROM iddi_sysfilenames WHERE entityname=\'' . $entity->entityname . '\' ORDER BY id DESC LIMIT 0,1';
                    $template = iddiMySql::query($sql);
                    foreach ($template as $fs) {
                        foreach ($fs->dbfields as $k => $v) {
                            $entity->$k = $v;
                        }
                    }
                    $vf = explode('/', $entity->virtualfilename);
                    array_pop($vf);
                    $entity->id = -1;
                    $entity->rootlangaugeitemid = -1;
                    $entity->entityid = -1;
                    $entity->virtualfilename=implode('/',$vf).'/'.iddiMySql::tidyname($entity_fields['pagetitle']).'_'.time();
                }

                foreach ($entity_fields as $k => $v) {
                    $entity->$k = $v;
                }

                $entity->save();
                $updates++;
            }
        }

        $this->message = 'Data Saved. ' . $updates . ' entities updated.';

        parent::output();
    }

}
