<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_table extends Api_Result{
    function output(){               
        $entity_name=$_GET['entity'];
        $_REQUEST['ajax']=1;
        iddiTemplate::$addeditors=false;
        
        return iddiTemplate::render(array('entity_list_'.$entity_name,'entity_list','missing'), $datasource);
        
        
        $paths=array(
            IDDI_FILE_PATH.'../iddi-project/templates/admin/'.iddiRequest::$current->language.'/entity_list_'.$entity_name,
            IDDI_FILE_PATH.'../iddi-project/templates/admin/entity_list_'.$entity_name,
            IDDI_FILE_PATH.'templates/admin/entity_list_'.$entity_name,
            IDDI_FILE_PATH.'../iddi-project/templates/admin/'.iddiRequest::$current->language.'/entity_list',
            IDDI_FILE_PATH.'../iddi-project/templates/admin/entity_list',
            IDDI_FILE_PATH.'templates/admin/entity_list',
            IDDI_FILE_PATH.'templates/missing');
        
        $template = iddiTemplate::load_first_match($paths);
                
        return $template->output();        
    }
}