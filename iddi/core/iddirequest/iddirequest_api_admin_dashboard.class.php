<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_dashboard extends Api_Result{
    function output(){        
        return iddiTemplate::render('dashboard');        
        
        $sales=$this->get_stat('total_sales');
        $headline1=new iddiComponent_Headline();
        $headline1->title='Sales';
        $headline1->subtitle='Total Sales';
        $headline1->figure='£'.number_format($sales->total,2);
        $headline1->velocity=$sales->velocity;

        $out.=$headline1->output();

        $sales_volume=$this->get_stat('total_sales_volume');
        $headline2=new iddiComponent_Headline();
        $headline2->title='Sales Volume';
        $headline2->subtitle='Total Items Sold';
        $headline2->figure=number_format($sales_volume->total,0);
        $headline2->velocity=$sales_volume->velocity;
        $out.=$headline2->output();

        $orders=$this->get_stat('total_orders');
        $headline3=new iddiComponent_Headline();
        $headline3->title='Orders';
        $headline3->subtitle='Total Orders';
        $headline3->figure=number_format($orders->total,0);
        $headline3->velocity=$orders->velocity;
        $out.=$headline3->output();

        $customers=$this->get_stat('total_customers');
        $headline4=new iddiComponent_Headline();
        $headline4->title='Customers';
        $headline4->subtitle='Total Customers';
        $headline4->figure=number_format($customers->total,0);
        $headline4->velocity=$customers->velocity;
        $out.=$headline4->output();

        $chart=new iddiComponent_Flot_Chart();
        $out.='<div class="iddi-dashboard-component" style="width:65%"><h1>Sales Activity</h1>';
        $out.=$chart->output();
        $out.='</div>';

        $chart2=new iddiComponent_Flot_Pie();
        $out.='<div class="iddi-dashboard-component" style="width:35%"><h1>Product Sales</h1>';
        $out.=$chart2->output();
        $out.='</div>';


        $table=new iddiComponent_Table();
        $table->title='Latest Orders to Process';
        $table->limit=7;
        $out.='<div class="iddi-dashboard-component iddi-dashboard-small-component">';
        $out.=$table->draw('order','payment_status=113 and delivery_status=97',array('id','billable_total','total_items','firstname','lastname','postcode'));
        $out.='<span class="iddi-show-item" data-entity-id="97" data-entity-name="deliverystatus">View All</span>';
        $out.='</div>';

        $table=new iddiComponent_Table();
        $table->title='Latest Payments';
        $table->limit=7;
        $out.='<div class="iddi-dashboard-component iddi-dashboard-small-component">';
        $out.=$table->draw('payment');
        $out.='<span class="iddi-show-entity" data-iddi-entity="payment">View All</span>';
        $out.='</div>';

        $table=new iddiComponent_Table();
        $table->title='Latest Reviews';
        $table->limit=15;
        $out.='<div class="iddi-dashboard-component iddi-dashboard-component" style="width:55%">';
        $out.=$table->draw('review',null,array('reviewname','reviewtext','country','score'));
        $out.='<span class="iddi-show-entity" data-iddi-entity="review">View All</span>';
        $out.='</div>';
        
        $out.='<div class="iddi-dashboard-component iddi-dashboard-small-component" style="width:45%;"><h1>Reviews (past 30 days)</h1>';
        $chart3=new iddiComponent_Flot_Pie();
        $chart3->sql='SELECT country as pagetitle,count(r.id) as v
                FROM iddi_review r
                INNER JOIN iddi_sysfilenames f ON r.id=f.id
                WHERE f.created>DATE_SUB(NOW(),INTERVAL 30 DAY) AND f.deleted=0
                GROUP BY country 
                ORDER BY count(f.id) desc';  
        $out.='<div class="iddi-dashboard-component" style="width:50%">';
        $out.=$chart3->output();
        $out.='</div>';        
        
        $chart3=new iddiComponent_Flot_Pie();
        $chart3->sql='SELECT score as pagetitle,count(r.id) as v
                FROM iddi_review r
                INNER JOIN iddi_sysfilenames f ON r.id=f.id
                WHERE f.created>DATE_SUB(NOW(),INTERVAL 30 DAY)  AND f.deleted=0
                GROUP BY score
                ORDER BY score';  
        $out.='<div class="iddi-dashboard-component" style="width:50%">';
        $out.=$chart3->output();
        $out.='</div>';           
        
        $chart4=new iddiComponent_Flot_Chart();
        $chart4->sql='SELECT f.created,COUNT(r.id) v FROM iddi_review r INNER JOIN iddi_sysfilenames f ON r.id=f.id WHERE r.created>DATE_SUB(NOW(),INTERVAL 30 DAY) AND f.deleted=0 GROUP BY YEAR(f.created),MONTH(f.created),DAY(f.created)';
        $out.='<div class="iddi-dashboard-component">';
        $out.=$chart4->output();
        $out.='</div>';         
        $out.='</div>';
        
        die($out);
    }

    function get_stat($name){
        $name="get_$name";
        $data=iddiCache::get('STAT_'.$name);
        if($data==null){
            $data=$this->$name();
            //iddiCache::save('STAT_'.$name, $data,120);
        }
        return $data;
    }

    function get_total_sales(){
        $sql='SELECT sum(total_price) v from iddi_order WHERE balance_to_pay=0';
        $rs=iddiMySql::query($sql);
        $out->total=$rs->getFirstRow()->v;

        $sql='SELECT sum(total_price) v from iddi_order WHERE balance_to_pay=0 and created>=date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->this_month=intval($rs->getFirstRow()->v);

        $sql='SELECT sum(total_price) v from iddi_order WHERE balance_to_pay=0 and created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->previous_month=intval($rs->getFirstRow()->v);

        $out->velocity=intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }

    function get_total_sales_volume(){
        $sql='SELECT sum(total_items) v from iddi_order WHERE balance_to_pay=0 and total_price>0';
        $rs=iddiMySql::query($sql);
        $out->total=$rs->getFirstRow()->v;

        $sql='SELECT sum(total_items) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->this_month=intval($rs->getFirstRow()->v);

        $sql='SELECT sum(total_items) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->previous_month=intval($rs->getFirstRow()->v);

        $out->velocity=intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }

    function get_total_orders(){
        $sql='SELECT count(id) v from iddi_order WHERE balance_to_pay=0 and total_price>0';
        $rs=iddiMySql::query($sql);
        $out->total=$rs->getFirstRow()->v;

        $sql='SELECT count(id) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->this_month=intval($rs->getFirstRow()->v);

        $sql='SELECT count(id) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->previous_month=intval($rs->getFirstRow()->v);

        $out->velocity=intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }

    function get_total_customers(){
        $sql='SELECT count(id) v from iddi_customer';
        $rs=iddiMySql::query($sql);
        $out->total=$rs->getFirstRow()->v;

        $sql='SELECT count(id) v from iddi_customer WHERE created>=date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->this_month=intval($rs->getFirstRow()->v);

        $sql='SELECT count(id) v from iddi_customer WHERE created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs=iddiMySql::query($sql);
        $out->previous_month=intval($rs->getFirstRow()->v);

        $out->velocity=intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }
}