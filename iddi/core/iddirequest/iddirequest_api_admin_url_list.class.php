<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_url_list extends Api_Result{
    function output(){
        $entity_name = filter_input(INPUT_GET, 'entity_name', FILTER_SANITIZE_STRING);
        $path = urldecode(filter_input(INPUT_GET, 'path', FILTER_SANITIZE_STRING));
        
        if($path=='/')
            $path='';
        
        $sql='SELECT * from {PREFIX}sysfilenames where virtualfilename like "'.$path.'/%" and virtualfilename not like "'.$path.'/%/%" and deleted=0 ';
        if($entity_name != '')
            $sql.=' AND entityname="'.mysql_escape_string($entity_name).'"';
        $sql.=' ORDER BY pagetitle';
        
        $results = iddiMySql::query($sql);
        $this->pages=[];
        foreach($results as $result){
            $page=new stdClass();
            $page->url=$result->virtualfilename;
            $page->id=$result->id;
            $page->entityname=$result->entityname;
            $page->title=$result->pagetitle;
            $this->pages[]=$page;
        }
        
        $parts=explode('/',$path);
        array_pop($parts);
        $this->up = implode('/',$parts);
        
        
        $this->success = true;
        return parent::output();
    }
}
