<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_core_tool extends Api_Result{
    function output(){
        $template_file=$_GET['template'];
        $entity=$_GET['entity'];
        $id=intval($_GET['entity_id']);
        
        $fullname=IDDI_FILE_PATH.'templates/'.$template_file;
        $fullnameproject=IDDI_FILE_PATH.'../iddi-project/templates/'.$template_file;
        $fullnamelang=IDDI_FILE_PATH.'../iddi-project/templates/'.iddiRequest::$current->language.'/'.$template_file;
        if(file_exists($fullnameproject)) $fullname=realpath($fullnameproject);
        if(file_exists($fullnamelang)) $fullname=realpath($fullnamelang);

        $datasource=null;
        if($entity!='' && $id > 0){
            $datasource=new iddiEntity($entity,$id);
        }
        
        $template = new iddiTemplate($fullname,$datasource);
                
        return $template->output();
                
    }
}