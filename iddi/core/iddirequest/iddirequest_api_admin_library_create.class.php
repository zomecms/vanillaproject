<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_library_create extends iddiApi_Result {

    function output() {
        $library = filter_input(INPUT_GET, 'name', FILTER_SANITIZE_STRING);        

        $library = preg_replace('/[^a-z0-9_]/','',str_replace(' ','_',strtolower($library)));
        
        $library_path = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . $library;
        @mkdir($library_path);
        @chmod($library_path,0755);
        $library_path = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'originals' . DIRECTORY_SEPARATOR . $library;
        @mkdir($library_path);
        @chmod($library_path,0755);
        $library_path = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $library;
        @mkdir($library_path);
        @chmod($library_path,0755);        
        if (!file_exists($library_path)) {
            throw new iddiException('Failed to create library ' . $library, 'yendoi.api.library_create.invalid_library');
        }
        
        $this->library_created=$library;
        $this->success=true;
        
        parent::output();
    }

}
