<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_create extends Api_Result {

    function output() {        
        $entity_name=$_GET['entity'];        
        
        $paths=array(
            IDDI_FILE_PATH.'../iddi-project/templates/'.iddiRequest::$current->language.'/entity_create_'.$entity_name,
            IDDI_FILE_PATH.'../iddi-project/templates/entity_create_'.$entity_name,
            IDDI_FILE_PATH.'templates/entity_create_'.$entity_name,
            IDDI_FILE_PATH.'../iddi-project/templates/'.iddiRequest::$current->language.'/entity_create',
            IDDI_FILE_PATH.'../iddi-project/templates/entity_create',
            IDDI_FILE_PATH.'templates/entity_create');
        
        $template = iddiTemplate::load_first_match($paths);
                
        return $template->output();        
    }

}
