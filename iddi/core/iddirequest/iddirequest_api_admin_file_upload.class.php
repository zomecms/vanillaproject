<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_file_upload extends iddiApi_Result {

    function output() {
        $library = filter_input(INPUT_POST, 'library', FILTER_SANITIZE_STRING);        

        if (strstr($library, '/')) {
            throw new iddiException('Invalid Library : ' . $library, 'yendoi.api.file_delete.invalid_library');
        }
        if (strstr($file, '/')) {
            throw new iddiException('Invalid File', 'yendoi.api.file_delete.invalid_file');
        }
        $library_path = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'originals' . DIRECTORY_SEPARATOR . $library;
        if (!file_exists($library_path)) {
            throw new iddiException('Invalid Library : ' . $library, 'yendoi.api.file_delete.invalid_library');
        }

        $file_name = $_FILES['file']['name'];
        $file_type = $_FILES['file']['type'];
        $file_error = $_FILES['file']['error'];
        $temp_name = $_FILES["file"]["tmp_name"];

        if(is_array($file_name)){
            foreach($file_name as $k=>$filename){
                $this->uploadfile($library_path,$filename,$file_type[$k],$temp_name[$k],$file_error[$k]);
            }
        }else{
            $this->uploadfile($library_path,$filename,$file_type,$temp_name,$file_error);
        }
        
        $this->success = true;

        parent::output();
        
    }
    
    function uploadfile($library_path,$file_name,$file_type,$temp_name,$file_error){            
        move_uploaded_file($temp_name,$library_path . DIRECTORY_SEPARATOR . $file_name);            
    
        if ($file_error == UPLOAD_ERR_OK) {
            //Processes your file here
        } else {
            switch ($file_error) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new iddiException('File too large', 'yendoi.api.file_upload.invalid_file');
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    throw new iddiException('File too large', 'yendoi.api.file_upload.invalid_file');
                    break;
                case UPLOAD_ERR_PARTIAL:
                    throw new iddiException('Invalid File', 'yendoi.api.file_upload.invalid_file');
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new iddiException('Invalid File', 'yendoi.api.file_upload.invalid_file');
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    throw new iddiException('Invalid File', 'yendoi.api.file_upload.invalid_file');
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    throw new iddiException('Invalid File', 'yendoi.api.file_upload.invalid_file');
                    break;
                case UPLOAD_ERR_EXTENSION:
                    throw new iddiException('Invalid File', 'yendoi.api.file_upload.invalid_file');
                    break;
                default: 
                    throw new iddiException('Invalid File', 'yendoi.api.file_upload.invalid_file');
                    break;
            }
        }
        
    }

}
