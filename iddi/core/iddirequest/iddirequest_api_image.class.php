<?php

class iddiRequest_api_image extends Api_Result {

    function getResponse() {        
        //Unlike other API calls we can specify to just return the image filename rather than a full api response
        $width = intval(filter_input(INPUT_GET, 'width', FILTER_SANITIZE_STRING));
        $height = intval(filter_input(INPUT_GET, 'height', FILTER_SANITIZE_STRING));
        $scalemode = filter_input(INPUT_GET, 'scalemode', FILTER_SANITIZE_STRING);
        $outmode = filter_input(INPUT_GET, 'outmode', FILTER_SANITIZE_STRING);

        $filename = urldecode(filter_input(INPUT_GET, 'filename', FILTER_SANITIZE_STRING));
        $library = filter_input(INPUT_GET, 'library', FILTER_SANITIZE_STRING);
        $file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);

        $data = new stdClass();
        $data->width = $width;
        $data->height = $height;
        $data->scalemode = $scalemode;

        $code = base64_encode(serialize($data));        
        
        //This converts old style filename entries to libraries which is much safer
        if($filename != null && ($library == null || $file == null)){
            $filename=str_replace('\\','/',$filename);
            $fileparts=pathinfo($filename);
            $pathparts=explode('/',$fileparts['dirname']);
            $file=$fileparts['basename'];
            $library=array_pop($pathparts);
        }
        
        if ($library != null && $file !== null) {
            
            if (strstr($library, '/')){
                throw new iddiException('Invalid Library : '.$library, 'yendoi.api.image.invalid_library');
            }
            if (strstr($file, '/')){
                throw new iddiException('Invalid File', 'yendoi.api.image.invalid_file');
            }            
            $library_path = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'originals' . DIRECTORY_SEPARATOR . $library;
            if (!file_exists($library_path)){
                throw new iddiException('Invalid Library : '.$library, 'yendoi.api.image.invalid_library');
            }            
                        
            $in_filename = $library_path . DIRECTORY_SEPARATOR . $file;
            $fileparts = pathinfo($in_filename);
            $out_filename = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $library . DIRECTORY_SEPARATOR . $fileparts['filename'] . '{' . $code . '}.' . $fileparts['extension'];
        } else {
            $in_filename = __DIR__ . '/../../no-pic.jpg';
            $out_filename = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'general' . DIRECTORY_SEPARATOR . 'no-pic{' . $code . '}.jpg';
        }

        if ($scalemode === null) {
            $scalemmode = 'force';
        }

        if (!file_exists($out_filename) || filemtime($in_filename) > filemtime($out_filename)) {
            $image = new iddiImage($in_filename, $height, $width, $scalemode, true);
            $image->resize($height, $width, $scalemode);
            $image->save($out_filename);
        }
        
        switch ($outmode) {
            case 'image':
                $file_info = getimagesize($out_filename);
                list($inputwidth, $inputheight, $inputtype) = $file_info;
                $image_mime = image_type_to_mime_type($inputtype);
                switch ($inputtype) {
                    case IMAGETYPE_GIF:
                        header('Content-Type: image/gif');
                        break;
                    case IMAGETYPE_PNG:
                        header('Content-Type: image/png');
                        break;
                    default:
                        header('Content-Type: image/jpeg');
                }
                readfile($out_filename);
                die();
                break;
            case 'json':
                $image_attr = new stdClass();
                $image_attr->outputwidth = $image->outputwidth;
                $image_attr->outputheight = $image->outputheight;
                $image_attr->path = str_replace('..', '', $out_filename);
                $image_attr->path = str_replace('//', '/', $image_attr->path);
                die(json_encode($image_attr));
                break;
            default:
                die(str_replace('//', '/', str_replace('..', '', $out_filename)));
        }
    }

}
