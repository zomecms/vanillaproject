<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_setup extends iddiApi_Result{
    var $login_required = false;
    function output(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            iddiConfig::$_config->site->{'site-name'} = filter_input(INPUT_POST,'sitename',FILTER_SANITIZE_STRING);
            iddiConfig::$_config->databases->database[0]->{'user'} = filter_input(INPUT_POST,'dbusername',FILTER_SANITIZE_STRING);
            iddiConfig::$_config->databases->database[0]->{'host'} = filter_input(INPUT_POST,'dbhost',FILTER_SANITIZE_STRING);
            iddiConfig::$_config->databases->database[0]->{'pass'} = filter_input(INPUT_POST,'dbpassword',FILTER_SANITIZE_STRING);
            iddiConfig::$_config->databases->database[0]->{'database'} = filter_input(INPUT_POST,'dbname',FILTER_SANITIZE_STRING);
            iddiConfig::Save();
            
            iddimysql::startup(iddiConfig::Load());
            
            $_GET['all'] = 1;
            
            try{
            iddiTemplateList::compileall();
            iddiTemplateList::compileall();
            }catch(Exception $e){
                
            }
            $user = new iddiUser();
            $user->username = filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING);
            $user->password = sha1(filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING));
            $user->accesslevels = 'admin';
            $user->entityname = 'user';
            $user->save();
            //$user->login($user->username, filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING));
                        
            die('<script>alert("Setup complete. Taking you home. /admin to login");document.location="/";</script>');
        }else{
            return iddiTemplate::render('z-setup');        
        }
    }
}