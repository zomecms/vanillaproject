<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_save extends Api_Result{
    function output(){
        $eclass='iddiEntity_'.$_POST['entityname'];
        if(class_exists($eclass)){
            $entity=new $eclass();
        }else{
            $entity=new iddiEntity();
        }

        if($_POST['id']>0){
            $entity->get_by_id($_POST['id']);
        }else{
            $entity->language='en-gb';
            $entity->entityname=$_POST['entityname'];
            //Grab the latest entry of this type from the database to use as a template
            $sql='SELECT * FROM iddi_sysfilenames WHERE entityname=\''.$entity->entityname.'\' ORDER BY id DESC LIMIT 0,1';
            $template=iddiMySql::query($sql);
            foreach($template as $fs){
                foreach($fs->dbfields as $k=>$v){
                    $entity->$k=$v;
                }
            }
            $vf=explode('/',$entity->virtualfilename);
            array_pop($vf);
            $entity->id=-1;
            $entity->rootlangaugeitemid=-1;
            $entity->entityid=-1;
        }


        unset($_POST['entityname']);
        unset($_POST['entityid']);
        foreach($_POST as $k=>$v){
            if($entity->pagetitle=='' && $_POST['pagetitle']==''){
                $entity->pagetitle=$v;
            }
            $entity->$k=$v;
        }
        if($entity->id==-1) $entity->virtualfilename=implode('/',$vf).'/'.iddiMySql::tidyname($entity->pagetitle).'_'.time();

        $entity->save();

        $out=new stdClass();
        $out->entity=$entity;
        $out->message='Item Saved';        
        
        die(json_encode($out));

    }
}