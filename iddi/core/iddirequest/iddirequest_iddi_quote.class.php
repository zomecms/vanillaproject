<?php
    /**
    * iddiRequest_iddi_quote Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiRequest_iddi_quote extends iddiRequest_resource{
      function getResponse(){
        return $this->_getResponse($_GET['entity'],'quote.xslt');
      }
    }

   