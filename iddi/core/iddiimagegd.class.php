<?php
    /**
    * iddiImage Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiImageGd extends iddiEvents{
        var $inputfilename,$im;

        function __construct($inputfilename,$outputheight=0,$outputwidth=0,$scalemode='crop',$preventupscale=false){
            $this->inputfilename=$inputfilename;

            if ($inputfilename!=''){
                $this->resize($outputheight,$outputwidth,$scalemode,$preventupscale);
            }
        }

        function resize($outputheight,$outputwidth,$scalemode='crop',$preventupscale=false){
$preventupscale=true;
            $inputfilename=$this->inputfilename;
            $file_info=getimagesize($inputfilename);
            list($inputwidth,$inputheight,$inputtype)=$file_info;
            $image_mime=image_type_to_mime_type($inputtype);
            iddiDebug::dumpVar('Reszing File of type '.$inputtype.':'.$image_mime,$file_info);
            $this->inputtype=IMAGETYPE_JPEG;
            switch($inputtype){
              case IMAGETYPE_BMP:
                $img=@imagecreatefrombmp($inputfilename);
                break;
              case IMAGETYPE_GIF:
                $this->inputtype=IMAGETYPE_GIF;
                $img=@imagecreatefromgif($inputfilename);
                break;
              case IMAGETYPE_JPEG:
                $img=@imagecreatefromjpeg($inputfilename);
                break;
              case IMAGETYPE_PNG:
                $this->inputtype=IMAGETYPE_PNG;
                if(iddi::$debug) iddiDebug::message('Processing PNG');
                //Allocate some memory for the image
                $required_memory = Round(filesize($inputfilename)*8);
                $new_limit=memory_get_usage() + $required_memory;
                if(iddi::$debug) iddiDebug::message('Allocating '.$required_memory.' memory to '.$new_limit);
                ini_set("memory_limit", round($new_limit/1024).'M');
                //Load the image
                if(iddi::$debug) iddiDebug::message('Loading the PNG');
                $img=@imagecreatefrompng($inputfilename);
                //Preserve Alpha
                if(iddi::$debug) iddiDebug::message('Preserving any Alpha Transparancy');
                break;
              case IMAGETYPE_WBMP:
                $img=@imagecreatefromwbmp($inputfilename);
                break;
              case IMAGETYPE_XBM:
                $img=@imagecreatefromxbm($inputfilename);
                break;
              case IMAGETYPE_JPEG2000:
              case IMAGETYPE_IFF:
              case IMAGETYPE_JB2:
              case IMAGETYPE_JP2:
              case IMAGETYPE_JPC:
              case IMAGETYPE_JPX:
              case IMAGETYPE_PSD:
              case IMAGETYPE_SWC:
              case IMAGETYPE_SWF:
              case IMAGETYPE_TIFF_II:
              case IMAGETYPE_TIFF_MM:
              case IMAGETYPE_ICO:
              default:
                throw new iddiException('Image type '.$image_mime.' is not currently supported','iddi.imaging.type_not_supported');
                break;
            }
            //if(!$img) throw new iddiException('Failed to create image','iddi.imaging.image_create_failed');

            $outputaspect=($outputwidth>$outputheight)?'landscape':'portrait';
            $outputratio=($outputheight/$outputwidth);
            $inputaspect=($inputwidth>$inputheight)?'landscape':'portrait';
            $inputratio=($inputheight/$inputwidth);
            switch($scalemode){
                case 'stretch':
                    //Stretch just whacks it in - really this should not be used, if you use it you're more than likely an idiot.
                    //Retains size - no
                    //Retains aspect - no
                    //Retains entire image - yes
                    //Adds bars - no
                    $targettop=0;
                    $targetleft=0;
                    $targetwidth=$outputwidth;
                    $targetheight=$outputheight;
                    break;
                case 'fit':
                    //Fits it into the desired image, will add bars to one dimension
                    //Shrink the image by the largest difference
                    //Retains size - no
                    //Retains aspect - yes
                    //Retains entire image - yes
                    //Adds bars - yes
                    $heightratio=$outputheight/$inputheight;
                    $widthratio=$outputwidth/$inputwidth;
                    $ratio=($heightratio>$widthratio)?$widthratio:$heightratio;
                    $targetheight=ceil($inputheight*$ratio);
                    $targetwidth=ceil($inputwidth*$ratio);
                    $targettop=(ceil(($outputheight-$targetheight)/2));
                    $targetleft=(ceil(($outputwidth-$targetwidth)/2));
                    break;
                case 'bestfit':
                case 'force':
                    //This will do a fit, but then reduce the size of the output image instead of black bars
                    //Shrink the image by the largest difference
                    //Retains size - no
                    //Retains aspect - yes
                    //Retains entire image - yes
                    //Adds bars - no
                    $heightratio=$outputheight/$inputheight;
                    $widthratio=$outputwidth/$inputwidth;
                    $ratio=($heightratio>$widthratio)?$widthratio:$heightratio;
                    if($ratio>1 && $preventupscale) $ratio=1;
                    $targetheight=ceil($inputheight*$ratio);
                    $targetwidth=ceil($inputwidth*$ratio);
                    $targettop=0;
                    $targetleft=0;
                    $outputheight=$targetheight;
                    $outputwidth=$targetwidth;
                    break;
                default:
                    //This is the best option really;
                    //Shrink the image by the largest difference - but don't increase the image size.
                    //Retains size - no
                    //Retains aspect - yes
                    //Retains entire image - yes
                    //Adds bars - no
                    $heightratio=$outputheight/$inputheight;
                    $widthratio=$outputwidth/$inputwidth;
                    $ratio=($heightratio<$widthratio)?$widthratio:$heightratio;
                    if($ratio>1 && $preventupscale) $ratio=1;
                    $targetheight=ceil($inputheight*$ratio);
                    $targetwidth=ceil($inputwidth*$ratio);
                    $targettop=(ceil(($outputheight-$targetheight)/2));
                    $targetleft=(ceil(($outputwidth-$targetwidth)/2));
                    break;

            }
            if($this->im) image_destroy($this->im);
            $this->im=imagecreatetruecolor($outputwidth,$outputheight);
            //$background_color = imagecolorallocate ($this->im, 255, 255, 255);
            //$text_color = imagecolorallocate ($this->im, 233, 14, 91);
            $trans_colour = imagecolorallocatealpha($this->im, 0, 0, 0, 127);
            imagefill($this->im, 0, 0, $trans_colour);
            //now overlay the imagefile
            //echo "$targetleft , $targettop , $targetwidth , $targetheight , $inputwidth , $inputheight";
            imagecopyresampled($this->im,$img,$targetleft,$targettop,0,0,$targetwidth,$targetheight,$inputwidth,$inputheight);
        }

        function destroy(){
            imagedestroy($this->im);
        }

        function save($filename,$quality=90){    
            if(substr($filename, 0, strlen(IDDI_IMAGES_PATH)) !== IDDI_IMAGES_PATH){
                iddiDebug::warning($filename.' is not in the images folder. Being replaced with no-pic to allow the CMS to continue without error','core.imagegd.invalidimagelocation');
                $fileparts=pathinfo($filename);
                $filename=IDDI_IMAGES_PATH.'/public/general/'.$fileparts[PATHINFO_BASENAME];
            }
                    
            $p=pathinfo($filename);
            $d=$p['dirname'];
            $paths=explode('/',$d);
            $p='';
            foreach($paths as $p1){
                $p.=$p1.'/';
                if ($p1!='..' && $p1!='.'){
                    @mkdir($p);
                }
            }
            if(substr($filename, 0, strlen(IDDI_IMAGES_PATH)) === IDDI_IMAGES_PATH) 
                @chmod($p,0777);
            
            if(iddi::$debug) iddiDebug::message('Saving '.$filename);
            switch($this->inputtype){
              case IMAGETYPE_BMP:
                imagejpeg($this->im,$filename,$quality);
                break;
              case IMAGETYPE_GIF:
                imagegif($this->im,$filename);
                break;
              case IMAGETYPE_JPEG:
                imagejpeg($this->im,$filename,$quality);
                break;
              case IMAGETYPE_PNG:
                @imagealphablending($this->im, false);
                @imagesavealpha($this->im, true);
                @imagepng($this->im,$filename);
                break;
              case IMAGETYPE_WBMP:
                imagewbmp($this->im,$filename);
                break;
              case IMAGETYPE_XBM:
                imagejpeg($this->im,$filename,$quality);
                break;
              case IMAGETYPE_JPEG2000:
              case IMAGETYPE_IFF:
              case IMAGETYPE_JB2:
              case IMAGETYPE_JP2:
              case IMAGETYPE_JPC:
              case IMAGETYPE_JPX:
              case IMAGETYPE_PSD:
              case IMAGETYPE_SWC:
              case IMAGETYPE_SWF:
              case IMAGETYPE_TIFF_II:
              case IMAGETYPE_TIFF_MM:
              case IMAGETYPE_ICO:
              default:
                throw new iddiException('Image type '.$image_mime.' is not currently supported','iddi.imaging.type_not_supported');
                break;
            }
            //if(!$img) throw new iddiException('Failed to create image','iddi.imaging.image_create_failed');
            //@image_destroy($this->im);
        }
    }

