<?php
/**
 * This provides some core functions, most importantly iddi::output() which is where
 * all the action for the CMS starts.
 *
 * @author J.Patchett
 * @package IDDI Core
 **/
class iddi {
    const IDDI_NAMESPACE='http://d.yendoi.org';

    /**
     * Tracks the node id used by get_next_node_id()
     * @var int
     */
    protected static $currentid = 0;

    /**
     * Use this to switch debugging on and off. Ideal set like this because you
     * can switch on at the start of your plugin code and off at the end so that
     * you only see debug information for your plugin
     *
     * Defaults to false (obviously!)
     *
     * @var bool
     */
    public static $debug = false;

    public static $content_type_header;
    protected static $devmode = null;

    /**
     * This is where everything starts. There is a call to this function from the bootstrap
     * file iddi.php in /iddi/. All calls to this site will be sent through iddi.php
     * and in turn that sends the request here for processing.
     *
     * The call first checks to see if a custom request handler exists. This is a class
     * named in the form iddiRequest_URL. See more under custom request handlers in the
     * developer guide
     *
     * If there isn't a custom request handler the iddiRequest object is used instead.
     *
     * The request handler is then queried for a response object, where the request object
     * does it's work returning a response object.
     *
     * Finally the render method is called on the response object and returned back out to
     * iddi.php which simply echos the response.
     *
     * @return string
     * @throws iddiException
     */
    public static function output() {
        self::$content_type_header=iddiConfig::GetValue('site', 'default-content-type-header', 'Content-Type:text/html; charset=UTF-8');

        //Seperate the querystring from the request uri to give us the requested URL
        $raw_url = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);
        $url_parts = explode('?', $raw_url, 2); 
        $url = $url_parts[0];        
        
        $request_object=self::get_request_object_from_url($url);
        return $request_object->getResponse();
    }

    /**
     * Gets the request object based on the URL. This allows us to create custom
     * request objects to handle things like API calls. If none exist then we just
     * use the standard request object.
     *
     * @return \iddiRequest|\class_name
     */
    protected static function get_request_object_from_url($url){
        //Now check to see if a custom request handler is available
        $class_name = 'iddiRequest' . str_replace('/', '_', $url);
        
        if (class_exists($class_name)){
            $request_object = new $class_name;
        }else{
            $request_object = new iddiRequest();
        }
        
        if(!($request_object instanceof iddiRequest)){
            $additional_data->url = $url;
            $additional_data->class = get_class($request_object);
            $additional_data->parents = get_parent_classes($request_object);
            throw new iddiException_Request('Request URL resulted in an invalid request object','iddi.core.invalid_request',$additional_data);
        }
        
        return $request_object;
    }

    /**
     * Create a log message to the screen
     * @param $message Text of the message to display
     * @param $level Level of the message
     * */
    public static function Log($message, $level = 0) {
        if ($level < IDDI_LOG_DISPLAY_LEVEL) {
            $message = str_replace('<', '&lt;', $message);
            $message = str_replace('>', '&gt;', $message);
            $mem = memory_get_usage(true) / 1024 / 1024;
            echo "<li style=\"font:monospace 8px\">{$mem}:{$message}</li><script>window.scroll(0,150000);</script>" . str_repeat(" ", 255);
            flush();
        }
    }

    /**
     * Utility for counting nodes across all classes. Located in here so that if
     * we find ourselves using document fragments etc... we can still keep a unique
     * ID across all documents.
     *
     * Note : this method gets called a lot. Would be great to see if we can lose it somehow
     *
     * @return int
     */
    public static function get_next_node_id() {
        return ++self::$currentid;  //Using pre-increment for performance.
    }
    

    public static function is_devmode() {        
        if(self::$devmode==null){
            $serveraddress = $_SERVER['SERVER_ADDR'];
            $remoteaddress = $_SERVER['REMOTE_ADDR'];
            self::$devmode = ($serveraddress == $remoteaddress);
        }
        return self::$devmode;
    }    
}
