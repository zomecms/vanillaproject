<?php
    /**
    * iddiXmlCData Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlCData extends iddiXmlIddiNode {
        protected $_v;
        function output($clean=false,$level=0){
            return $this->_v;
        }
        function setvalue($value){
            $this->_v=$value; return $this;
        }
        function getvalue(){
            return $this->_v;
        }
        function clonenode($target,$deep=false){
            $newnodeclass=get_class($this);
            $newnode=new $newnodeclass();
            $newnode->setup($this->owner,$this->nodename,$target,$this->attributes,$this->_v);
            $newnode->setValue($this->_v);
            if ($deep){
                if ($this->children){
                    foreach($this->children as $child) {
                        $newnode->appendchild($child->clonenode($newnode,true));
                    }
                }
            }
            return $newnode;
        }
    }
   