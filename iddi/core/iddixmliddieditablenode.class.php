<?php

/**
 * This is for iddi nodes that are editable on the frontend of the website.
 * These nodes MUST ALWAYS be wrapped in an enclosing html node that can be
 * used exclusively by this node. The enclosing node becomes the target for 
 * data attibutes used by the editor.
 * 
 * Compiler will throw an exception if there is no parent html node that is not already
 * in use by another editable node
 *
 * @author Jonathan
 */
class iddiXmlIddiEditableNode extends iddiXmlIddiNode {

    function wrapIfNecessary() {
        //Basically if our parent node has any other descendants we need to wrap our edit node in a div. If designers need to override this to get a good
        //WYSIWYG output do it manually adding an editmodeonly="true" attribute if you don't want the extra tag to ruin nice markup
        if (count($this->parent->children) > 1) {
            $div = new iddiXmlNode($this->owner, 'div', $this->parent);
            $div->appendChild($div);
        }
    }

}
