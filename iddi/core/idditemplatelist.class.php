<?php
    /**
    * iddiTemplateList Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiTemplateList extends iddiIterator{
      static function loadall(){
          $tlist=new iddiTemplateList();
          //$tlist->loadfrompath('./templates/');
          $tlist->loadfrompath(IDDI_FILE_PATH.'../iddi-project/templates/');
          return $tlist;
      }
      function loadfrompath($path){
          $d=opendir($path); while($f=readdir($d)) if (substr($f,-4)=='.xml') $this->append(new iddiTemplate($path.$f));
      }
      static function compileall($path=''){
            //This operation requires lots of time and memory, so we'll try and force the issue
            set_time_limit(0);
            ini_set('memory_limit','2048M');  //Don't panic - even the biggest projects shouldn't need 512Mb!

          if ($path=='') $path=IDDI_FILE_PATH.'../iddi-project/templates/';
          echo '<body style="margin:0;padding:10px;background:black;color:white;font-family:monospace;font-size:12px;"><h1>IDDI Compiler v1.0 alpha</h1>';
          echo str_repeat("  \n",2550);flush();
          iddiDatabaseBase::builddatabase();
          set_time_limit(0);
          $d=opendir($path);
          $errors=0;
          //iddiMySql::$_defer=true;
          while($f=readdir($d)) {
            if (substr($f,-4)=='.xml') {
                echo "<li>Compiling {$f}";
                echo "<script>window.scroll(0,10000);</script>";
                echo str_repeat("  \n",2550);flush();
                try{
                  if(filemtime($path.$f) > time()-(60*30) || $_GET['all']==1){
                    $t=new iddiTemplate($path.$f);
                    $t->compile();
                    $t->data->destroy();
                    echo " - <span style=\"color:green;\">Success</span>";
                  }else{
                  echo " - <span style=\"color:orange;\">Ignored</span>";
                  }
                }catch(Exception $e){
                  echo " - <span style=\"color:red;\">Fail</span>";
                  echo "<ul><li style=\"color:red;\">There is an error in template {$f} ".$e->code."</li></ul>";
                  $errors++;
                }
                echo "</li>";
                echo "<script>window.scroll(0,10000);</script>";
                echo str_repeat("  \n",2550);flush();
            }
          }
          //iddiMySql::ProcessQueue();
          //Compile the resourse files
          $path='templates/';
          $d=opendir($path);
          //iddiMySql::$_defer=true;
          while($f=readdir($d)) {
            if (substr($f,-4)=='.xml') {
                try{
                    $t=new iddiTemplate($path.$f);
                    $t->compile();
                    $t->data->destroy();
                }catch(Exception $e){
                  die($e);
                }

            }
          }

          echo '<hr/><strong>Compile Completed '.$errors.' errors found</strong>';
          echo "<script>window.scroll(0,10000);</script>";          
      }
    }
