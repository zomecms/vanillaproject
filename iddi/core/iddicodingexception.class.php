<?php
    /**
    * iddiCodingException Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiCodingException extends iddiException{}
    /**
    * @desc Template exceptions can be handled by template or request
    */
   