<?php

/**
 * iddiXmlDocument Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 * */
class iddiXmlDocument extends iddiXmlNode {

    var $_node_id;
    var $children, $currentid, $documentelement, $filename, $dont_preparse=true;
    static $doctype;

    /**
     * HTML documents need slightly different output on void nodes (annoyingly)
     * This is worked out automaticlly from the doctype.
     * @var type bool
     */
    static $is_html = false;
    protected $_x, $_d, $_t, $_lasttag, $_depth, $bl = false;

    function __construct($data = null, $template = null) {
        $this->_node_id = iddi::get_next_node_id();
        $this->setTemplate($template);
        if ($data != null)
            $this->Load($data);
    }

    function Load($data) {
        if (substr_count($data, 'DOCTYPE') == 0) {
            self::$doctype = 'html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"';
        } else {
            preg_match_all('/<!DOCTYPE(.*?)>/', $data, $matches, PREG_SET_ORDER);
            self::$doctype = trim($matches[0][1]);
        }

        if (strtolower(self::$doctype) == 'html') {
            self::$is_html = true;
        }

        $this->bl = true;
        $this->_x = xml_parser_create_ns();
        xml_set_object($this->_x, $this);
        xml_set_element_handler($this->_x, 'tag_open', 'tag_close');
        xml_set_character_data_handler($this->_x, 'cdata');
        xml_parse($this->_x, $data);
        if ($this->_depth != 0) {
            throw new iddiException('Tag ' . $this->_lasttag . ' is not closed properly in file ' . $this->_t->filename . ' on line ' . $this->lineno . ', parsing was stopped', 'iddi.xml.tagnotclosed', $this);
        }
        if (!$this->documentelement) {
            throw new iddiException('No Document Element', 'iddi.xmldocument.setdatasource.nodocumentelement', $this);
        }
        if(!$this->dont_preparse) $this->documentelement->preparse();
        return $this;
    }

    function LoadFile($filename) {
        $loaddata = true;
        $this->filename = $filename;
        @mkdir('templatecache');
        $cname = 'templatecache/' . base64_encode($filename) . '.ich';
        if (!file_exists($filename))
            throw new iddiexception('File ' . $filename . ' not found', 'iddi.xmldocument.loadfile.filenotfound');

        if (IDDI_USE_TEMPLATE_CACHE_MODE == IDDI_CACHE_MODE_FILESYSTEM && file_exists($cname)) {
            if (filemtime($cname) > filemtime($this->filename)) {
                $d = file_get_contents($cname);
                $this->documentelement = unserialize($d);
                $loaddata = false;
            }
        }
        if (IDDI_USE_TEMPLATE_CACHE_MODE == IDDI_CACHE_MODE_DATABASE) {
            $this->data = iddiMySql::get_template($this->filename);
            if ($this->data != null)
                $loaddata = false;
        }


        if ($loaddata) {
            if ($data == '')
                $data = file_get_contents($this->filename);
            $this->Load($data);
            if (IDDI_USE_TEMPLATE_CACHE_MODE == IDDI_CACHE_MODE_FILESYSTEM) {
                @mkdir(IDDI_FILE_PATH . 'templatecache');
                @chmod(IDDI_FILE_PATH . 'templatecache', 0777);
                $stor = serialize($this->documentelement);
                $fp = fopen($cname, 'w');
                fwrite($fp, $stor);
                fclose($fp);
            }
            if (IDDI_USE_TEMPLATE_CACHE_MODE == IDDI_CACHE_MODE_DATABASE) {
                $this->data = $this->documentelement;
                iddiMySql::cache_template($this);
            }
        }

        return $this;
    }

    function filteredLoad($filename, $xpath) {
        $domobj = new DOMDocument();
        $domobj->load($filename);
        $x = new DOMXPath($domobj);
        $r = $x->query($xpath);
        foreach ($r as $n) {
            $this->convertnode($n, $this);
        }
    }

    function convertnode(DomNode $node, $parentnode) {
        //        $newnode=new DomNodeSub($this,$parentnode);
        if ($node->nodeName == '#text') {
            //            $newnode=new iddiXmlCData($this,$node->nodeName,$parentnode,null,$this->nodeValue);
        } else {
            $objname = 'iddixml' . str_replace(iddi::IDDI_NAMESPACE, 'iddi_', str_replace('-', '_', $node->nodeName));
            $objname = (class_exists($objname)) ? $objname : 'iddixmlnode';
            $newnode = new $objname();
            $newnode->setup($this, $node->nodeName, $parentnode, null);
            $newnode->setValue($node->nodeValue);
            if ($node->childNodes) {
                foreach ($node->childNodes as $child) {
                    $this->convertnode($child, $newnode);
                }
            }
        }
    }

    function setDataSource($datasource) {
        if (!$this->documentelement)
            throw new iddiException('No Document Element', 'iddi.xmldocument.setdatasource.nodocumentelement', $this);
        $this->_d = $datasource;
    }

    function getDataSource() {
        return $this->_d;
    }

    function is_in_body() {
        return false;
    }

    function output($datasource = null, $clean = false) {
        if ($datasource)
            $this->setDataSource($datasource);
        $this->documentelement->parse();
        //echo $this->dump(2);
        return $this->documentelement->output($clean);
    }

    function tag_open($parser, $tag, $attributes) {
        $tag = strtolower($tag);
        $this->_lasttag = $tag;
        ++$this->_depth; //++X is faster than X++
        ++$this->lineno;

        //Get the namespace out
        $tag_parts = explode(':', $tag, 3);
        if (sizeof($tag_parts) > 2) {
            $namespace = $tag_parts[0] . ':' . $tag_parts[1];
            $tag = $tag_parts[2];
        }

        /**
         * Split out the handling of iddi and html namespaces
         */
        $class_prefix = '';
        if ($namespace == iddi::IDDI_NAMESPACE)
            $class_prefix = 'xmliddi';
        if ($namespace == 'http://www.w3.org/1999/xhtml')
            $class_prefix = 'html';

        if ($class_prefix == '') {
            $newnode = new iddixmlnode();
        } else {
            $objname = 'iddi' . $class_prefix . '_' . str_replace('-', '_', $tag);
            $objname = (class_exists($objname)) ? $objname : 'iddixmlnode';
            $newnode = new $objname();
        }

        if ($newnode) {
            $newnode->setup($this, $tag, $this->currentnode, $attributes);

            $newnode->lineno = $this->lineno;
            if (method_exists($newnode, 'opennode'))
                $newnode->opennode();
            ///              $newnode->parentid=($this->currentnode)?$this->currentnode->_node_id:0;
            //if ($this->currentnode) $this->currentnode->appendchild($newnode);
            if (!$this->documentelement) {
                $this->documentelement = $newnode;
                $this->documentelement->parent = $this;
            }
            $this->currentnode = $newnode;
        }
    }

    function cdata($parser, $cdata) {
        //Create a new #text node
        $cdata2 = trim($cdata);
        if ($cdata2 != "\n" && $cdata2 != '') {
            $n = new iddiXmlCData($this, '#text', $this->currentnode, null, $cdata);
            $this->lineno+=substr_count($cdata, "\n");
        }
    }

    function tag_close($parser, $tag) {
        $this->_depth--;
        if (method_exists($this->currentnode, 'closenode'))
            $this->currentnode->closenode();
        $this->currentnode = $this->currentnode->parent;
    }

    function xpath($xpath, $includeiddinamespace = false, $resultset = null) {
        if (!$this->documentelement)
            throw new iddiException('No document element', 'iddi.xml.document.nodocumentlement');
        return $this->documentelement->xpath($xpath, $includeiddinamespace, $resultset);
    }

    function setTemplate($template) {
        $this->_t = $template;
        return $this;
    }

    function getTemplate() {
        return $this->_t;
    }

}
