<?php
    /**
    * iddiDatabaseBase Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiDatabaseBase{
      static function builddatabase(){
          echo '<li>Update 212';
          //Remove some folders that were created by accident
          @rmdir('IDDI_FULL_CACHE_PATH');
          @rmdir('IDDI_PARTIAL_CACHE_PATH');

          //Change the location of some files
          //Images
          echo "<li>Copying ".(IDDI_FILE_PATH.'../images').' to '.IDDI_IMAGES_PATH;
          if(iddiUtils::copyDir(IDDI_FILE_PATH.'../images',IDDI_IMAGES_PATH)){
            echo ' - success';
          }else{
            echo ' - fail';
          }
          //Plugins and Config
          $d=opendir(IDDI_PROJECT_PATH);
          while($f=readdir($d)){
            if(is_file(IDDI_PROJECT_PATH.'/'.$f)){
              if(substr($f,-14)=='iddiconfig.xml'){
                if(copy(IDDI_PROJECT_PATH.'/'.$f,IDDI_CONFIG_PATH.'/'.$f)){
                  unlink(IDDI_PROJECT_PATH.'/'.$f);
                }
              }
              if(substr($f,-4)=='.php'){
                if(copy(IDDI_PROJECT_PATH.'/'.$f,IDDI_PLUGINS_PATH.'/'.$f)){
                  unlink(IDDI_PROJECT_PATH.'/'.$f);
                }
              }
            }
          }
          echo '<li>.iddi files become .xml files</li>';
          $d=opendir(IDDI_TEMPLATES_PATH);
          while($f=readdir($d)){
            if(substr($f,-5)=='.iddi'){
              $target=str_replace('.iddi','.xml',$target);
              if(@copy(IDDI_TEMPLATES_PATH.'/'.$f,IDDI_TEMPLATES_PATH.'/'.$target)){
                unlink(IDDI_TEMPLATES_PATH.'/'.$f);
              }
            }
            if(substr($f,-5)=='.html'){
              $target=str_replace('.html','.xml',$target);
              if(@copy(IDDI_TEMPLATES_PATH.'/'.$f,IDDI_TEMPLATES_PATH.'/'.$target)){
                unlink(IDDI_TEMPLATES_PATH.'/'.$f);
              }
            }
          }
          echo '<li>Checking core database structure';
          echo '<li>Entity Table';
          //Table for entities
          $etable=new iddiMySqlTable('sysentities');
          $etable->addField(new iddiMySqlTableField('name','VARCHAR','255'));
          $etable->addField(new iddiMySqlTableField('tidyname','VARCHAR','255'));
          $etable->addIndex(new iddiMySqlUniqueKey('name_unique','name'));
          $etable->build();
          //Table for templates
          echo '<li>Templates Table';
          $ttable=new iddiMySqlTable('systemplates');
          $ttable->addField(new iddiMySqlTableField('name','VARCHAR','255'));
          $ttable->addField(new iddiMySqlTableField('title','VARCHAR','255'));
          $ttable->addField(new iddiMySqlTableField('description','TEXT'));
          $ttable->addField(new iddiMySqlTableField('parentid','INT','11'));
          $ttable->addField(new iddiMySqlTableField('cache','LONGTEXT'));
          $ttable->addIndex(new iddiMySqlUniqueKey('name_unique','name'));
          $ttable->build();
          //Entity Template Join Table
          echo '<li>Entity Templates Table';
          $jtable=new iddiMySqlTable('sysjoin_entity_template');
          $jtable->addField(new iddiMySqlTableField('entityname','VARCHAR','255'));
          $jtable->addField(new iddiMySqlTableField('templatefilename','VARCHAR','255'));
          $jtable->addIndex(new iddiMySqlUniqueKey('unique',array('entityname','templatefilename')));
          $jtable->build();
          //Index of virtual filenames
          echo '<li>Filenames Table';
          $vtable=new iddiMySqlTable('sysfilenames');
          $vtable->addField(new iddiMySqlTableField('virtualfilename','VARCHAR',255));
          $vtable->addField(new iddiMySqlTableField('created','DATETIME'));
          $vtable->addField(new iddiMySqlTableField('modified','DATETIME'));
          $vtable->addField(new iddiMySqlTableField('owner','INT',11));
          $vtable->addField(new iddiMySqlTableField('lastchangedby','INT',11));
          $vtable->addField(new iddiMySqlTableField('entityname','VARCHAR',255));
          $vtable->addField(new iddiMySqlTableField('pagetitle','VARCHAR',255));
          $vtable->addField(new iddiMySqlTableField('parentid','INT',11));
          $vtable->addField(new iddiMySqlTableField('entityid','INT',11));
          $vtable->addField(new iddiMySqlTableField('language','VARCHAR',5));
          $vtable->addField(new iddiMySqlTableField('rootlanguageitemid','INT',11));
          $vtable->addField(new iddiMySqlTableField('templatefile','VARCHAR','255'));
          $vtable->addField(new iddiMySqlTableField('redirect','VARCHAR','255'));
          $vtable->addField(new iddiMySqlTableField('deleted','TINYINT',1));
          $vtable->addField(new iddiMySqlTableField('locked','TINYINT',1));
          $vtable->addField(new iddiMySqlTableField('odr','INT',11));
          $vtable->addIndex(new iddiMySqlUniqueKey('virtualfilesname','virtualfilename'));
          $vtable->build();
          //Index Table
          echo '<li>Index Table';
          $itable=new iddiMySqlTable('sysfullindex');
          $itable->addField(new iddiMySqlTableField('entityid','INT','11'));
          $itable->addField(new iddiMySqlTableField('entityname','VARCHAR','255'));
          $itable->addField(new iddiMySqlTableField('fieldname','VARCHAR','255'));
          $itable->addField(new iddiMySqlTableField('value','VARCHAR','255'));
          $itable->addIndex(new iddiMySqlUniqueKey('unique',array('entityid','fieldname')));
          //$itable->addIndex(new iddiMySqlIndex('search','value'));
          $itable->build();
          //Widget holders
          echo '<li>Widget Holders Table';
          $wtable=new iddiMySqlTable('syswidget_holders');
          $wtable->addField(new iddiMySqlTableField('entityid','INT',11));
          $wtable->addField(new iddiMySqlTableField('entityname','VARCHAR','255'));
          $wtable->addField(new iddiMySqlTableField('areaname','VARCHAR',255));
          $wtable->addField(new iddiMySqlTableField('subentityid','INT',11));
          $wtable->addField(new iddiMySqlTableField('position','INT',11));
          $wtable->build();
          //Fields
          echo '<li>Fields Table';
          $vtable=new iddiMySqlTable('sysentityfields');
          $vtable->addField(new iddiMySqlTableField('entityname','VARCHAR',250));
          $vtable->addField(new iddiMySqlTableField('fieldname','VARCHAR',250));
          $vtable->addField(new iddiMySqlTableField('type','VARCHAR',250));
          $vtable->addField(new iddiMySqlTableField('format','VARCHAR',250));
          $vtable->addField(new iddiMySqlTableField('group','VARCHAR',250));
          $vtable->addField(new iddiMySqlTableField('maxlength','INT',11));
          $vtable->addField(new iddiMySqlTableField('minlength','INT',11));
          $vtable->addField(new iddiMySqlTableField('hidden','INT',1));
          $vtable->addField(new iddiMySqlTableField('lookup','VARCHAR',250));
          $vtable->addField(new iddiMySqlTableField('help','VARCHAR',250));
          $vtable->addField(new iddiMySqlTableField('otherparams','TEXT',2500));
          $vtable->addField(new iddiMySqlTableField('caption','VARCHAR',250));
          $vtable->addIndex(new iddiMySqlUniqueKey('unique',array('entityname','fieldname')));
          $vtable->build();
          //Metric Monitoring
          echo '<li>Metrics Table';
          $wtable=new iddiMySqlTable('sysmetrics');
          $wtable->addField(new iddiMySqlTableField('datetime','DATETIME'));
          $wtable->addField(new iddiMySqlTableField('entityid','INT',11));
          $wtable->addField(new iddiMySqlTableField('requesturi','VARCHAR','255'));
          $wtable->addField(new iddiMySqlTableField('agent','VARCHAR',255));
          $wtable->addField(new iddiMySqlTableField('loadtime_ms','INT',11));
          $wtable->addField(new iddiMySqlTableField('peakmemory','INT',11));
          $wtable->addField(new iddiMySqlTableField('queries','INT',11));
          $wtable->addField(new iddiMySqlTableField('templatefile','VARCHAR',255));
          $wtable->addField(new iddiMySqlTableField('dbtime_ms','INT',11));
          $wtable->build();

          echo '<li>Update 213 - Set rootlanguage item id';
          iddiMySql::query('UPDATE {PREFIX}sysfilenames SET rootlanguageitemid=id WHERE `language`=\'en-gb\'');
 
      }


    }
   