<?php

/**
 * iddiImage_Kraken image handler
 *
 * Used by iddiImage class when kraken config has been entered
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiImageKraken extends iddiEvents {

    var $inputfilename;
    var $kraked_url;

    function __construct($inputfilename, $outputheight = 0, $outputwidth = 0, $scalemode = 'crop', $preventupscale = false) {
        $this->inputfilename = $inputfilename;
    }

    /**
     *
     * @param type $outputheight
     * @param type $outputwidth
     * @param type $scalemode
     * @param type $preventupscale
     * @return boolean
     */
    function resize($outputheight, $outputwidth, $scalemode = 'crop', $preventupscale = false) {
        /**
         * 1. Connect to Kraken
         * 2. Process the file
         * 3. Store the result url from kraken to be used by ->save()
         */
        $kraken = new Kraken(iddiConfig::GetValue('kraken', 'api-key'), iddiConfig::GetValue('kraken', 'api-secret'));


        //Resizing Strategy
        switch ($scalemode) {
            case 'stretch':
                //Stretch just whacks it in - really this should not be used, if you use it you're more than likely an idiot.
                //Retains size - no
                //Retains aspect - no
                //Retains entire image - yes
                //Adds bars - no
                $strategy = 'exact';
                break;
            case 'fit':
                //Fits it into the desired image, will add bars to one dimension
                //Shrink the image by the largest difference
                //Retains size - no
                //Retains aspect - yes
                //Retains entire image - yes
                //Adds bars - yes
                $strategy = 'fill';
                break;
            case 'bestfit':
            case 'force':
                //This will do a fit, but then reduce the size of the output image instead of black bars
                //Shrink the image by the largest difference
                //Retains size - no
                //Retains aspect - yes
                //Retains entire image - yes
                //Adds bars - no
                $strategy = 'auto';
                break;
            default:
                //This is the best option really;
                //Shrink the image by the largest difference - but don't increase the image size.
                //Retains size - no
                //Retains aspect - yes
                //Retains entire image - yes
                //Adds bars - no
                $strategy = 'fit';
                break;
        }


        $params = array(
            # Can be a file or URL
            "file" => __DIR__.'/../'.$this->inputfilename,
            # Basically aysnc / sync
            "wait" => true,
            # Compressess beyond lossless
            "lossy" => true,
            "quality" => 90,
            # WebP format
            "webp" => false,
            "resize" => array(
                "width" => (int)$outputwidth,
                "height" => (int)$outputheight,
                "strategy" => $strategy
            )
        );

        # Send to Kraken
        $data = $kraken->upload($params);

        # Read result
        if ($data['success']) {
            # We were valid, but no URL so no further optimisation possible
            if (!empty($data['kraked_url'])) {
                # Store new optimised image
                $this->kraked_url = $data['kraked_url'];
            }
            return true;
        }
    }

    function destroy(){}

    function save($filename, $quality = 90) {
            if(substr($filename, 0, strlen(IDDI_IMAGES_PATH)) !== IDDI_IMAGES_PATH){
                iddiDebug::warning($filename.' is not in the images folder. Being replaced with no-pic to allow the CMS to continue without error','core.imagegd.invalidimagelocation');
                $fileparts=pathinfo($filename);
                $filename=IDDI_IMAGES_PATH.'/public/general/'.$fileparts[PATHINFO_BASENAME];
            }
        $p = pathinfo($filename);
        $d = $p['dirname'];
        $paths = explode('/', $d);
        $p = '';
        foreach ($paths as $p1) {
            $p.=$p1 . '/';
            if ($p1 != '..' && $p1 != '.') {
                @mkdir($p);
            }
        }
        if(substr($filename, 0, strlen(IDDI_IMAGES_PATH)) === IDDI_IMAGES_PATH) 
            @chmod($p, 0777);
        //echo "<li>Saving $filename";
        if (iddi::$debug)
            iddiDebug::message('Saving ' . $filename);

        copy($this->kraked_url, $filename);
    }

}
