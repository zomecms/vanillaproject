<?php
    /**
    * iddiIterator Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiIterator extends iddiDataSource implements Iterator  {
        protected $items;
        public function __construct($array=null)    { if(is_array($array)) $this->items=$array; }
        public function __destruct()                { $this->items=array();  unset($this->items); }
        public function rewind()                    { if ($this->items) reset($this->items); }
        public function current()                   { if ($this->items) return current($this->items); }
        public function key()                       { if ($this->items) return key($this->items); }
        public function next()                      { if ($this->items) return next($this->items); }
        public function valid()                     { if ($this->items) return $this->current() !== false; }
        public function append($item)               { $this->items[]=$item;}
        public function first(){if($this->items) foreach($this->items as $i)return $i;}
        public function count(){return count($this->items);}
    }
   