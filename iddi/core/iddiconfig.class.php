<?php

/**
 * iddiConfig Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 * */
class iddiConfig {

    var $config;
    static $filename;
    
    /**
     * When uploading files to the library do we want to overwrite files with
     * the same name, or generate new filenames.
     * @var type bool
     */
    static $overwriteUploads = false;
    static $_config;

    static function Get() {
        if (!self::$_config)
            self::Load();
        return self::$_config;
    }

    static function GetValue($section, $value, $default = null) {
        if (!self::$_config)
            self::Load();
        if (!self::$_config->$section) {
            if ($default === null)
                throw new iddiException($section . ' section dosn\'t exist in config file');
            return $default;
        }
        if (!self::$_config->$section->$value) {
            if ($default === null)
                throw new iddiException($value . ' value dosn\'t exist in config file');
            return $default;
        }
        return (string) self::$_config->$section->$value;
    }

    static function Load() {
        if (!file_exists(IDDI_CONFIG_PATH . '/iddiconfig.xml')) {
            @copy(IDDI_FILE_PATH . 'iddiconfig.xml', IDDI_CONFIG_PATH . '/iddiconfig.xml');
            @unlink(IDDI_FILE_PATH . 'iddiconfig.xml');
        }
        $configfilename = $_SERVER['SERVER_NAME'] . '.iddiconfig.xml';
        if (!file_exists(IDDI_CONFIG_PATH . '/' . $configfilename)) {
            @copy(IDDI_CONFIG_PATH . '/iddiconfig.xml', IDDI_CONFIG_PATH . '/' . $configfilename);
        }
        if (file_exists(IDDI_CONFIG_PATH . '/iddiconfig.xml'))
            $config_filename = IDDI_CONFIG_PATH . '/' . $configfilename;
        else if (file_exists('iddiconfig.xml'))
            $config_filename = 'iddiconfig.xml';
        
        self::$filename=$config_filename;
        
        $data = file_get_contents($config_filename);
        $n = new iddiconfig();
        if ($data)
            $n->config = new SimpleXMLElement($data);
        self::$_config = $n->config;
        return $n;
    }

    /**
     * @desc Saves the current config option back to the config file
     */
    static function Save() {
        self::$_config->asXml(self::$filename);
    }

    public static function get_admin_url() {
        return self::GetValue('site', 'adminurl', '/admin');
    }

}
