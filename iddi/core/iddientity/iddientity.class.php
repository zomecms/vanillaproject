<?php
    /**
    * iddiEntity Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiEntity extends iddiDataSource{
        const BEFORELOAD='BeforeLoad',AFTERLOAD='AfterLoad',LOAD_FAILED='LoadFailed',BEFORE_SAVE='BeforeSave',AFTER_SAVE='AfterSave';
        var $fielddefs,$deleted=0;

        function __construct($entityname='',$id=''){
          if($entityname!='' && $id==''){
            $this->loadByVirtualFilename($entityname);
          }elseif($id!=''){
            $this->loadById($entityname,$id);
          }

        }

        function loadByVirtualFilename($invirtualfilename){
            $e=$this->trigger(self::BEFORELOAD);
            if (!$e->cancelled)
            {
                try{
                    iddiMySql::loadpagebyvf($invirtualfilename,$this);
                    $this->trigger(self::AFTERLOAD);
                }catch(iddiCodingException $e){
                    $this->trigger(self::LOAD_FAILED);
                    throw $e;
                }
            }
        }
        /**
        * @desc Load an entity by its type and id
        * @param string $inEntityName The name of the entity i.e. 'webpage'
        * @param int $inPageId The Id of the entity to load
        * @return iddiEntity
        * @deprecated since version 3 - use get_by_id instead - basically the same but entity name is second
        */
        function loadById($inEntityName,$inPageId){
            $e=$this->trigger(self::BEFORELOAD);
            if (!$e->cancelled)
            {
                if($inEntityName==''){
                    $inEntityName=iddiMySql::getEntityType($inPageId);
                }
                $this->entityname=$inEntityName;
                $this->entityid=$inPageId;
                iddiMySql::loadpagebyid($inEntityName,$inPageId,$this);
                $this->trigger(self::AFTERLOAD);
            }
        }

        /**
         * Replacement for loadById - actually is an alias for the original, for
         * tracked entities. For untracked entities second argument is optional
         * @param type $page_id
         * @param type $entity_name
         */
        function get_by_id($page_id,$entity_name=null){
            $this->loadById($entity_name,$page_id);
        }

        function setData($inData){
            if($inData){
                foreach($inData as $k=>$v){
                    $this->$k=$v;
                }
            }
        }



        function xmlFunction_parent(){
            $r=new iddiXpathResultSet();
            if($this->parentid>0){
	          $x=new iddiEntity('',$this->parentid);
              $r->addResult($x);
            }
            return $r;
        }
        function xmlFunction_created($params){
          require_once(IDDI_ROOT.'/iddi/date-functions.lib.php');
          $format=$params[0];
          if($format=='') $format='jS F Y';
          return iddiXpathResultSet::quickResult('created',date_lang($format,strtotime($this->created)));
        }

    }
