<?php
    /**
    * iddiDataSource Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiDataSource extends iddiXmlNode{
      const START_NEW='Start_New',CHANGE_ENTITY_NAME='Change_Entity_Name';
      const BEFORELOAD='BeforeLoad',AFTERLOAD='AfterLoad',LOAD_FAILED='LoadFailed',BEFORE_SAVE='BeforeSave',AFTER_SAVE='AfterSave';

      var $fields,$template,$virtualfilename,$basefilename,$id,$modified,$created,$templatefile,$changed=false,$entityname='',$entityid,$currentnode,$parentid,$_e,$fieldname,$fieldvalue,$pchildren,$datasource,$rootlanguageitemid=0,$parentpageid,$odr,$link;
      var $dbfields=array('pagetitle'=>'');

      var $accesslevels;
      
      static $_next_entity_id=-1;

      var $is_tracked=true; //True if this entity is tracked in the master sysfilenames table.

      function __construct(){
      }

      function __set($k,$v){
          $this->fields[$k]=$v;
          if(array_key_exists($k,$this->dbfields)) $this->dbfields[$k]=$v;
          $this->changed=true;
          //$j=new iddiJavascriptEvent('SetPageValue',array('key'=>$k,'value'=>$v));
      }
      function __get($k){
          return $this->fields[$k];
      }

      function output($clean=false,$level=0){
            //$output.=$this->value;
            if ($this->children) foreach($this->children as $child) $output.=$child->output($clean,($clean)?$level+1:0);
            return $output;
      }


      function xmlFunction_formfieldid($params){
            $fieldname=$this->processCurlyBrackets($params[0]);
            //$fieldname=$this->resolveValue($params[0]);
            iddiDebug::message('Getting fieldref for '.$params[0]);
            $r=new iddiXpathResultSet();
            $x=new iddiXmlNode();
            $f=iddiRequest::getform()->findfield($this->id,$fieldname);
            $x->value='iddifield_'.$f->postfieldid;
            if (method_exists($this,'getAttribute')){
                $parentnodeidattr=$this->getAttribute('ID');
                if ($parentnodeidattr=='') $parentnodeidattr=$this->setAttribute('id','iddinode_'.$this->id);
                iddiRequest::getform()->linkVar($parentnodeidattr,$f);
            }
            $r->addResult($x);
            return $r;
      }
      function setEntityName($entityname){
          $this->entityname=$entityname;
          $this->trigger(self::CHANGE_ENTITY_NAME);
      }
      function setValue($fieldname,$value){
          //See if this value needs processing
          if($value!=$this->$fieldname){

          }
          $this->$fieldname=$value;
          $this->fields[$fieldname]=$value;
          $this->dbfields[$fieldname]=$value;
//          $j=new iddiJavascriptEvent('SetPageValue',array('key'=>$fieldname,'value'=>$value));

          if ($fieldname=='templatefile') $j=new iddiJavascriptEvent('TemplateChanged',array('key'=>$fieldname,'value'=>$value));
          return $this;
      }
      function setDbValue($fieldname,$value,$create=false){
          if($create) $this->dbfields[$fieldname]=$value;
          $this->$fieldname=$value;
      }
      function getDbValues(){
          return $this->dbfields;
      }
      function getValue($fieldname){
          return $this->$fieldname;
      }
      function compile(){
          //iddi::Log('Compile : Skipping data source node',0);
          if ($this->children) foreach($this->children as $child) $child->compile();
      }
      function getDataSource(){
          return $this;
      }
      function addField($field){
           $this->fields[$field->getName()]=$field;
       }
      function getChildren(){
          foreach($this->fields as $fieldname=>$fieldvalue){
              $n=new iddiXmlNode($this,$fieldname,$this,null,$fieldvalue);
              $n->preventoutput=true;
              $r[]=$n;
          }
          foreach($this->dbfields as $fieldname=>$fieldvalue){
              $n=new iddiXmlNode($this,$fieldname,$this,null,$fieldvalue);
              $n->preventoutput=true;
              $r[]=$n;
          }          
          return $r;
      }

        function deep_copy($used=array()){
          if($this->id>0){
            iddiDebug::message('Deep copy on '.get_class($this).' '.$this->id);
            //Make copies of all children
            $children=$this->get_child_entities();
            $this->copy();
            if($children->hasData()){
              foreach($children as $child_s){
                $used[]=$childs->id;
                try{
                  $child=new iddiEntity('',$child_s->id);
                  $child->parentid=$this->id;
                  $child->virtualfilename=$this->virtualfilename.'/'.strtolower(preg_replace('/[^A-Za-z0-9]/','_',$child_s->pagetitle));
                  $child->deep_copy($used);
                }catch(Exception $e){

                }
              }
            }
          }
        }
        function copy(){
          $this->id=-1;
          $this->saveme();
          $this->rootlanguageitemid=$this->id;
          $this->saveme();
        }
        function get_child_entities(){
          $sql="SELECT * FROM {PREFIX}sysfilenames WHERE parentid=".$this->id." AND deleted is null";
          $result=iddiMySql::query($sql);
          return $result;
        }
        function saveme(){
            $e=$this->trigger(self::BEFORE_SAVE);
            if (!$e->cancelled){
                try{
                    iddiMySql::saveEntity($this);
            //Finally check that all data is accessible
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET rootlanguageitemid=id');
            iddiMySql::query('UPDATE {PREFIX}sysfilenames SET deleted=0 WHERE deleted is null');

                    $this->trigger(self::AFTER_SAVE);
                }catch(iddiCodingException $e){

                    throw $e;
                }
            }
        }
        function save(){
          return $this->saveme();
        }
        function rename($inNewName){
            $this->virtualfilename=$inNewName;
            $this->save();
        }
        function delete(){   
            $tablename=iddiMySql::tidyname($this->entityname);
            $sql='DELETE FROM `{PREFIX}sysfilenames` WHERE entityname="'.$this->entityname.'" id='.$this->id;
            iddiMySql::query($sql);
            $sql='DELETE FROM `{PREFIX}'.$tablename.'` WHERE id='.$this->id;
            iddiMySql::query($sql);            
        }
        function restore(){
            //iddiMySql::restoreEntity($this);
        }
                /**
        * @desc Gets an array of all fields plus all non array and object based object variables - This is used for dumping data to the database
        */
        function getAllFields(){
            $outData=array();
            foreach($this->fields as $k=>$v){
                $outData[$k]=$v;
            }
            foreach(get_object_vars($this) as $k=>$v){
                if(!is_array($v) && !is_object($v)) $outData[$k]=$v;
            }
            return $outData;
        }

      function xmlFunction_if_in_path($params){
          $classname=$this->processCurlyBrackets($params[0]);
          $r=new iddiXpathResultSet();
          $v1=iddiRequest::$current->page->basefilename;
          $v2=$this->basefilename;
          if((substr($v1,0,strlen($v2))==$v2 && strlen($v2)>1) || $v2==$v1) {
            $x=new iddiXmlnode();
            $x->value=$classname;
          }
          $r->addResult($x);
          return $r;
        }

      function xmlFunction_if_path_in_current($params){
          $path=$this->processCurlyBrackets($params[0]);
          $classname=$this->processCurlyBrackets($params[1]);
          $r=new iddiXpathResultSet();
          $v1=iddiRequest::$current->page->basefilename;
          $v2=str_replace('|','/',$path);
          if((substr($v1,0,strlen($v2))==$v2 && strlen($v2)>1) || $v2==$v1) {
            $x=new iddiXmlnode();
            $x->value=$classname;
          }
          $r->addResult($x);
          return $r;
        }
      function  xmlFunction_orbatcrop($params){
          $data=$this->processCurlyBrackets($params[0]);
          $r=new iddiXpathResultSet();
          $x=new iddiXmlnode();
          if(substr_count($data,' ')>0){
            $b=ucwords($data);
            $b=preg_replace('/[^A-Z0-9]/','',$b);
            $x->value=$b;
          }else{
            if(strlen($data)>3){
              $x->value=substr($data,0,3);
            }else{
              $x->value=$data;
            }
          }
          $r->addResult($x);
          return $r;
      }
      function xmlFunction_if_open($params){
          $classname=$this->processCurlyBrackets($params[0]);
          $r=new iddiXpathResultSet();
          if($this->getValue('event1')!=''){
            $x=new iddiXmlnode();
            $x->value=$classname;
            $r->addResult($x);
          }
          return $r;
      }
      function xmlFunction_if_part_open($params){
          $classname=$this->processCurlyBrackets($params[0]);
          $r=new iddiXpathResultSet();
          if($this->getValue('event1')!='' || $this->getValue('event2')!='' || $this->getValue('event3')!='' || $this->getValue('event4')!=''){
            $x=new iddiXmlnode();
            $x->value=$classname;
            $r->addResult($x);
          }
          return $r;
      }

    }
