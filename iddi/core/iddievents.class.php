<?php
    /**
    * iddiEvents Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiEvents extends iddiBase {
        protected $_h;
        static $_gh;
      /**
      * @desc Adds a handler to this event
      ** @param object $object the target object that contains the handler - normally would be $this
      * @param string $method the method name on the target object to be called
      */
      function addHandler($eventname, $object, $method) { $this->_h[$eventname][]=new iddiEventHandler($object,$method); }
      /**
      * @desc Adds a global handler - these can listen for an event on ANY object - we can create these before instantiating an object, preventing the need for event bubbling
      * @param string $classname the class that we want to listen on
      * @param object $eventname the name of the event
      * @param object $object the target object that contains the handler - normally would be $this
      * @param string $method the method name on the target object to be called
      */
      static function addGlobalHandler($classname,$eventname,$object,$method){
        self::$_gh[$classname][$eventname][]=new iddiEventHandler($object,$method);
      }
      /**
      * @desc Triggers this event and sends itself to each of the event handlers listening for it
      */
      function trigger($eventname,$event=null,$c=null) {
          //if (iddi::$debug) iddiDebug::message("Triggering event {$c}::{$eventname}");
          if (!$event) $event=new iddiEvent($this);
          if($c===null){
              $c=get_class($this);
              if ($this->_h[$eventname])foreach($this->_h[$eventname] as $h) $h->trigger($event);
          }
          //Pass to any global event handlers
          if (self::$_gh[$c][$eventname]) foreach(self::$_gh[$c][$eventname] as $h){
            $h->trigger($event,$eventname);
          }
          //If we are in a subclass pass up for the global event handlers, otherwise listeners listening on a parent class will not recieve the event
          $parentclass=get_parent_class($c);
          if($parentclass!=null){
              $this->trigger($eventname,$event,$parentclass);
          }
          return $event;
      }
      function triggerGlobal($eventname,$c,$event=null){
          if($c!=null){
              //if (iddi::$debug) iddiDebug::message("Triggering global event {$c}::{$eventname}");
              if (!$event) $event=new iddiEvent($sourceobject);
              if (self::$_gh[$c][$eventname]) foreach(self::$_gh[$c][$eventname] as $h){
                $h->trigger($event);
              }
              $parentclass=get_parent_class($c);
              if($parentclass!=null){
                  self::triggerGlobal($eventname,$parentclass,$event);
              }
              return $event;
          }
      }
      function listHandlers(){return $_h;}
      function hasObservers(){
          if ($this->_h) return true;
          if (self::$_gh[get_class($this)]) return true;
          return false;
      }
    }
