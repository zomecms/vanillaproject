<?php
    /**
    * iddiEntity Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiModel extends iddiEntity{
        const BEFORELOAD='BeforeLoad',AFTERLOAD='AfterLoad',LOAD_FAILED='LoadFailed',BEFORE_SAVE='BeforeSave',AFTER_SAVE='AfterSave';
        var $fielddefs,$deleted=0;
        var $entityname,$documentelement;

        function __construct($id=''){
          if($id!=''){
            $this->get_by_id($id);
          }
        }

        public function __sleep()
        {
            return array_diff(array_keys(get_object_vars($this)), array('documentelement','children'));
        }

        function loadByVirtualFilename($invirtualfilename){
            $e=$this->trigger(self::BEFORELOAD);
            if (!$e->cancelled)
            {
                try{
                    iddiMySql::loadpagebyvf($invirtualfilename,$this);
                    $this->trigger(self::AFTERLOAD);
                }catch(iddiCodingException $e){
                    $this->trigger(self::LOAD_FAILED);
                    throw $e;
                }
            }
        }

        function get_by_id($entity_id){
            $e=$this->trigger(self::BEFORELOAD);
            if (!$e->cancelled)
            {
                $this->entityid=$entity_id;
                iddiMySql::load_model_by_id($this->entityname,$entity_id,$this);
                $this->trigger(self::AFTERLOAD);
            }
        }

        function saveme(){
            $e=$this->trigger(self::BEFORE_SAVE);
            if (!$e->cancelled){
                try{
                    iddiMySql::save_model($this);
                    $this->trigger(self::AFTER_SAVE);
                }catch(iddiCodingException $e){
                    throw $e;
                }
            }
        }

    }
