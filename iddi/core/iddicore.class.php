<?php
/**
 * iddiCore Class file
 * This class is here to provide core functionality. At the moment it just provides
 * event triggerring functionality using iddiEvents and nothing else
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 **/
class iddiCore extends iddiEvents {}
