<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard extends iddiXmlIddiNode {
    var $components=array();
    
    function parse() {
        if (!$_SESSION['script'])
            $_SESSION['script'] = array();
        parent::parse();
    }

    function output() {
        $script = '<script>';
        foreach ($this->components as $component) {
            $script.="yendoi.dashboard.addComponent('" . $component . "',{});";
        }
        $script.='yendoi.dashboard.drawAll();';
        $script.='</script>';
        return parent::output() . $script;
    }

    function add_component($component){
        $this->components[]=$component;
    }
}
