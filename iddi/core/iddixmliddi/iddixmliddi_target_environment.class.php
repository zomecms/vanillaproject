<?php
/**
 * Allows targeting of output to specific environments only
 */

class iddiXmlIddi_Target_Environment extends iddiXmlIddi_If_Base{
    function test_if(){
        $response=true;
        if($this->getAttribute('ip')!='' && $this->getAttribute('ip')!=$_SERVER['SERVER_ADDR']) $response=false;
        if($this->getAttribute('port')!='' && $this->getAttribute('port')!=$_SERVER['SERVER_PORT']) $response=false;
        if($this->getAttribute('protocol')!='' && $this->getAttribute('protocol')!=$_SERVER['REQUEST_SCHEME']) $response=false;
        if($this->getAttribute('host')!='' && $this->getAttribute('host')!=$_SERVER['SERVER_NAME']) $response=false;
        return $response;
    }
}