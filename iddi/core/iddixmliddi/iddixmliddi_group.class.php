<?php
    /**
    * iddiXmlIddi_Group Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Group extends iddiXmlIddiNode{

    }
    /**
     * Allows a set of nodes to only be output if the current language matches the language set in the language attribute
     * If this node is part of a group and no language attribute is set it becomes the default set of nodes to output for the group
     */
   