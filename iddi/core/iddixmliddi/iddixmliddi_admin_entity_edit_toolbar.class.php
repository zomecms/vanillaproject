<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit_Toolbar extends iddiXmlIddi_Admin_Toolbar {
    
    function parse() {
        if(sizeof($this->children)==0) $this->add_default_buttons();
        parent::parse();
    }
    
    function add_default_buttons(){
        $d=$this->getParentOfType('iddiXmlIddi_Admin_Entity_Edit');
        $attrs=array('data-y-entity-name'=>$d->entity,'data-y-entity-id'=>$d->entityid);
        $this->appendChild(iddiXmlIddi_Admin_Toolbar_Button::create('fa-chevron-left', 'Back to List', 'y-action-list-entity',array('data-y-entity'=>$d->entity)));
        //$this->appendChild(iddiXmlIddi_Admin_Toolbar_Button::create('fa-save', 'Save', 'y-action-save',$attrs));
        $this->appendChild(iddiXmlIddi_Admin_Toolbar_Button::create('fa-trash', 'Delete', 'y-action-delete',$attrs));
        //$this->appendChild(iddiXmlIddi_Admin_Toolbar_Button::create('fa-lock', 'Lock', 'y-action-lock',$attrs));
    }        
}
