<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard_Flot_Pie_Base extends iddiXmlIddiNode {

    static $chartid = 1;
    var $title;
    var $subtitle;
    var $headline_prefix;
    var $colors = array("#1ab394", "#464f88",'#d3d3d3', '#bababa', '#ED5565', '#79d2c0', '#1ab394', '#23c6c8', '#f8ac59');

    function preparse() {
        $template = new iddiXmlIddi_Insert_Template();
        $template->setAttribute('source', 'flot_pie');
        $template->setAttribute('select', '//div');
        $this->appendChild($template);
        parent::preparse();
    }

    function parse() {
        $out = new iddiDataSource();

        $rs = $this->get_data();

        $running_total = 0;
        $other_total = 0;
        $total_items = 0;
        $max_items = 25;

        foreach ($rs as $row) {
            $overall_total+=$row->value;
        }

        foreach ($rs as $row) {

            if (($row->value / $overall_total > 0.05) && ($total_items < $max_items)) {

                $color = array_shift($this->colors);
                array_push($this->colors, $color);

                $dataitem = new stdClass();
                $dataitem->color = $color;
                $dataitem->label = $row->label;
                $dataitem->data = $row->value;

                $data[] = $dataitem;
                $total_items++;
            } else {
                $other_total+=$row->value;
            }
            $running_total+=$row->value;
        }

        if ($other_total > 0) {
            $dataitem = new stdClass();
            $dataitem->color = $color;
            $dataitem->label = 'Other';
            $dataitem->data = $other_total;

            $data[] = $dataitem;
        }

        $data_string = json_encode($data);

        $out->headline_prefix = $this->headline_prefix;
        $out->title = $this->title;
        $out->subtitle = $this->subtitle;
        $out->chart_name = 'flotpie' . self::$chartid++;
        $out->chart_data = $data_string;
        $this->setDataSource($out);
        parent::parse();
    }

    function get_data() {
        throw new Exception('You must implement get_data on your pie chart', 'yendoi.flot_pie.get_data_not_implementd');
    }

}
