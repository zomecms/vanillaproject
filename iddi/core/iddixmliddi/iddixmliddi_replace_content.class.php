<?php
    /**
    * iddiXmlIddi_Replace_Content Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Replace_Content extends iddiXmlIddiNode{
        function preparse(){
            $xpath=$this->getAttribute('select');
            $targets=$this->xpath($xpath,true);
            foreach($targets as $tgtnode) {
                foreach($this->children as $child){
                    $tgtnode->parent->children[$tgtnode->_node_id]=$child;
                }
            }
            //$this->parent->removeChild($this);
        }
    }
