<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Table_Field extends iddiXmlIddiNode{
    var $field;
    var $table;
        
    function add_to_table(iddiXmlIddi_Admin_Entity_Table $table){
        $this->table=$table;
        
        $fieldname=$this->attributes['NAME'];
        $this->field=$table->entity_definition->fielddefs[$fieldname];
        
        if($this->attributes['EDITABLE'])
            $this->field->is_editable=($this->attributes['EDITABLE']==='true');
                
        if(is_object($this->field) && $this->field instanceOf iddiEntityField) $table->add_field($this);
    }
    
    function get_heading_cell() {
        $field_type = $this->field->type;
        $method = 'get_heading_cell_of_type_' . $field_type;
        if (method_exists($this, $method)) {
            $new_col = $this->$method();
        } else {
            $new_col = new iddiHtml_TH();
            if ($this->field->is_key) {
                $new_col->addclass('keyfield');
                $new_col->attributes['WIDTH'] = 1;
            } else {
                $new_col->addclass($this->field->type);
                $new_col->setvalue($this->field->caption);
            }            
        }
        return $new_col;
    }

    function get_heading_cell_of_type_html() {
        return null;
    }
    
    function get_value_cell($data){
        $field_type = $this->field->type;
        $method = 'get_value_cell_of_type_' . $field_type;
        if (method_exists($this, $method)) {
            $new_col = $this->$method($data);
        } else {
            $new_col = new iddiHtml_TD();
            if ($this->field->is_key) {
                $new_col->addclass('keyfield');
            } else {
                if ($this->table->can_edit_cells() && (!isset($this->field->is_editable) || $this->field->is_editable===true))
                    $new_col->attributes['DATA-EDITABLE'] = 'true';
            }            
            $new_col->setvalue($data);            
        }        
        return $new_col;
    }
    
    function get_value_cell_of_type_html($data) {
        return null;
    }
    
    function get_value_cell_of_type_datetime($data) {
        $new_col = new iddiHtml_TD();
        $new_col->setvalue(date('d/m/Y H:i',strtotime($data)));
        return $new_col;
    }    

    function get_value_cell_of_type_date($data) {
        $new_col = new iddiHtml_TD();
        $new_col->setvalue(date('d/m/Y',strtotime($data)));
        return $new_col;
    }        
    
    function get_value_cell_of_type_lookup($data) {
        $new_col = new iddiHtml_TD();
        $entityname = iddiMySql::tidyname($this->field->lookup);
        try {
            $lookup = iddiMySql::loadpagebyid($entityname, $data, new iddiEntity());
            $new_col->value = $lookup->pagetitle;
        } catch (Exception $e) {
            $new_col->value = '-';
        }
        return $new_col;
    }
    

    function get_value_cell_of_type_action($data) {
        $new_col = new iddiHtml_TD();
        $new_col->addclass('action');
        $new_col->attributes['data-action-name']=$this->field->action;        
        $new_col->attributes['data-y-entity-id'] = $this->table->currentrow->id;
        $new_col->attributes['data-y-entity-name'] = $this->table->entity;
        //$new_col->attributes['title'] = 'Edit '.$this->table->entity.' '.$this->table->currentrow->pagetitle;
        $new_col->value = '<i class="fa ' . $this->field->icon . '"></i>';
        return $new_col;
    }

    function get_value_cell_of_type_image($data) {
        $new_col = new iddiHtml_TD();
        $img = new iddiHtml_Img();
        $img->attributes['SRC'] = '/api/image?width=40&height=40&scalemode=force&outmode=image&filename='.urlencode($data);
        $new_col->appendChild($img);
        return $new_col;
    }
    
    
    function get_filter_cell(){
        
    }
    
    function get_sorting_cell(){
        
    }
    
    function get_grouping(){
        
    }
}