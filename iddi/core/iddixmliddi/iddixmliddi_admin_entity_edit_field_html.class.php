<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit_Field_Html extends iddiXmlIddi_Admin_Entity_Edit_Field{
    function get_field($data){
        $field_name = $this->field->fieldname;        
        $data->value=$data->$field_name;

        $fielddata=new iddiDataSource();        
        $fielddata->dbfields['caption']=$this->field->caption;
        $fielddata->dbfields['fieldvalue']=$data->$field_name;
        
        $t=iddiTemplate::load_first_match('edit_field_type_html', $fielddata)->data->documentelement;    
        foreach($t->children as $child) $child->setDataSource($fielddata);
        return $t;
    }    
}