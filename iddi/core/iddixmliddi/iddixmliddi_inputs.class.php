<?php

    class iddiXmlIddi_Inputs extends iddiXmlIddiNode{
      var $fields;
      function preparse(){

        $this->fields=explode(',',$this->getAttribute('fields'));
        foreach($this->fields as $field){
            $new_node=new iddiXmlIddi_Input();
            $new_node->attributes['NAME']=$field;
            $this->appendChild($new_node);
        }
        parent::preparse();
      }
      function output(){
          if($this->children) foreach($this->children as $child) $output.=$child->output();
          return $output;
      }
    }