<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard_Sales_Volume extends iddiXmlIddi_Dashboard_Headline_Base {
    var $where='total_paid > 0';
    var $table='iddi_order';
    var $stat='SUM(id)';
    var $title='Sales Volume';
    var $subtitle='Last 30 Days';
}