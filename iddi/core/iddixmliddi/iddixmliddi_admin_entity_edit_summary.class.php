<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit_Summary extends iddiXmlIddiNode{
    var $field;
    var $form;
           
    function get_field($data){        
        $field_type = $this->field->type;
        $field_name = $this->field->fieldname;        
        if ($this->field->is_key || (isset($this->field->is_editable) && $this->field->is_editable===false)){
            $field = new iddiHtml_Div();
            $field->setValue($data->$field_name);
        }else{
            $field = new iddiHtml_Input();
            $field->attributes['VALUE']=$data->$field_name;
            $field->attributes['TYPE']='text';
            $field->attributes['DATA-ENTITY']=$data->entityname;
            $field->attributes['DATA-ENTITYID']=$data->id;
        }
        $new_col=$this->field_helper($this->field->caption, $field);
        $new_col->label->addclass('iddi-text-field');
        return $new_col;
    }    
}