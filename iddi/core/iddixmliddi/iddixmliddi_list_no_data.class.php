<?php
    /**
    * iddiXmlIddi_List_No_Data Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_List_No_Data extends iddiXmlIddiNode{
        function preparse(){
            $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
            $qp->nodata=$this;
            parent::preparse();
        }
    }
   