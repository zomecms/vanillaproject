<?php
    /**
    * iddiXmlIddi_Post_Error_Message Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Post_Error_Message extends iddiXmlIddiNode{
        function output(){
            return $_POST['errors'][$this->getParentOfType('iddiXmlIddi_Post_Error')->errorcode];
        }
    }
    /**
    * @desc This command grabs some xml from another template and inserts it into the dom on the preparse so that it can be used as part of the output
    *       Needs 2 parameters - source document (can be a http request) and xpath to the desired node or nodes.
    */
   