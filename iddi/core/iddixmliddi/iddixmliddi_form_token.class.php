<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of iddixmliddi_form_token
 *
 * @author Jonathan
 */
class iddiXmlIddi_Form_Token extends iddiHtml_Input {    
    function parse() {
        $_SESSION['formtoken']=md5(time-rand(20000,1000000)).uniqid();
        $this->attributes['TYPE']='hidden';        
        $this->attributes['NAME']='iddiformtoken';
        $this->attributes['VALUE']=$_SESSION['formtoken'];
        parent::parse();
    }    
}
