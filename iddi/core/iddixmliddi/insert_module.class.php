<?php

class iddiXmlIddi_Insert_Module extends iddiXmlIddiNode {
    var $fail = false;

    function parse() {
        try {
            $this->processAVT();
            if($_GET['res']) iddi::log_resource_use('template', 'module_' . $this->attributes['MODULE'] . '.xml');
            $d = new iddiXmlDocument();
            $d->setTemplate($this->owner->getTemplate());
            $d->LoadFile('../iddi-project/templates/module_' . $this->attributes['MODULE'] . '.xml');
            $this->appendChild($d->documentelement);
            $ds=$this->parent->getDataSource();
            //Append any attributes to the current entity/datasource
            foreach($this->attributes as $k=>$v){
                $k=strtolower($k);
                $ds->$k=$v;
                $ds->dbfields[$k]=$v;
            }
            $ds=$this->getCurrentEntity();
            $d->setCurrentEntity($ds);
            //$this->setowner($this->owner);
            $d->documentelement->preparse();
            if ($this->children) foreach($this->children as $child) $child->parse();
        } catch (Exception $e) {
            $this->fail = $e;
        }
    }

    function output() {
        if ($this->fail) {
	    //return $this->fail;
            //$this->fail->getMessage();
        } else {
            return parent::output();
        }
    }

}
