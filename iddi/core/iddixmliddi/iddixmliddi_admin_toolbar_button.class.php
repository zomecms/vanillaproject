<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Toolbar_Button extends iddiXmlIddiNode {
    var $icon;
    var $caption;
    var $name;
    
    static function create($icon,$caption,$name,$attrs=null){
        $button=new iddiXmlIddi_Admin_Toolbar_Button();
        $button->icon=$icon;
        $button->caption=$caption;
        $button->name=$name;
        
        if($attrs!=null) $button->attributes=$attrs;
        
        return $button;
    }
    
    function parse(){
        if($this->attributes['ICON']) $this->icon=$this->attributes['ICON'];
        if($this->attributes['CAPTION']) $this->caption=$this->attributes['CAPTION'];
        if($this->attributes['NAME']) $this->name=$this->attributes['NAME'];
        
        unset($this->attributes['ICON']);
        unset($this->attributes['CAPTION']);
        unset($this->attributes['NAME']);
        parent::parse();
    }
    
    function output(){
        foreach($this->attributes as $k=>$v) $attroutput.=$k.'="'.$v.'" ';
        return '<i class="fa '.$this->icon.'" data-action-name="'.$this->name.'"'.$attroutput.'><span>'.$this->caption.'</span></i>';
    }
}
