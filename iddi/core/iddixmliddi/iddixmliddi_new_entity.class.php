<?php
    /**
    * iddiXmlIddi_New_Entity Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_New_Entity extends iddiDataSource{
       function parse(){
           $this->entityname=$this->getAttribute('NAME');
           $this->id=--self::$_next_entity_id;
           parent::parse();
       }
    }

    //---- Editable Nodes ----
   