<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard_Headline_Base extends iddiXmlIddiNode {
    var $title;
    var $subtitle;
    var $interval='month';
    var $period='-13';
    var $stat='SUM(total_paid)';
    var $where;
    var $table;
    var $headline_prefix;

    function parse() {
        $out = new iddiDataSource();

        $where=($this->where)?' WHERE '.$this->where:'';
        if($this->interval=='month'){
            $sql = 'SELECT MONTH(created), YEAR(created), MONTH(created) - MONTH(NOW()) - ((YEAR(NOW())-YEAR(created)) * 12) d, '.$this->stat.' v FROM `'.$this->table.'` '.$where.' GROUP BY MONTH(created),YEAR(created) HAVING d > '.$this->period.' ORDER BY d';
        }
        if($this->interval=='day'){
            $sql = 'SELECT MONTH(created), YEAR(created), DAY(created), DATEDIFF(created,NOW()) d, '.$this->stat.' v FROM `'.$this->table.'` '.$where.' GROUP BY MONTH(created),YEAR(created) HAVING d > '.$this->period.' ORDER BY d';
        }
        
        $rs = iddiMySql::query($sql);
        foreach($rs as $row){
            $data[$row->d]=$row->v;
        }
        
        for($a=-12;$a<=0;$a++){
            $sparkdata[$a]=$data[$a];
        }
        
        $out->this_month = ($data[0])?intval($data[0]):0;
        $out->previous_month = ($data[-1])?intval($data[-1]):0;

        $out->velocity = intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        $out->headline_prefix=$this->headline_prefix;
        $out->title=$this->title;
        $out->subtitle=$this->subtitle;
        $out->velocity_class=($out->velocity==0)?'iddi-velocity-steady':($out->velocity<0)?'iddi-velocity-down':'iddi-velocity-up';
        $out->velocity_icon=($out->velocity==0)?'fa-circle-o':($out->velocity<0)?'fa-level-down':'fa-level-up';
        $out->spark_data=implode(',',$sparkdata);
        $this->setDataSource($out);
        parent::parse();
    }

}
