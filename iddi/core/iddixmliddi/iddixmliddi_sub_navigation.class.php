<?php
    /**
    * iddiXmlIddi_Sub_Navigation Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Sub_Navigation extends iddiXmlIddiNode{
      static $sn_depth=0;
      function preparse(){
          $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
          $qp->sub_nav=$this;
      }
      function parse(){
        if(self::$sn_depth < 10){
          self::$sn_depth++;
          $ce=$this->getDataSource();
          if($ce->id > 0){
            $p=$this->getParentOfType('iddiXmlIddi_Navigation');
            $current_page=$this->owner->getDataSource()->id;
            if($p){
              if(array_key_exists($ce->id,$p->_parent_path) || $ce->id==$current_page || $this->getAttribute('show')=='all-children')
              {
                //Clone it
                $new_repeater=$p->cloned->clonenode($this,true);
                $new_repeater->setDataSource($ce);
                $new_repeater->preparse();
                $new_repeater->parse($ce->id);
              }
            }
          }
          self::$sn_depth--;
        }
      }


    }
    /**
    * @desc Defines an area within a list to be used as the repeatable area
    */
   