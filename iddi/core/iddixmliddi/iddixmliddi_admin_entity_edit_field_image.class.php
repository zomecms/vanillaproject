<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit_Field_Image extends iddiXmlIddi_Admin_Entity_Edit_Field{
    function set_data(){                
        $field_name = $this->field->fieldname;                
        $fielddata=new iddiDataSource();        
        $fielddata->dbfields['caption']=$this->field->caption;
        $fielddata->dbfields['entityname']=iddiMySql::tidyname($this->field->lookup);
        $fielddata->dbfields['src']='/api/image?width=40&height=40&scalemode=force&outmode=image&filename='.urlencode($this->form->row->$field_name);
        $fielddata->dbfields['fieldvalue']=$this->form->row->$field_name;
        $fielddata->dbfields['id']=$this->form->entityid;
        $fielddata->dbfields['entity']=$this->form->entity;
        $fielddata->dbfields['fieldname']=$field_name;
        $this->setDataSource($fielddata);
    }            
}