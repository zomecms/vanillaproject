<?php
    /**
    * iddiXmlIddi_Template_Meta Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Template_Meta extends iddiXmlIddiNode{
         function compile(){
             $fieldname=$this->getAttribute('NAME');
             if ($fieldname!=''){
                $this->owner->getTemplate()->$fieldname=$this->value;
             }
             if ($this->children) foreach($this->children as $child) $child->compile();
         }
         function parse(){
            $this->processAVT();
             $this->value='';
             return '';
         }
         function output(){
             return '';
         }
    }
   