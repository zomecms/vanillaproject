<?php
    /**
    * iddiXmlIddi_If_Base Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_If_Base extends iddiXmlIddiNode{
      function parse(){
        $test=$this->test_if();
        if($this->negate) $test=!$test;
        if($test) return parent::parse();
      }
      function output(){
        $test=$this->test_if();
        if($this->negate) $test=!$test;
        if($test) return parent::output();
      }
    }
