<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Auto_Filter extends iddiXmlIddiNode {
    function parse(){
        $this->processAVT();
        $this->datasource=$this->getParentOfType('iddiXmlIddi_Admin_Entity_Table')->getDataSource(); 
        $this->load_template();
        parent::parse();
    }

    function load_template(){
        $t=iddiTemplate::load_first_match(array('y-search-bar_'.$this->entityname,'y-search-bar'), $this->datasource)->data->documentelement;    
        foreach($t->children as $child) $this->appendChild($child);        
    }    
}
