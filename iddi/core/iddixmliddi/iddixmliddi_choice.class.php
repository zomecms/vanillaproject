<?php
    /**
    * iddiXmlIddi_Choice Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Choice extends iddiXmliddiNode{
      var $active=false;
      function parse(){
          parent::parse();
        $this->group=$this->getParentOfType('iddiXmlIddi_Group');
        if($this->is_default()){
          //If no language is provided this area will only be displayed if no other language areas in the current group have been used
          //If we are not in a group this is an invalid state
          if($this->group){
            if(!$this->group->active_item){
              $this->active=true;
              $this->group->active_item=$this;
            }
          }
        }else{
          //Check for a match of the language attribute against the current requests language code
          if($this->group_test()){
            //See if this node is participating in a group
            if($this->group){
              //If it is set this node as the groups active item
              $this->active=true;
              $this->group->active_item=$this;
            }else{
              //If we're not in a group just do the output
              $this->active=true;
            }
          }
        }
      }
      function group_test(){
        return true;
      }
      function is_default(){
        return false;
      }
      function output(){
        if ($this->active) return parent::output();
      }

    }
   