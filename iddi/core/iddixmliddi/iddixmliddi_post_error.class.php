<?php
    /**
    * iddiXmlIddi_Post_Error Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Post_Error extends iddiXmlIddiNode {
        var $errorcode;

        function preparse(){
            $this->errorcode=$this->getAttribute('forcode');
        }
        function output(){
            if($_POST['errors'][$this->errorcode]){
                return parent::output();
            }
            return '';
        }
    }
   