<?php
/**
 * Appends the contained content to either the current node or to the node
 * specified by @select.
 *
 * This runs only at preparse so you cannot use any AVTs or values from the current
 * datasource to affect, however the content is both preparsed and parsed as usual
 *
 * If @select specified multiple nodes the content is appended to all matching nodes
 *
 * @example
 *
 * Appends a new section in situ
 *
 * <iddi:append-content>
 *   <section>
 *     <h2>New Content Section</h2>
 *     <div><iddi:value name="New Content Section" type="html"/></div>
 *   </section>
 * </iddi:append-content>
 *
 * Appends a new section within the div called maincontent
 *
 * <iddi:append-content select="//div[@id='maincontent']">
 *   <section>
 *     <h2>New Content Section</h2>
 *     <div><iddi:value name="New Content Section" type="html"/></div>
 *   </section>
 * </iddi:append-content>
 *
 * @author J.Patchett
 * @package IDDI Core
 * @property xpath select optional xpath to the node to change
 **/
class iddiXmlIddi_Append_Content extends iddiXmlIddiNode {

    /**
     * Overrides the preparse method
     */
    public function preparse() {
        $xpath = $this->getAttribute('select');
        if ($xpath != '') {
            $targets = $this->xpath($xpath, true);
            foreach ($targets as $tgtnode) {
                foreach ($this->children as $child) {
                    $tgtnode->appendChild($child);
                }
            }
        } else {
            foreach ($this->children as $child) {
                $this->appendChild($child);
            }
        }
    }

}
