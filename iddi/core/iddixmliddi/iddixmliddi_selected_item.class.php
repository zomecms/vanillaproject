<?php
/**
 * Attaches the selected value to the current datasource allowing it to be used
 * as the selected item in the selected-attribute node
 *  
 * Adds a field called selecteditem to the current datasource
 * 
 * You can also name them so you can make use of multiple using the @name attribute
 *
 * @attribute select - xpath query that resolves to a scalar value which selected-attribute can compare
 * @attribute name string optional a name to use instead of selecteditem - defaults to "selecteditem"
 * @author Jonathan
 * @example
 * 
 * <select data-entity-id="{id}" data-entity-name="{entity}" name="{fieldname}">
 *   <option value="0">Please choose</option>
 *     <iddi:list-items entity="{entityname}">
 *       <iddi:sort select="pagetitle"/>
 *       <iddi:selected-item select="{id}" name="selecteditem"/>
 *       <option value="{id}"><iddi:selected-attribute select="{id}" name="selecteditem"/><iddi:value-of select="pagetitle"/></option>
 *    </iddi:list-items>
 * </select>
 * 
 */
class iddiXmlIddi_Selected_Item extends iddiXmlIddiNode {    
    var $selected_item_value;
    
    function parse(){
        $this->processAVT();
        $xpathquery=$this->getAttribute('SELECT');
        $ds=$this->getdatasource();
                
        $v1=$ds->xpath($xpathquery);
        if ($v1)
            $this->selected_item_value=$v1->first()->value;
        
        parent::parse(); 
    }
}
