<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of iddixmliddi_flash_message
 *
 * @author Jonathan
 */
class iddiXmlIddi_Flash_Message extends iddiXmlIddiNode {

    static function add($code,$message){
        $_SESSION[$code]=$message;
    }
    
    function output(){
        $code=$this->getAttribute('code');
        if(isset($_SESSION[$code])){
            $var=filter_input(INPUT_SESSION,$code,FILTER_SANITIZE_STRING);
            unset($_SESSION[$code]);
            return $var;
        }
    }

}