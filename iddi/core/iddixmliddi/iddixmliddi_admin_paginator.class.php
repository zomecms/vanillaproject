<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Paginator extends iddiXmlIddiNode {
    function parse(){
        $this->search=$_GET['quicksearch'];
        
        $paginator=new iddiHtml_Div();
        $paginator->addClass('y-paginator');
        
        $this->datasource=$this->getParentOfType('iddiXmlIddi_Admin_Entity_Table');
        if($this->datasource->page > 2){
            $paginator->appendChild($this->createPageLink(1,'<i class="fa fa-angle-double-left"></i>'));
        }
        if($this->datasource->page > 1){
            $paginator->appendChild($this->createPageLink($this->datasource->page-1,'<i class="fa fa-angle-left"></i>'));
        }

        $startListFrom=$this->datasource->page-5;
        if($startListFrom+9 > $this->datasource->totalPages) $startListFrom=$this->datasource->totalPages-9;
        if($startListFrom < 1) $startListFrom=1;
        for($a=$startListFrom; $a<$startListFrom+10; $a++){
            if($a <= $this->datasource->totalPages){
                $button=$this->createPageLink($a,$a);
                if($a==$this->datasource->page) $button->addClass('y-active-page');
                $paginator->appendChild($button);
            }
        }
        
        if($this->datasource->page < $this->datasource->totalPages){
            $paginator->appendChild($this->createPageLink($this->datasource->page+1,'<i class="fa fa-angle-right"></i>'));
        }
        if($this->datasource->page < $this->datasource->totalPages - 1){
            $paginator->appendChild($this->createPageLink($this->datasource->totalPages,'<i class="fa fa-angle-double-right"></i>'));
        }
        
        
        $this->parent->appendChild($paginator);
    }
    
    function createPageLink($page,$content){
        $element=new iddiHtml_Button();
        $element->setAttribute('data-y-entity',$this->datasource->entity);
        $element->setAttribute('data-y-page',$page);
        $element->setAttribute('data-action-name','y-action-list-entity');
        $element->setAttribute('data-y-search',$this->search);
        $element->setValue($content);
        return $element;
    }
}
