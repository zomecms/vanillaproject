<?php
    /**
    * iddiXmlIddi_Language_Area Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Language_Area extends iddiXmlIddi_Choice{
      var $active=false;
      function preparse(){
        $this->language_choice=$this->getAttribute('language');
        parent::preparse();
      }
      function group_test(){
        return $this->language_choice==iddiRequest::$current->language;
      }
      function is_default(){
        return $this->language_choice=='';
      }
    }
   