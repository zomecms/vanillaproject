<?php
    /**
    * iddiXmlIddi_CData Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_CData extends iddiXmlIddiNode{
        function output($clean=false,$level=0){
            $v=parent::output($clean,$level);
            return "<![CDATA[\n".$v."\n";
        }
    }

   