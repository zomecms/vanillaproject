<?php
    /**
    * iddiXmlIddi_List_Items Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_List_Items extends iddiXmlIddi_Query_Parser{
        var $limiter,$filters,$repeatable,$repeatable_current,$sorter,$grouper;

      function parse(){
            $this->processAVT();
            $select=$this->attributes['SELECT'];
            $entity=$this->attributes['ENTITY'];
            $selected=$this->attributes['SELECTED'];
            $checked=$this->attributes['SELECTED'];
            $sql=$this->attributes['SQL'];
            $this->preparse();
            $repeatable=($this->repeatable)?$this->repeatable:$this;

            $rs=$this->get_data();

            $this->count=sizeof($rs);
            if ($rs)
            {
                //Set the datasource to the results from the query
                $this->setDataSource($rs);
                //Make an orphaned clone of the repeateable node
                $clone=$repeatable->clonenode(null,true);
                //Now remove the repeatable sections from this node
                foreach($repeatable->children as $child) $repeatable->removeChild($child);
                //Same for current if it's there
                if($this->repeatable_current){
                  $clone_current=$this->repeatable_current->clonenode(null,true);
                  $this->repeatable_current->is_origin=true;
                }
                $this->pos=0;
                //Run through each result and clone the clone, but append it to myself ready for outputting
                foreach($rs as $record) {
                  if($this->mode=='entity'){
                    //Load the item
                    $entityname=($entity=='sysfilenames')?$record->entityname:iddiMySql::tidyname($entity);
                    if(substr($entityname,0,3)=='sys'){
                        $ent=(method_exists($record,'expandData'))?$record->expandData():$record;
                    }elseif($entityname!=''){
                      try{
                        $ent=iddimysql::loadpagebyid($entityname,$record->id,new iddiMySqlRow());
                      }catch(Exception $e){
                        $ent=null;
                      }
                    }else{
                        $ent=null;
                    }
                  }else{
                    $ent=$record;
                  }
                  if($ent){
                    $ent->position=$this->pos+1;
                    if ($this->limiter==null || $this->pos >= $this->limiter->start){
                        //Decide which repeatable area to use and assign to $repeatable_group - this is then cloned in a moment
                        if (substr_count($this->owner->getDataSource()->virtualfilename,$record->virtualfilename)>0){
                          $repeatable_group=($clone_current)?$clone_current:$clone;
                        }else{
                          $repeatable_group=$clone;
                        }
                        $repeatable_group->setDataSource($ent);
                        if ($repeatable_group->children){
                            foreach($repeatable_group->children as $child) {
                                //Clone the repeatable area, and set the datasource
                                $newchild=$child->clonenode($repeatable,true);
                                $newchild->setDataSource($ent);
                                $newchild->_pos=$this->pos;
                                //$newchild->parse();
                                $newchild->processAVT();
                                if ($this->owner->getDataSource()->id==$record->id || $this->owner->getDataSource()->rootlanguageitemid==$record->rootlanguageitemid) $newchild->addClass('active');
                                if (substr_count($this->owner->getDataSource()->virtualfilename,$record->virtualfilename)>0) $newchild->addClass('expanded');
                                if ($this->pos==0) $newchild->addClass('first');
                                if ($ent->id==$selected && $selected!='') $newchild->setAttribute('selected','selected');
                                if ($newchild->getAttribute('value')==$selected && $selected!='') $newchild->setAttribute('selected','selected');
                                if ($ent->id==$checked && $selected!='') $newchild->setAttribute('checked','checked');
                            }
                        }
                    }
                    ++$this->pos;
                    if ($this->limiter!=null && $this->pos > $this->limiter->length+$this->limiter->start) break;
                  }else{

                  }
                }
                if($this->pos==0) foreach($this->children as $child) $this->removeChild($child);
                foreach($this->children as $child) $child->Parse();
                if ($this->nodata) $this->nodata->remove();
            }else{
                if($this->nodata) $c=$this->nodata->cloneNode(null,true);
                foreach($this->children as $child){
                  $this->removeChild($child);
                }
                if($c){
                  $c->setDataSource($this->getDataSource());
                  $c->parse();
                  $this->appendChild($c);
                }
            }
      }

      function get_data(){
            $select=$this->attributes['SELECT'];
            $entity=$this->attributes['ENTITY'];
            $selected=$this->attributes['SELECTED'];
            $checked=$this->attributes['SELECTED'];
            $sql=$this->attributes['SQL'];
            if($sql!=''){
              $sql=str_replace('= ','=0 ',$sql);
              $this->mode='sql';
              $this->sql=$sql;
              $rs=iddiMySql::query($this->sql);
              $rs->setPageLength(1000);
              if(!$rs->hasData()) $rs=null;
              $this->limiter=null;
            }elseif ($entity!=''){
              $this->mode='entity';
                //Pull from entity
                if(substr($entity,0,3)!='sys' && !$this->allow_non_root_items){
                    $langfilter=new iddiXmlIddi_Filter();
                    $langfilter->attributes['FILTER']='(rootlanguageitemid is null or l.rootlanguageitemid=l.id)';
                    $this->appendChild($langfilter);
                }
                $this->sql=$this->buildSql();
                $rs=iddiMySql::query($this->sql);
                if(!$rs->hasData()) $rs=null;
                $this->limiter=null;
            }elseif ($select!=''){
                //Pull from select
                $rs=$this->xpath($select);
            }
            return $rs;
      }

    }
