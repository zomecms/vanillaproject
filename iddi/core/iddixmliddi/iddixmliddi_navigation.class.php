<?php
    /**
    * iddiXmlIddi_Navigation Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Navigation extends iddiXmlIddi_List_Items{
        var $limiter,$filters,$repeatable,$sorter,$grouper,$cloned;

        function preparse(){
          if(!$this->cloned) $this->cloned=$this->clonenode(null,true);
          parent::preparse();
        }

        function parse($parent_page_id=null){
          $this->setAttribute('entity','sysfilenames');
          if($parent_page_id===null){
            //Locate the top level page (stopping before we hit stopbefore
            $this->stopbefore=$this->getAttribute('STOPBEFORE');
            $d=$this->getDataSource();
            $parent_page_id=$this->findtoplevel($d->id,$this->stopbefore,$d->id);
          }else{
            $this->stopbefore=$this->getAttribute('STOPBEFORE');
            $current_page=$this->owner->getDataSource()->id;
            $this->findtoplevel($current_page,$this->stopbefore,$current_page);
          }
          //Add the parent page as a filter
          $f=new iddiXmlIddi_Filter($this->owner,'filter',$this);
          if ($parent_page_id==''){
              $f->setAttribute('filter','parentid={current()/id()}');
          }else{
              $f->setAttribute('filter','parentid='.$parent_page_id);
          }
          $f->setAttribute('order','odr,id');

          parent::parse();


        }
    }
