<?php
    /**
    * iddiXmlIddi_Widget_Holder Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Widget_Holder extends iddiXmlIddiNode{
      function parse(){
            $this->processAVT();
          //Insert widgets
          switch($this->owner->getTemplate()->getmode()) {
            case 'edit':
                $this->value='<div class="iddi-widget-holder"><h1>Widget Holder</h1></div>';
                break;
            default: $this->value='A value would go in here'; break;
          }
          if ($this->children) foreach($this->children as $child) $child->parse();
      }
    }
   