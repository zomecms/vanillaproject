<?php
/**
 * Adds an attribute of selected="selected" to it's parent if the resolved value of 
 * select matches the field on the parent datasource
 *  
 * Adds a field called selecteditem to the current datasource
 * 
 * You can also name them so you can make use of multiple using the @name attribute
 *
 * @attribute select - xpath query that resolves to a scalar value which selected-attribute can compare
 * @attribute name string optional a name to use instead of selecteditem - defaults to "selecteditem"
 * @author Jonathan
 * @example
 * 
 * <select data-entity-id="{id}" data-entity-name="{entity}" name="{fieldname}">
 *   <option value="0">Please choose</option>
 *     <iddi:list-items entity="{entityname}">
 *       <iddi:sort select="pagetitle"/>
 *       <iddi:selected-item select="{id}" name="selecteditem"/>
 *       <option value="{id}"><iddi:selected-attribute select="{id}" name="selecteditem"/><iddi:value-of select="pagetitle"/></option>
 *    </iddi:list-items>
 * </select>
 * 
 */
class iddiXmlIddi_Selected_Attribute extends iddiXmlIddiNode {        
    function parse(){
        $this->processAVT();
        $ds=$this->getDatasource();
        $xpath_query=$this->getAttribute('SELECT');
        
        $v1=$ds->xpath($xpath_query);
        $v2=$this->getParentOfType('iddiXmlIddi_Selected_Item')->selected_item_value;
        if ($v1 && $v2)
            if ($v1->first()->value == $v2)
                $this->parent->attributes['SELECTED']='selected';
         
    }
}
