<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Querystring extends iddiXmlIddiNode{
    
    function parse(){
        $d=new iddiDataSource();
        $objectkeys=get_object_vars($d);
        foreach($_GET as $k=>$v){
            if(array_key_exists($k, $objectkeys))
                $k='__'.$k;                    
            $d->$k=$v;
            $d->dbfields[$k]=$v;
        }
        $this->setDataSource($d);
        parent::parse();
    }
    
}