<?php
    /**
    * iddiXmlIddi_Widget Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Widget extends iddiXmlIddi_Entity{
        function compile(){
            iddi::Log('Compiling Widget '.$this->getAttribute('NAME'),0);
            parent::compile();
        }

    }
   