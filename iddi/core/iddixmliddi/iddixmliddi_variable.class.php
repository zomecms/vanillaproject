<?php
    /**
    * iddiXmlIddi_Variable Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Variable extends iddiXmlIddiNode{
        var $data;
        function parse(){
            $this->processAVT();
            $xpath=$this->getAttribute('select');
            $name=$this->getAttribute('name');

            $this->data=$this->xpath($xpath,true);
            $this->name=$name;

            $this->parent->addVariable($name,$this);
        }
    }
    /**
    * @desc RSS Feed Reader
    */
