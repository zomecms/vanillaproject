<?php
/**
 * This is for iddi nodes that are editable on the frontend of the website.
 * These nodes MUST ALWAYS be wrapped in an enclosing html node that can be
 * used exclusively by this node. The enclosing node becomes the target for 
 * data attibutes used by the editor.
 * 
 * Compiler will throw an exception if there is no parent html node that is not already
 * in use by another editable node
 *
 * @author Jonathan
 */
class iddiXmlIddiEditableNode extends iddiXmlIddiNode {
    
    function output() {
        if (iddiRequest::$is_edit_mode && $this->is_in_body()) {
            return parent::output();
        } else {
            return $this->outputchildrenonly();
        }
    }
    
}
