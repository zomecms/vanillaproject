<?php

/**
 * iddiXmlIddi_Img Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 * */
class iddiXmlIddi_Img extends iddiXmlIddi_Value {

    var $fieldinfo;

    function parse() {
        if (!$this->owner)
            throw new iddiException('Node dosn\'t have an owner', 'iddi.iddiXml.IddiValue.parse.noowner', $this);
        if (!$this->owner->getCurrentEntity())
            throw new iddiException('There is no current entity to get a value from', 'iddi.iddiXml.IddiValue.parse.noentity', $this);

        $this->processAVT();

        $this->fieldname = iddiMySql::tidyname($this->attributes['NAME']);
        $this->fieldvalue = $this->getFieldValue($this->fieldname);

        /**
         * Get the original filename so that we can get dimensions from it for the data attributes
         */
        $original_filename = str_replace('/public/', '/originals/', $this->fieldvalue);
        $original_filename_parts = explode('{', $original_filename);
        $original_filename_ext = explode('.', $original_filename);
        $original_filename = $original_filename_parts[0]; // . '.' . $original_filename_ext[1];

        //if(substr($this->fieldvalue,0,1)=='/') $this->fieldvalue=substr($this->fieldvalue,1);
        //Generate the image if we need to
        if (!$this->fieldinfo)
            $this->preparse();
        $i = new iddiImageField();
        $filename = $i->parseValue($this->fieldvalue, $this->fieldinfo);

        if ($this->fieldvalue == '') {
            $this->fieldvalue = $this->attributes['DEFAULTSRC'];
            if ($this->fieldvalue == '') {
                $this->fieldvalue = '/images/public/general/no-pic.jpg';
            }
        }



        //Generate the image if we need to
        if (!$this->fieldinfo)
            $this->preparse();
        $i = new iddiImageField();
        $filename = $i->parseValue($this->fieldvalue, $this->fieldinfo);

        if (file_exists('../' . $filename))
            $this->fieldvalue = $filename;
        $this->fieldvalue = $filename;


        $p = getimagesize('../' . $original_filename);
        $w = $p[3];

        $this->attributes['DATA-ORIGINAL-HEIGHT'] = $p[1];
        $this->attributes['DATA-ORIGINAL-WIDTH'] = $p[0];        
        
        
        if (iddiRequest::$is_edit_mode) {
            $d = $this->getdatasource();
            $this->setAttribute('data-y-entityid', $d->id);
            $this->setAttribute('data-y-entityname', iddiMySql::tidyname($d->entityname));
            $this->setAttribute('data-y-fieldtype', 'image');
            $this->setAttribute('data-y-fieldname', $this->fieldname);
            $this->setAttribute('data-y-original_filename', $original_filename);    
            foreach($this->fieldinfo as $fieldinfo){
                $this->setAttribute('data-y-resize-height', $fieldinfo['resize']['height']);            
                $this->setAttribute('data-y-resize-width', $fieldinfo['resize']['width']);            
                $this->setAttribute('data-y-resize-scalemode', $fieldinfo['resize']['scalemode']);            
                $this->setAttribute('data-y-resize-upscale', $fieldinfo['resize']['upscale']);            
                break;
            }
        }

        $this->value = $this->fieldvalue;

        if ($this->children)
            foreach ($this->children as $child)
                $child->parse();
        
        $this->owner->getCurrentEntity()->addField($this);
    }

    function output($clean = false, $level = 0) {
        /**
         * Ensure that we always have an alt tag so output is valid
         */
        if (!$this->attributes['ALT'])
            $this->setAttribute('alt', $this->getDataSource()->pagetitle);

        //if ($clean) $output="\n".str_repeat('  ',$level);
        $found = false;
        if (substr_count($this->fieldvalue, 'no-pic') == 0) {
            $found = true;
        } else {
            $found = false;
        }
        $p = getimagesize(IDDI_ROOT . $this->fieldvalue);
        $w = $p[3];

        $tag_close = (iddiXmlDocument::$is_html) ? '>' : '/>';

        if ($this->attributes['NOIMAGE'] == 'hide' && !isset($p['mime'])) {
            if (iddiRequest::$current->getMode() == 'edit') {
                unset($this->attributes['NOIMAGE']);
                unset($this->attributes['NAME']);
                $output.='<img src="' . $this->fieldvalue . '" ' . $this->passthroughattributes() . ' ' . $w . $tag_close;
            }
        } else {

            $this->fieldvalue = str_replace('{', '%7B', str_replace('}', '%7D', $this->fieldvalue));
            unset($this->attributes['NOIMAGE']);
            unset($this->attributes['NAME']);

            $output.='<img src="' . $this->fieldvalue . '" ' . $this->passthroughattributes() . ' ' . $w . $tag_close;
        }
        return $output;
    }

    function getType() {
        return 'image';
    }

}
