<?php
    /**
    * iddiXmlIddi_Php Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Php extends iddiXmlIddiNode{
        function parse(){
            if(iddiRequest::getMode()!='edit'){
                $this->processAVT();
                $this->output=eval($this->getValue());
            }
        }
        function output(){
            if(iddiRequest::getMode()!='edit'){
                return $this->output;
            }else{
                return '<div class="editor-note"><p class="text">PHP Actions disabled in edit mode</p></div>';
            }
        }
    }
   