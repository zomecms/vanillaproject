<?php
    /**
    * iddiXmlIddi_List_Subpages Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_List_Subpages extends iddiXmlIddi_Query_Parser{
        var $stopbefore;

        function findtoplevel($pageid,$stopbefore,$previouspageid,$level=0){
           //if ($pageid=='') throw new iddiCodingException('No Entity ID Provided','iddi.xml.List-Subpages.NoEntityId');
            $language=iddiRequest::$current->language;
            $baselanguage=iddiRequest::$current->baselanguage;

            if ($level>10 && $pageid>0){
                return $pageid;
            }else{
                $sql='SELECT parentid,virtualfilename,rootlanguageitemid FROM {PREFIX}sysfilenames WHERE id='.$pageid;
                $rs=iddiMySql::query($sql);
                if ($rs->HasData()){
                    foreach($rs as $row){
                        if ($row->virtualfilename==$stopbefore){
                            return $previouspageid;
                        }else{
                            $parentid=$row->parentid;
                            if ($row->rootlanguageitemid>0) $parentid=$row->rootlanguageitemid;
                            return $this->findtoplevel($parentid,$stopbefore,$pageid,++$level);
                        }
                        break;
                    }
                }else{
                    return $previouspageid;
                }
            }
        }

      function parse($parentpageid=''){
            $this->stopbefore=$this->getAttribute('STOPBEFORE');
            $d=$this->getDataSource();
            $parentpageid=$this->findtoplevel($d->id,$this->stopbefore,$d->id);

            $f=new iddiXmlIddi_Filter($this->owner,'filter',$this);
            if ($parentpageid==''){
                $f->setAttribute('filter','parentid={current()/id()}');
            }else{
                $f->setAttribute('filter','parentid='.$parentpageid);
            }
            $f2=new iddiXmlIddi_Filter($this->owner,'filter',$this);
            $f2->setAttribute('filter','rootlanguageitemid is null');

            $this->processAVT();
            $this->sql=$this->buildSql();

            $rs=iddiMySql::query($this->sql);

            $repeatable=($this->repeatable)?$this->repeatable:$this;

            if($rs->HasData()){
                //Grab the dataset
                $rs->entityname=$rs->getFirstRow()->entityname;
                $this->setdatasource($rs);
                $clone=$repeatable->clonenode(null,true);
                //foreach($this->children as $child) $this->removeChild($child);
                foreach($repeatable->children as $child) $repeatable->removeChild($child);
                foreach($rs as $record) {
                    $ent=iddimysql::loadpagebyid($record->entityname,$record->id,new iddiMySqlRow());
                    if ($clone->children){
                        foreach($clone->children as $child) {
                            $newchild=$child->clonenode($repeatable,true);
                            $newchild->setDataSource($ent);
                            $newchild->parse();
                            $newchild->processAVT();
                        }
                    }
                }
                parent::parse();
            }else{
                foreach($this->children as $child) $this->removeChild($child);
            }
      }
    }
   