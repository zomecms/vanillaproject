<?php
    /**
    * iddiXmlIddi_Query_Parser Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Query_Parser extends iddiXmlIddi_Entity{
      var $sql;
      function parse(){
          $this->processAVT();
          $sql=$this->buildSql();
      }

      function compile(){
          //iddi::Log("Compile : Skipping node ".$this->nodename,0);
          if ($this->children) foreach($this->children as $child) $child->compile();
      }
      function buildSql(){
            //if (!$this->owner)  throw new iddiException('Node dosn\'t have an owner','iddi.iddiXml.IddiValue.parse.noowner',$this);

            $this->entityname=iddiMySql::tidyname($this->attributes['ENTITY']);

            //Look for any joins
            foreach($this->children as $child){
                if(method_exists($child,'buildSql_Joins')) {$sql_joins.=$child->buildSql_Joins(); $this->removeChild($child);}
                if(method_exists($child,'buildSql_Joins_Where')) {$sql_where.=$child->buildSql_Joins_Where($whereand);  $this->removeChild($child);}
                if(method_exists($child,'buildSql_Where')) {$sql_where.=$child->buildSql_Where($whereand); $this->removeChild($child);}
                if(method_exists($child,'buildSql_GroupBy')) {$sql_groupby.=$child->buildSql_GroupBy($groupcomma); $this->removeChild($child);}
                if(method_exists($child,'buildSql_Having')) {$sql_having.=$child->buildSql_Having($havingand); $this->removeChild($child);}
                if(method_exists($child,'buildSql_OrderBy')) {$sql_orderby.=$child->buildSql_OrderBy($ordercomma); $this->removeChild($child);}
                if(method_exists($child,'buildSql_Limit')) {$sql_limit=$child->buildSql_Limit(); $this->removeChild($child);}
            }
            
            if(substr($this->entityname,0,3)=='sys'){
                $sql='SELECT * FROM `{PREFIX}'.$this->entityname.'` as '.$this->entityname;
            }else{
                $sql='SELECT '.$this->entityname.'.*, l.virtualfilename,l.language,l.pagetitle,l.virtualfilename as link FROM `{PREFIX}'.$this->entityname.'`  as '.$this->entityname.', {PREFIX}sysfilenames l';
                $sql_join_where='('.$this->entityname.'.id=l.id)'.$whereand;
            }
            $sql.=$sql_joins;
            if ($sql_join_where!='' || $sql_where!='') {
                $sql.=' WHERE '.$sql_join_where.$sql_where;
            }
            if ($sql_groupby!='') $sql.=' GROUP BY '.$sql_groupby;
            if ($sql_having!='') $sql.=' HAVING '.$sql_groupby;
            if ($sql_orderby!='') $sql.=' ORDER BY '.$sql_orderby;
            if ($sql_limit!='') $sql.=' LIMIT '.$sql_limit;


            //echo '<li>'.$sql;
            return $sql;
      }
    }
   