<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard_Flot_Bar_Base extends iddiXmlIddiNode {
    static $chartid=1;
    var $title;
    var $subtitle;
    var $interval='days';
    var $period=-31;
    var $stat='SUM(total_paid)';
    var $where;
    var $table;
    var $headline_prefix;

    function parse() {
        $out = new iddiDataSource();

        $where=($this->where)?' WHERE '.$this->where:'';
        if($this->interval=='month'){
            $sql = 'SELECT MONTH(created), YEAR(created), MONTH(created) - MONTH(NOW()) - ((YEAR(NOW())-YEAR(created)) * 12) d, '.$this->stat.' v FROM `'.$this->table.'` '.$where.' GROUP BY MONTH(created),YEAR(created) HAVING d > '.$this->period.' ORDER BY d';
        }
        if($this->interval=='days'){
            $sql = 'SELECT created, DATEDIFF(created,NOW()) d, '.$this->stat.' v FROM `'.$this->table.'` '.$where.' GROUP BY DAY(created),MONTH(created),YEAR(created) HAVING d > '.$this->period.' ORDER BY d';
        }
                
        $rs = iddiMySql::query($sql);
        for($a=0;$a<(0-$this->period);$a++){
            $data[date('Y-m-d',strtotime('-'.$a.' '.$this->interval))]=0;
        }

        foreach($rs as $row){
            $data[date('Y-m-d',strtotime($row->created))]=$row->v;
        }

        $data_string='';
        $axis_string='';
        $i=0;
        $comma='';
        foreach($data as $item=>$value){
            $axis_string.=$comma.'['.$i.',"'.date('j',strtotime($item)).'"]';
            $data_string.=$comma.'['.$i++.','.$value.']';
            $comma=',';
        }
        
        $out->headline_prefix=$this->headline_prefix;
        $out->title=$this->title;
        $out->subtitle=$this->subtitle;
        $out->chart_name='flotbar'.self::$chartid++;
        $out->chart_data=$data_string;
        $out->chart_axis_data=$axis_string;
        $this->setDataSource($out);
        parent::parse();
    }

}
