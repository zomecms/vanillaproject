<?php
    /**
    * iddiXmlIddi_Default_Value Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Default_Value extends iddiXmlIddiNode{
      function parse(){
            $this->processAVT();
          switch(iddiRequest::$current->getMode()) {
            case 'edit':
                if ($this->parent->fieldvalue!=''){
                    unset($this->children);
                }else{
                    $this->parent->value=$this->value;
                }
                break;
            default:
                unset($this->children);
                break;
          }

      }
    }
   