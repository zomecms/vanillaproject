<?php
    /**
    * iddiXmlIddi_Load_Plugin Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Load_Plugin extends iddiXmlIddiNode {
        var $plugin_name;

        function preparse(){
            $this->plugin_name=$this->getAttribute('name');
            if($this->plugin_name=='') throw new iddiTemplateException('Name attribute not specified','iddi.xml.load-plugin.noNameAttribute');
            if(!file_exists(IDDI_PROJECT.$this->plugin_name)) throw new iddiTemplateException('Plugin file not in project folder','iddi.xml.load-plugin.pluginFileDoesNotExist');
        }

        function parse(){
            require_once(IDDI_PROJECT.$this->plugin_name);
        }
    }
   