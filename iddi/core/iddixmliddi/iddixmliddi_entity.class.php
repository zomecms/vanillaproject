<?php
    /**
    * iddiXmlIddi_Entity Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Entity extends iddiDataSource {
       var $pe,$fields,$query,$entityname,$name;

       function opennode(){
           if (!$this->owner)  throw new iddiException('This node doesn\'t appear to have an owner document','iddi.iddixmliddi_entity_name.node.setvalue.noowner',$this);
           if (!$this->owner->getTemplate())  throw new iddiException('This node doesn\'t appear to have a template','iddi.iddixmliddi_entity_name.node.setvalue.notemplate',$this);
           $this->pe=$this->owner->getCurrentEntity();
           //parent::setvalue($value);

           $this->owner->setCurrentEntity($this);
           $this->owner->entityname=$this->getAttribute('NAME');
           $this->entityname=$this->getAttribute('NAME');
           $this->owner->getTemplate()->addentity($this);
       }
       function closenode(){
           if ($this->pe){
               $this->owner->setCurrentEntity($this->pe);
               $this->owner->entityname=$this->pe->name;
           }
       }

       /**
       * @desc Compiles this entity to the database
       */
       function compile(){
           iddi::Log('Compiling Entity '.$this->getName().' at line number '.$this->lineno,0);
           if ($this->children) foreach($this->children as $child) $child->compile();
           iddiMySql::buildentitytable($this);
           iddiMySql::savefromarray('sysentities',array('name'=>$this->getname(),'tidyname'=>iddiMySql::tidyname($this->getName())));
           //Save the entity
           if ($this->data->entityname) iddiMySql::savefromarray('sysentities',array('name'=>$this->data->entityname,'tidyname'=>iddiMySql::tidyname($this->data->entityname)));
       }

       function getName(){return $this->getAttribute('name');}
       function getDataSource(){
         if($this->parent){
           return $this->parent->getDataSource();
         }else{
           return null;
         }
       }


    }
   