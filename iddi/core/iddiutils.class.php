<?php
    /**
    * iddiUtils Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiUtils{
       static function copyDir($dir,$target){
         @mkdir($target);
         echo "<li>Copying $dir to $target";
          $d=opendir($dir);
          while($f=readdir($d)){
            if(is_file($dir.'/'.$f)){
              if(!copy($dir.'/'.$f,$target.'/'.$f)) return false;
            }
            if(is_dir($dir.'/'.$f) && $f!='.' && $f!='..'){
              if(!self::copyDir($dir.'/'.$f,$target.'/'.$f)) return false;
            }
          }
          return true;
       }
    }


    //---- HTTP subsystem ----
    /**
    * @desc Main request system - this handles the request coming in and the getResponse method returns a response object depending on the request
    */
   