<?php
    /**
    * iddiApi_Result Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiApi_Result extends iddiRequest {
        var $success=true;
        var $error=null;
        //Error constants
        const ERROR_NO_CALL='iddi.api.no_call';
        const ERROR_UNKNOWN_CALL='iddi.api.unknown_call';
        const ERROR_ACCESS_DENIED='iddi.api.access_denied';
        const ERROR_LOGIN_REQUIRED='iddi.api.access_denied.login_required';
        const ERROR_INVALID_KEY='iddi.api.invalid_key';
        const ERROR_AUTH_FOLDER_NOT_WRITABLE='iddi.api.auth_folder_not_writable';
        const ERROR_FIREWALL_ERROR='iddi.api.firewall.request_rejected';
        const ERROR_FIREWALL_XSS='iddi.api.firewall.request_rejected.xss_detected';
        const ERROR_FIREWALL_FLOOD='iddi.api_firewall.request_rejected.flood';
        const ERROR_INVALID_INPUT='iddi.api.invalid_input';

        //Event Constants
        const EVENT_BEFORE_API_CALL='api_result.before_api_call';
        const EVENT_AFTER_API_CALL='api_result.after_api_call';
        const EVENT_BEFORE_API_OUTPUT='api_result.before_api_output';
        //Note: there is no after api output as the output results in a die with the result

        //Other constants
        const CALL_NAME_PREFIX='call_'; //Used to prefix functions that relate the call being made - DO NOT CHANGE
        const CALL_INPUT_KEY='call';    //Used to determine the name of the POST/GET key with the name of the function to call
        const AUTH_KEY_FOLDER=IDDI_PROJECT_PATH."/auth_keys";

        //Some basic security options
        var $login_required=true;
        var $acceptable_privelleges=array('admin','api');
        var $auth_key_required=false;


        //---- FIREWALL --------------------------------------------------------
        /*
         * This provides some code for running generic protection against common
         * and potentially dangerous hacks, such as Cross Site Scripting (XSS)
         * and Denial of service (DDOS) attacks.
         */

        /**
         * Provides any global security against XSS, Flood etc...
         */
        function firewall(){
            $this->firewall_xss();
            $this->firewall_flood();
        }

        /**
         * Protects against XSS attack
         */
        function firewall_xss(){
            foreach(get_object_vars($this->_call) as $k=>$v){
                if(substr_count($v,'<script')>0) throw new iddiException('Possible XSS attack detected', self::ERROR_FIREWALL_XSS);
            }
        }

        /**
         * Prevents flooding and therefore possible use of API to mount DOS attacks
         */
        function firewall_flood(){
            //TODO:Need to look into how we can do this
        }



        //---- UTILITIES -------------------------------------------------------

        /*
         * Makes request data available via _call object properties
         * for example $_POST['name'] can be accessed via $this->_call->name;
         */
        function populate_call_data(){
            $this->_call=new stdClass();
            foreach($_SESSION as $k=>$v) if(!is_object($v)) $this->_call->$k=urldecode($v);
            foreach($_COOKIE as $k=>$v) if(!is_object($v)) $this->_call->$k=urldecode($v);
            foreach($_GET as $k=>$v) if(!is_object($v)) $this->_call->$k=urldecode($v);
            foreach($_POST as $k=>$v) if(!is_object($v)) $this->_call->$k=urldecode($v);
        }

        /*
         * Copies all input data to target object. Useful for sending data to an entity
         * WARNING: Make sure the input has been cleaned first.
         * NOTE: Only copies scalars, will not copy arrays or other objects
         */
        function copy_call_data_to_object($object){
            foreach(get_object_vars($this->_call) as $o=>$v){
                if(!is_object($v) && !is_array($v)){
                    $object->$o=$v;
                }
            }
        }



        //---- SECURITY --------------------------------------------------------
        /*
         * This section provides security functions. It is important to ensure
         * that potentially malicious users cannot access this API
         */

        /**
         * Checks security inputs against settings in this object.
         * Settings in question are:
         *  var $login_required= [true/false];
         *  var $acceptable_privelleges=array([list of acceptable privileges]);
         *  var $auth_key_required= [true/false];
         */
        function check_security(){
            //Check some basic security
            if($this->login_required) $this->validate_session_user();
            if($this->auth_key_required) $this->validate_auth_key();
        }

        /**
         * Checks for:
         *   a) a user logged in and
         *   b) that the user has correct prileges listed in $this->acceptable_privileges
         *
         * If $this->acception_privileges is not an array a user just needs to be logged in to pass
         */
        function validate_session_user(){
            $user=$_SESSION['user'];
            if(!$user) throw new iddiException('Login Required', self::ERROR_LOGIN_REQUIRED);
            if(is_array($this->acceptable_privelleges))
                foreach($this->acceptable_privelleges as $privilege)
                    if($user->has_privilege($privilege)) return;
                    
            throw new iddiException('Insufficent Privilleges',self::ERROR_ACCESS_DENIED);            
        }

        /*
         * Checks to see if the auth key sent as part of the post is valid, or indeed that one was sent atall
         */
        function validate_auth_key(){
            if($this->_call->auth_key=='') throw new iddiException('No auth key provided',self::ERROR_INVALID_KEY);
            //Check in the keys database (on disk) for a file with this key
            if(!file_exists(self::AUTH_KEY_FOLDER)){
                @mkdir(self::AUTH_KEY_FOLDER);
                @chmod(self::AUTH_KEY_FOLDER,0777);
            }
            if(!is_writable(self::AUTH_KEY_FOLDER)) throw new iddiException('Auth key folder not writable. Auth key system unavailable', self::ERROR_AUTH_FOLDER_NOT_WRITABLE);
            if(!file_exists(self::AUTH_KEY_FOLDER.'/',$this->_call->auth_key)) throw new iddiException('Auth key not found',self::ERROR_INVALID_KEY);
        }



        //---- INPUT VALIDATION ------------------------------------------------
        /*
         * It is very important that all API call code validates that input from
         * the user is ok - these functions provide a quick way to check common
         * things. Each function takes a single input name or array of input
         * names for validation
         */

        function validate_strings($strings){
            if(is_array($strings)){
                foreach($strings as $string) $this->validate_strings($string);
            }
            if(!is_string($this->_call->$strings) && $this->_call->$strings!='') throw new iddiException($strings.' is not a string',self::ERROR_INVALID_INPUT);
        }

        function validate_numbers($numbers){
            if(is_array($numbers)){
                foreach($numbers as $number) $this->validate_strings($numbers);
            }
            if(!is_numeric($this->_call->$numbers)) throw new iddiException($numbers.' is not a number',self::ERROR_INVALID_INPUT);
        }

        function validate_required($keys){
            if(is_array($keys)){
                foreach($keys as $key) $this->validate_required($keys);
            }
            if(!isset($this->_call->$keys)) throw new iddiException($keys.' is required',self::ERROR_INVALID_INPUT);
        }



        //---- OUTPUT ----------------------------------------------------------

        function getResponse(){
            //Now run through all the call items to check for XSS
            try{
                $this->populate_call_data();
                $this->firewall();
                $this->check_security();
                $call_name=($_POST[self::CALL_INPUT_KEY])?$_POST[self::CALL_INPUT_KEY]:$_GET[self::CALL_INPUT_KEY];
                if(is_subclass_of($this, 'iddiApi_Result')){
                    die($this->output());
                }elseif($call_name==''){
                    throw new iddiException('No API call provided', self::ERROR_NO_CALL);
                }else{
                    if(method_exists($this,self::CALL_NAME_PREFIX.$call_name)){
                        $call_name=self::CALL_NAME_PREFIX.$call_name;
                        $event=$this->trigger(self::EVENT_BEFORE_API_CALL);
                        if(!$event->cancelled){
                            $this->$call_name();
                            $this->trigger(self::EVENT_AFTER_API_CALL);
                        }
                        //Things will have worked so lets remove the original call data
                        unset($this->_call);
                    }else{
                        throw new iddiException('API call '.get_class($this).'->'.$call_name.' not found',self::ERROR_UNKNOWN_CALL);
                    }
                    die($this->output());
                }
            }catch(IddiException $e){
                $this->success=false;
                $this->error=new Api_error($e->getCode(),$e->getMessage());
                if(iddi::is_devmode()){
                    $this->error->trace=$e->getTrace();
                    $this->error->additional_data=$e->additional_data;
                }
            }catch(Exception $e){
                $this->success=false;
                $this->error=new Api_error($e->getCode(),$e->getMessage());
                if(iddi::is_devmode()){
                    $this->error->trace=$e->getTrace();                    
                }
            }
            die($this->base_output());            
        }


        /**
         * Converts this object into the desired format
         * Available formats are currently:
         *   XML
         *   PHP serialised - ideal for passing calls between instances of iddi on different servers
         *   JSON
         * We would like to implement also XML-RPC and SOAP (although this might be a little long-winded!)
         * @return string The output result as a string
         */
        function output(){
            //Unless overridden we're going to handle output automatically
            //This uses a seperate method so that we can call it instead of the subclassed method
            //in the event of a security check failure and we don't want to even
            //attempt the subclassed output
            die($this->base_output());
        }
        
        function base_output(){
            $format=$_GET['format'];
            $out_object=$this;
            $event=$this->trigger(self::EVENT_BEFORE_API_OUTPUT);

            //Unset some values we don't want to send back out
            unset($this->login_required);
            unset($this->acceptable_privelleges);
            unset($this->auth_key_required);
            unset($this->_h);
            unset($this->_call);

            switch ($format){
                case 'xml':
                    header('Content-type: text/xml');
                    return $out_object->output_as_xml();
                    break;
                case 'php':
                    return $out_object->output_as_php();
                    break;
                case 'xmlrpc':
                    break;
                case 'soap':

                    break;
                default:
                    return $out_object->output_as_json();
                    break;
            }
        }

        private function output_as_xml(){
            $output="<result>".$this->convert_object_to_xml($this).'</result>';
            return $output;
        }

        private function convert_object_to_xml($object){
            foreach(get_object_vars($object) as $k=>$v){
                if(is_object($v)){
                    $output.="<$k>".$this->convert_object_to_xml($v)."</$k>";
                }elseif(is_array($v)){
                    $output.="<$k>".$this->convert_array_to_xml($v)."</$k>";
                }else{
                    $output.="<$k>".htmlentities($v)."</$k>";
                }
            }
            return $output;
        }

        private function convert_array_to_xml($array){
            foreach($array as $k=>$v){
                if(is_numeric($k)){
                    $attrs=" id=\"$k\"";
                    $k='value';
                }
                if(is_object($v)){
                    $output.="<$k$attrs>".$this->convert_object_to_xml($v)."</$k>";
                }elseif(is_array($v)){
                    $output.="<$k$attrs>".$this->convert_array_to_xml($v)."</$k>";
                }else{
                    $output.="<$k$attrs>$v</$k>";
                }
            }
            return $output;
        }

        private function output_as_json(){
            return json_encode($this);
        }

        private function output_as_php(){
            return serialize($this);
        }
    }

   