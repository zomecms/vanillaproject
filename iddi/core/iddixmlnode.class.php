<?php

/**
 * iddiXmlNode Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 * */
class iddiXmlNode extends iddiEvents {

    var $_node_id, $_pos, $nodename, $attributes, $children, $currentnode, $parent, $entityname;
    var $pchildren, $lineno, $_parent_path;
    var $datasource, $owner;
    var $prevent_output = false;

    /**
     * Override this to true if this node should be treated as void for html doctypes
     * Will be treated as self closing otherwise
     * @var type bool
     */
    var $is_void_html_tag = false;

    /**
     * Override this to true if this tag should self close on output. Also take note
     * of $is_void_html_tag
     * @var type bool
     */
    var $is_self_closing_tag = false;
    protected $_e;

    function __construct($owner = null, $tag = '', $parent = null, $attributes = null, $value = null) {
        if ($owner)
            $this->setowner($owner);
        if ($tag !== '')
            $this->setnodename($tag);
        if ($attributes)
            $this->setattributes($attributes);
        if ($parent)
            $parent->appendChild($this);
        if ($owner === $parent)
            $owner->documentelement = $this;
        if ($value)
            $this->setValue($value);
    }

    function __get($k) {
        switch ($k) {
            case 'value':
                return $this->getValue();
                break;
            default:
                return $this->$k;
        }
    }

    function __set($k, $v) {
        switch ($k) {
            case 'value': $this->setValue($v);
                break;
            default:
                $this->$k = $v;
        }
    }

    function destroy() {
        $this->owner = null;
        if ($this->children)
            foreach ($this->children as $child)
                $child->destroy();
        $this->children = array();
        $this->attributes = array();
        $this->pchildren = array();
        $this->parent = null;
    }

    function setowner($owner) {
        $this->owner = $owner;
        $this->_node_id = iddi::get_next_node_id();
    }

    function setnodename($name) {
        if (strpos($name, ':')) {
            $parts = explode(':', $name, 3);
            $this->nodename = $parts[2];
        } else {
            $this->nodename = $name;
        }
        //return $this;
    }

    function setup($owner, $tag, $parent, $attributes) {
        $this->setowner($owner);
        $this->setnodename($tag);
        $this->setattributes($attributes);
        if ($parent)
            $parent->appendChild($this);
        if ($owner == $parent)
            $owner->documentelement = $this;
        return $this;
    }

    function setvalue($value) {
        //$this->value=$value; return $this;
        $found = false;
        if ($this->children)
            foreach ($this->children as $child) {
                if ($child instanceof iddiCmlCData) {
                    $found = true;
                    $child->setValue($value);
                    break;
                }
            }

        if ($found == false)
            $n = new iddiXmlCData($this->owner, '#text#', $this, null, $value);
    }

    function getvalue() {
        //$this->value=$value; return $this;
        $found = false;
        if ($this->children)
            foreach ($this->children as $child) {
                if ($child instanceof iddixmlcdata) {
                    $output.=$child->getValue();
                }
            }
        return $output;
    }

    function pre_compile() {
        if (is_array($this->children))
            foreach ($this->children as $child)
                $child->pre_compile();
    }

    function _require_attribute($attr) {
        if (is_array($attr))
            foreach ($attr as $a)
                $this->_require_attribute($a);
        if ($this->attributes[strtoupper($attr)] === null)
            throw new iddiException('Attribute ' . $attr . ' missing on ' . $this->nodename . ' node', 'iddi.xml.' . $this->nodename . '.attributes.' . strtolower($attr) . '.missing');
    }

    /**
     * This is a big boy. Generates any json required for the editor to be able to do
     * things that aren't shown as part of the main page
     * @return string JSON for the editor
     */
    function get_editor_json($json = '') {
        if ($this->children)
            foreach ($this->children as $child)
                $json.=$child->get_editor_json();
        return $json;
    }

    function is_in_body(){
        return $this->parent->is_in_body();
    }
    
    function preparse() {
        if (is_array($this->children))
            foreach ($this->children as $child)
                $child->preparse();
    }

    function parse() {
        $this->processAVT();
        if (is_array($this->children))
            foreach ($this->children as $child)
                $child->parse();
    }

    /**
     * @desc Proccesses any attribute value templates
     */
    function processAVT() {
        $checkedfords = false;
        if (is_array($this->attributes)) {
            foreach ($this->attributes as $attrid => $attrvalue) {
                while (strstr($attrvalue, '{')) {
                    while (strstr($attrvalue, '}')) {
                        preg_match_all('/\{([^{}]*)\}/', $attrvalue, $matches, PREG_SET_ORDER);
                        foreach ($matches as $match) {
                            $xp = $match[1];

                            /**
                             * If we don't yet have a datasource see if we have one we can
                             * use, otherwise bail. This is effectively a lazy load
                             * of the datasource that only happens if we have any
                             * AVTs to process
                             */
                            if (!$checkedfords) {
                                $d = $this->getDataSource();
                                if (!$d)
                                    return;
                            }

                            if (method_exists($d, 'xpath')) {
                                $res = $d->xpath($xp, true);
                                if ($res) {
                                    $attrvalue = str_replace($match[0], $res->first()->value, $attrvalue);
                                    $changed = true;
                                }
                            } else {
                                echo $this->dump(1);
                                throw new iddiBreakpoint('test');
                            }
                        }
                    }
                }
                if ($changed)
                    $this->setAttribute($attrid, $attrvalue);
            }
        }
    }

    function processCurlyBrackets($data) {
        preg_match_all('/{([^\}]*)}/', $data, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $xp = $match[1];
            $res = $d->xpath($xp, true);
            if ($res) {
                $data = str_replace($match[0], $res->first()->value, $data);
            }
        }
        return $data;
    }

    function __toString() {
        return '';
    }
    
    function output($clean = false, $level = 0) {
        $childoutput = '';

        if (is_array($this->attributes))
            foreach ($this->attributes as $p => $v)
                if (strstr($p, ':') == null)
                    $atoutput.=' ' . strtolower($p) . '="' . trim($v) . '"';

        if (is_array($this->children))
            foreach ($this->children as $child)                
                $childoutput.=$child->output($clean, $level + 1);           

        if (!$this->preventoutput) {
            if ($this->is_void_html_tag) {
                if (iddiXmlDocument::$is_html)
                    return $indent . '<' . $this->nodename . $atoutput . '>' . "\n";
                else
                    return $indent . '<' . $this->nodename . $atoutput . '/>' . "\n";
            }

            if ($this->is_self_closing_tag)
                return $indent . '<' . $this->nodename . $atoutput . '/>' . "\n";


            //All other tags treated in full
            return $indent . '<' . $this->nodename . $atoutput . '>' . $nl . $indent . $childoutput . $nl . $indent . '</' . $this->nodename . ">\n";
        }
    }

    function compile() {
        //iddi::Log("Compile : Skipping node ".$this->nodename,0);
        if ($this->children)
            foreach ($this->children as $child)
                $child->compile();
    }

    function compile2(iddiTemplate $template) {
        if ($this->children)
            foreach ($this->children as $child)
                $child->compile2($template);
    }

    function addVariable($name, $value) {
        $this->vars[$name] = $value;
    }

    function getVariables() {
        return $this->vars;
    }

    function appendChild($child) {
        if (is_array($child) || $child instanceof iddiIterator) {
            foreach ($child as $c)
                $this->appendChild($c);
        } else {
            if ($child->_node_id === 0 || $child->_node_id === null)
                $child->_node_id = iddi::get_next_node_id();
            if (!isset($this->children[$child->_node_id])) {
                $child->owner = $this->owner;
                $this->children[$child->_node_id] = $child;
                if ($child->parent !== $this)
                    $child->setparent($this);
            }
        }
        //$this->appendNodesToArray($this->children,$child);
        return $child;
    }

    function removeChild($node) {
        unset($this->children[$node->_node_id]);
        $node->parentid = null;
    }

    /**
     * @desc Appends the child node after $afternode
     * @param iddiXmlNode $child The node to insert
     */
    function remove() {
        if ($this->parent)
            $this->parent->removeChild($this);
    }

    function appendChildAfter($child, $afternode) {
        $this->appendChildAt($child, $afternode, false);
        return $this;
    }

    /**
     * @desc Appends the child node before $beforenode
     */
    function appendChildBefore($child, $beforenode) {
        $this->appendChildAt($child, $beforenode, false);
        return $this;
    }

    private function appendChildAt($child, $nodepos, $before = false) {
        if (is_array($this->children)) {
            foreach ($this->children as $achild) {
                if ($achild == $nodepos && $before)
                    $this->appendNodesToArray($newchildarray, $child);
                $newchildarray[$achild->_node_id] = $achild;
                if ($achild == $nodepos && !$before)
                    $this->appendNodesToArray($newchildarray, $child);
            }
        }
        $this->children = $newchildarray;
        return $this;
    }

    private function appendNodesToArray($array, $nodes) {
        if (is_array($nodes) || $nodes instanceof iddiIterator) {
            foreach ($nodes as $tchild) {
                $array[$tchild->_node_id] = $tchild;
                $tchild->setParent($this);
            }
        } else {
            if (!isset($array[$nodes->_node_id])) {
                $array[$child->_node_id] = $nodes;
                //$nodes->setParent($this);
                if ($nodes->parentid)
                    $nodes->parent->removeChild($nodes);
                $nodes->parentid = $this->_node_id;
            }
        }
        return $this;
    }

    /**
     * @desc Replaces the node at $node with $newnode. $nodenode may also be a collection of nodes, in which case all the nodes will be inserted
     * @param IddiXmlNode $node The node to be replaced
     * @param IddiXmlNode $newnode The replacement node or node collection
     */
    function replaceNode($node, $newnode) {
        $this->appendChildAfter($newnode, $node);
        $this->removeChild($node);
        return $this;
    }

    /**
     * @desc Replaces this node with $node
     * @param iddiXmlNode $node The node to replace this one - can also be a collection or array of nodes
     */
    function replaceWith($node) {
        if ($this->parent)
            $this->parent->replaceNode($this, $node);
        return $node;
    }

    function changeContent($nodes) {
        if (is_array($nodes) || $nodes instanceof iddiIterator) {
            $this->children = $nodes;
        } else {
            $this->children = array();
            $this->appendChild($nodes);
        }
        return $this;
    }

    function clonenode($target, $deep = false) {
        $newnodeclass = get_class($this);
        $newnode = new $newnodeclass();

        $o = ($target->owner) ? $target->owner : $this->owner;

        //Copy the attributes
        $new_attributes = array();
        if (is_array($this->attributes))
            foreach ($this->attributes as $attr_name => $attr_value)
                $new_attributes[$attr_name] = $attr_value;

        //Create the node
        $newnode->setup($o, $this->nodename, $target, $new_attributes);
        //Go deep if requested
        if ($deep)
            if (is_array($this->children))
                foreach ($this->children as $child)
                    $newnode->appendchild($child->clonenode($newnode, true));

        return $newnode;
    }

    function setattributes($p) {
        $this->attributes = $p;
        if (is_array($p))
            foreach ($p as $paramname => $paramvalue) {
                $paramname = strtolower($paramname);
                $pnode = new iddiXmlAttributeNode($this->owner);
                $pnode->nodename = $paramname;
                $pnode->value = $paramvalue;
                $this->pchildren[$paramname] = $pnode;
            }
        return $this;
    }

    function getAttributes() {
        return $this->attributes;
    }

    function getAttribute($paramname) {
        return $this->attributes[strtoupper($paramname)];
    }

    function setAttribute($paramname, $paramvalue) {
        $this->attributes[strtoupper($paramname)] = $paramvalue;
        if (!isset($this->pchildren[$paramname])) {
            $paramname = strtolower($paramname);
            $pnode = new iddiXmlAttributeNode($this->owner);
            $pnode->nodename = $paramname;
            $pnode->value = $paramvalue;
            $this->pchildren[$paramname] = ($pnode);
        }
        return $this;
    }

    function setparent($p) {
        if ($this->parent != $p) {
            if ($this->parent)
                $this->parent->removeChild($this);
            $this->parent = $p;
            if ($this->parent)
                $this->parent->appendChild($this);
        }
        //return $this;
    }

    function addclass($class) {
        $this->attributes['CLASS'].=' ' . $class;
        return $this;
    }

    function setDataSource($d) {
        $this->datasource = $d;
        return $this;
    }

    function getDataSource() {
        if ($this->_datasource) {
            return $this->_datasource;
        } elseif ($this->datasource) {
            return $this->datasource;
        } else {
            if ($this->parent) {
                $d = $this->parent->getDataSource();
                $this->_datasource = $d;
                return $d;
            } else {
                $t = $this->owner->datasource;
                if ($t) {
                    return $t;
                } else {
                    return null;
                }
            }
        }
    }

    /**
     * @desc Gets the value of the fieldname passed in
     */
    function getFieldValue($field) {
        $field = strtolower($field);
        if ($field != '') {
            $d = $this->getdatasource();
            if ($d) {
                return $d->getValue($field);
            } else {
                $v = $this->owner->getDataSource()->$field;
                return $v;
            }
        }
    }

    function getdatasourcenode() {
        return ($this->datasource) ? $this : $this->parent->getdatasourcenode();
    }

    function setCurrentEntity($entity) {
        $this->_e = $entity;
        return $this;
    }

    function getCurrentEntity() {
        if ($this->_e) {
            return $this->_e;
        } else {
            if ($this->parent) {
                return $this->parent->getCurrentEntity();
            } else {
                // throw new iddiException('No Entity Found','iddi.xml.node.noentity');
                return null;
            }
        }
    }

    function getParentOfType($strnodetype) {
        if (is_a($this, $strnodetype)) {
            return $this;
        } else {
            if ($this->parent) {
                return $this->parent->getParentOfType($strnodetype);
            } else {
                return null;
            }
        }
    }

    function getMethod($method) {
        if (method_exists($this, $method)) {
            return $this;
        } else {
            if ($this->parent) {
                return $this->parent->getMethod($method);
            } else {
                return null;
            }
        }
    }

    /**
     * @desc Run an xpath query on this node. Normally this will only run on non-iddi nodes(i.e. just the html from the template), however you can elect to include iddi nodes in the query
     * @param string $xpath The xpath query to execute
     * @param bool $includeiddinamespace default false Set to true to include iddi nodes in the query
     * @return iddiXpathResultSet
     */
    function xpath($xpath, $includeiddinamespace = false, $resultset = null) {
        $xpath = iddiXpath::prepare($xpath);
        if ($resultset == null)
            $resultset = new iddiXpathResultSet();

        if (substr($xpath, 0, 1) == '/') {
            $b = explode('/', $xpath);
            $b1 = $b[1];
            if (substr_count($b1, '::') == 0)
                $xpath = 'self::' . substr($xpath, 1);
            else
                $xpath = substr($xpath, 1);
            /**
             * Check that we have an object to query on
             * Not having an object here is a symptom of some malformed xpath query
             */
            if (!is_object($this->owner) || !is_object($this->owner->documentelement)) {
                throw new iddiException('Malformed xPath (' . $xpath . ')', 'iddi.xmlnode.xpathquery.malformedxpath');
            }
            return $this->owner->documentelement->xpathinternal($xpath, $includeiddinamespace, $resultset);
        } else {
            return $this->xpathinternal($xpath, $includeiddinamespace, $resultset);
        }
    }

    function xpathinternal($strxpath, $includeiddinamespace = false, $resultset = null) {
        if ($resultset == null)
            $resultset = new iddiXpathResultSet();

        $xpath = new iddiXpath($strxpath);

        //Get the nodes to work on from the selected axis

        $axisdata = $this->getAxisData($xpath);

        //Process the nodes in the axis to see which ones match our selector and any predicates
        if ($xpath->functionname && $xpath->nodetest != 'node()') {
            $fn = $xpath->classfunctionname;
            if ($method_target = $this->getMethod($fn)) {
                $thisresultset = $method_target->$fn(explode(',', $xpath->functionparams), $xpath);
                //Process the items in the resultset
                if ($xpath->nextbit == '') {
                    //End of the line, so add to result set
                    $resultset->addResults($thisresultset);
                } else {
                    //More processing, so pass the xslt on
                    foreach ($thisresultset as $result) {
                        if (method_exists($result, 'xpath'))
                            $result->xpath($xpath->nextbit, $includeiddinamespace, $resultset);
                    }
                }
            }else {
                throw new iddiException("Function {$fn} Not supported on " . get_class($this), 'iddi.xml.functionnotsupported', $this);
            }
        } else {
            if (is_array($axisdata)) {
                foreach ($axisdata as $axisnode) {
                    if ($xpath->nodetest == $axisnode->nodename || $xpath->nodetest == '*' || $xpath->nodetest == 'node()') {
                        //Check the predicates
                        if ($xpath->nodepredicates == '' || $axisnode->processPredicates($xpath->nodepredicates)) {
                            if ($xpath->nextbit == '') {
                                if (!($axisnode instanceof iddiXmlCData)) {
                                    //End of the line, so add to result set
                                    $resultset->addResult($axisnode);
                                }
                            } else {
                                //More processing, so pass the xslt on
                                if (method_exists($axisnode, 'xpath'))
                                    $axisnode->xpath($xpath->nextbit, $includeiddinamespace, $resultset);
                            }
                        }
                    }
                }
            }
        }
        return $resultset;
    }

    function getAxisData($xpath) {
        if (substr($xpath->nodetest, 0, 1) == '$') {
            //Check for variable node substitutions
            $v = $this->getVariable(substr($xpath->nodetest, 1));
            if ($v)
                $axisdata = $v->data;
            $xpath->nodetest = 'node()';
        }else {
            switch ($xpath->axis) {
                case 'child':$axisdata = $this->getChildren();
                    break;
                case 'self':$axisdata[] = $this;
                    break;
                case 'parent':if ($this->parent != null)
                        $axisdata[] = $this->parent;
                    else
                        $axisdata[] = $this;
                    break;
                case 'attribute':$axisdata = $this->pchildren;
                    break;
                case 'descendant':$axisdata = $this->getDescendants();
                    break;
                case 'descendant-or-self': $axisdata = $this->getDescandantsOrSelf();
                    break;
                case 'ancestor': $axisdata = $this->getAncestors();
                    break;
                case 'ancestor-or-self': $axisdata = $this->getAncestorsOrSelf();
                    break;
                case 'following-preceding': throw new iddiException('XPath Axis ' . $thisaxis . ' not supported', 'iddi.xpath.unsupportedaxis', $this);
                    break;
                case 'preceeding-sibling': throw new iddiException('XPath Axis ' . $thisaxis . ' not supported', 'iddi.xpath.unsupportedaxis', $this);
                    break;
                case 'following': throw new iddiException('XPath Axis ' . $thisaxis . ' not supported', 'iddi.xpath.unsupportedaxis', $this);
                    break;
                case 'preceeding': throw new iddiException('XPath Axis ' . $thisaxis . ' not supported', 'iddi.xpath.unsupportedaxis', $this);
                    break;
                default: throw new iddiException('XPath Axis ' . $thisaxis . ' not supported', 'iddi.xpath.unsupportedaxis', $this);
            }
        }
        return $axisdata;
    }

    function processPredicates($predicates) {
        preg_match_all('/([A-Za-z0-9:\/\(\)]*?)[^A-Za-z0-9!=&;]*(=|!=|&gt;|&lt;)+[^\'A-Za-z0-9\-]*([\'A-Za-z0-9\-]*)(AND|OR)*/', $predicates, $matches, PREG_SET_ORDER);

        //            print_rp($matches);

        $eval = '';
        foreach ($matches as &$match) {
            $xpath = $match[1];
            if (substr_count($xpath, '::') > 0) {
                $vals = $this->xpath($xpath);
                if ($vals) {
                    foreach ($vals as $val) {
                        $match[1] = '\'' . $val->value . '\'';
                    }
                }
            }

            //print_rp($match);

            switch ($match[2]) {
                case '=':if ($match[1] == $match[3])
                        return true;
                    break;
            }
        }

        return false;
    }

    function getChildren() {
        return $this->children;
    }

    function getDescendants(&$r = null) {
        if (is_array($this->children)) {
            foreach ($this->children as $child) {
                $r[] = $child;
                $child->getDescendants($r);
            }
        }
        return $r;
    }

    function getDescandantsOrSelf() {
        $r[] = $this;
        $this->getDescendants($r);
        return $r;
    }

    function getAncestors(&$r = null) {
        if ($this->parentid > 0) {
            $r[] = $this->parent;
            $this->parent->getAncestors($r);
        } else {
            $r[] = $this;
        }
        return $r;
    }

    function getAncestorsOrSelf() {
        $r[] = $this;
        $this->getAncestors($r);
        return $r;
    }

    function getVariable($name) {
        if (!isset($this->vars[$name])) {
            if ($this->parent) {
                return $this->parent->getVariable($name);
            } else {
                return '';
            }
        } else {
            return $this->vars[$name];
        }
    }

    /**
     * @desc Resolves the values of parameters passed into a parameter
     */
    function resolveValue($value, $depth = 0) {
        $value = str_replace("\'", '\0xq', $value);
        $v = explode("'", $value);
        //Even number results need to be resolved and passed back to here for resolving again
        //Odd number values are treated as strings
        //Even numbers that begin with a number are treated as literals and not resolved
        $even = true;
        foreach ($v as $vpos => $v1) {
            if ($even) {
                $l = substr($v1, 0, 1);
                if (!is_numeric($l)) {
                    $result = $this->xpath($v1);
                    if ($result) {
                        //Just use the first result
                        $v[$vpos] = $result->first()->value;
                    } else {
                        unset($v[$vpos]);
                    }
                }
                $even = false;
            } else {
                $even = true;
            }
        }
        //join the string back together and put any escaped quotes back in
        $out = str_replace('\0xq', "'", implode($v));
        return $out;
    }

    function xmlFunction_methodtest() {
        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = 'This is a test';
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_current() {
        $r = new iddiXpathResultSet();
        $r->addResult(iddiRequest::getMainRequest()->page);
        return $r;
    }

    function xmlFunction_urlencode($params) {
        $value_path = $params[0];
        $input = $this->xpath($value_path)->first()->value;
        return iddiXpathResultSet::quickResult('result', urlencode($input));
    }

    function xmlFunction_pathfragments($params) {


        $xpath = $params[0];
        $fragments = $params[1];
        $xpath = str_replace('#', '()/', $xpath);
        $x = $this->xpath($xpath)->first();
        $v = $x->getvalue();
        $vparts = explode('/', $v);
        //Remove language
        if (file_exists(IDDI_PROJECT_PATH . '/languages/' . $vparts[1] . '.xml'))
            $fragments++;
        for ($a = $fragments + 1; $a <= sizeof($vparts); $a++)
            unset($vparts[$a]);
        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = implode('/', $vparts);
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_name() {
        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = $this->nodename;
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_text() {
        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = $this->value;
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_currententity() {
        $r = new iddiXpathResultSet();
        $r->addResult($this->getDataSource());
        return $r;
    }

    function xmlFunction_position() {
        if ($this->_pos) {
            $r = new iddiXpathResultSet();
            $x = new iddiXmlNode();
            $x->value = $this->_pos;
            $r->addResult($x);
            return $r;
        } else {
            if ($this->parent) {
                return $this->parent->xmlFunction_position();
            } else {
                $r = new iddiXpathResultSet();
                $x = new iddiXmlNode();
                $x->value = 0;
                $r->addResult($x);
                return $r;
            }
        }
    }

    function xmlFunction_post() {
        return iddiRequest::$form;
    }

    function xmlFunction_document($params, iddiXpath $xpath) {
        $r = new iddiXpathResultSet();
        $fn = str_replace("'", '', implode('', $params));
        $fn = preg_replace('/[^A-Za-z0-9 .=-]/', '', $fn);
        $fn = str_replace(' ', '_', $fn);
        $path = IDDI_FILE_PATH . '../iddi-project/rss_cache/' . $fn;

        iddiDebug::dump('Loading document ' . $path, $this);

        if (!file_exists($path)) {
//                 throw new iddiException("Document $path Not Found","iddi.page.xpath.document.documentnotfound",$this);
        } else {

            /*
              We know that any xpath after this function will be acting on this document. For that reason we can cheat a bit here
              and speed things up by running the xpath directly on the document and then return the results from that.
              We can also cache any xpath query results
             */


            $dirname = base64_encode('documents');
            $fname = base64_encode($path . $xpath->nextbit);
            @mkdir('xpathcache');
            @chmod('xpathcache', 0777);
            @mkdir('xpathcache/' . $dirname);
            @chmod('xpathcache/' . $dirname, 0777);
            if (!file_exists('xpathcache/' . $dirname . '/' . $fname)) {
                $x = new iddiXmlDocument();
                $t = microtime();
                $x->filteredLoad($path, $xpath->nextbit);
                foreach ($x->children as $child) {
                    $r->addResult($child);
                }

                $s = serialize($r);
                $fp = fopen('xpathcache/' . $dirname . '/' . $fname, 'w');
                fwrite($fp, $s);
                fclose($fp);
            } else {
                $d = file_get_contents('xpathcache/' . $dirname . '/' . $fname);
                $r = unserialize($d);
            }
            //$r->addResult($x->documentelement);
            /*
              $r=new iddiXpathResultSet();
              $x=new iddiXmlDocument();
              $t=microtime();
              $x->filteredLoad($path,$xpath->nextbit);
              foreach($x->children as $child){
              $r->addResult($child);
              }
              $xpath->nextbit='';
              return $r;
             */
        }
        $xpath->nextbit = '';
        return $r;
    }

    function xmlFunction_requestvar($params) {
        $p = $this->processCurlyBrackets($params[0]);

        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = $_REQUEST[$p];
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_postvar($params) {
        $p = $this->processCurlyBrackets($params[0]);

        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = $_POST[$p];
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_postvarchecked($params) {
        $p = $this->processCurlyBrackets($params[0]);

        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = ($_POST[$p]) ? 'checked' : 'false';
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_sessionvar($params) {
        $p = $this->processCurlyBrackets($params[0]);

        $r = new iddiXpathResultSet();
        $x = new iddiXmlNode();
        $x->value = $_SESSION[$p];
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_entity($params) {
        //$entid=$this->processCurlyBrackets($params[0]);
        $entid = $this->resolveValue($params[0]);
        $r = new iddiXpathResultSet();
        $x = new iddiEntity();
        if ($entid > 0)
            $x->loadById('', $entid);
        $r->addResult($x);

        return $r;
    }

    function xmlFunction_format($params) {
        print_rp($params);
        die();
    }

    function xmlFunction_findtoplevel($stopbefore) {
        //Find the top page id
        $top_page_id = $this->findtoplevel($this->getDataSource()->id, $stopbefore, $this->getDataSource()->id, $level);
        //Load it
        $ds = new iddiEntity();
        $ds->loadById('', $top_page_id);
        //Create a result set and add the entity, then return the result set from the function
        $r = new iddiXpathResultSet();
        $r->addResult($ds);
        return $r;
    }

    function xmlFunction_findtoplevelid($stopbefore) {
        //Find the top page id
        $top_page_id = $this->findtoplevel($this->getDataSource()->id, $stopbefore, $this->getDataSource()->id, $level);
        //Create a result set
        $r = new iddiXpathResultSet();
        //Creata a node to add to the result set, and set it's value to top_page_id
        $x = new iddiXmlNode();
        $x->value = $top_page_id;
        $r->addResult($x);
        return $r;
    }

    function xmlFunction_tidy($text) {
        if (is_array($text))
            $text = $text[0];
        return $this->_return_text(strtolower(str_replace('--', '-', str_replace('--', '-', preg_replace('/[^A-Za-z0-9]/', '-', $text)))));
    }

    function xmlFunction_date($params) {
        require_once(IDDI_ROOT . '/iddi/date-functions.lib.php');
        $format = $params[0];
        if ($format == '')
            $format = 'jS F Y';
        $value_path = $params[1];
        $value = $this->xpath($value_path)->first()->value;
        return iddiXpathResultSet::quickResult('date', date_lang($format, strtotime($value)));
    }

    function xmlFunction_add($params) {
        $val_one = $params[0];
        $val_two = $params[1];
        return iddiXpathResultSet::quickResult('result', $val_one + $val_two);
    }

    function xmlFunction_number_format($params) {
        $value = $params[0];
        //$value=$this->xpath($value_path)->first()->value;
        //$value=preg_replace('/[^0-9\.]*/','',$value);

        if (trim($value) == '' || $value == null)
            $value = 0;
        $dp = $params[1];
        $comma = ',';
        $dot = '.';
        $l = iddiRequest::$current->renderlanguage;
        if ($l == 'de' || $l == 'fr') {
            $comma = '.';
            $dot = ',';
            $pf = '&nbsp;';
        }
        if ($l == 'pt') {
            $comma = '.';
            $dot = ',';
            $pf = '';
        }
        if ($l == 'es') {
            $comma = '.';
            $dot = ',';
            $pf = '';
        }
        if ($l == 'fr') {
            $comma = ' ';
            $dot = ',';
            $pf = ' ';
        }

        $num = number_format($value, $dp, $dot, $comma);
        if ($num == '')
            $num = '0';
        return iddiXpathResultSet::quickResult('result', $pf . strval($num));
    }

    function xmlFunction_config($params) {
        $section = $params[0];
        $value = $params[1];
        return iddiXpathResultSet::quickResult('result', iddiConfig::GetValue($section, $value));
    }
    
    function xmlFunction_up($params) {
        $up = $this->getDataSource();
        
        return iddiXpathResultSet::quickResult('result', $up);
    }    

    function _return_text($text) {
        //Create a result set
        $r = new iddiXpathResultSet();
        //Creata a node to add to the result set, and set it's value to top_page_id
        $x = new iddiXmlNode();
        $x->value = $text;
        $r->addResult($x);
        return $r;
    }

    function findtoplevel($pageid, $stopbefore, $previouspageid, $level = 0) {
        //if ($pageid=='') throw new iddiCodingException('No Entity ID Provided','iddi.xml.List-Subpages.NoEntityId');
        $language = iddiRequest::$current->language;
        $baselanguage = iddiRequest::$current->baselanguage;
        $this->_parent_path[$pageid] = $pageid;
        if ($level == 0) {
            $this->_parent_path = array();
        }
        if ($level > 10 && $pageid > 0) {
            return $pageid;
        } else {
            $sql = 'SELECT parentid,virtualfilename,rootlanguageitemid FROM {PREFIX}sysfilenames WHERE id=' . $pageid;
            $rs = iddiMySql::query($sql);
            if ($rs->HasData()) {
                foreach ($rs as $row) {
                    if ($row->virtualfilename == $stopbefore) {
                        return $previouspageid;
                    } else {
                        $parentid = $row->parentid;
                        //if ($row->rootlanguageitemid>0) $parentid=$row->rootlanguageitemid;
                        return $this->findtoplevel($parentid, $stopbefore, $pageid,  ++$level);
                    }
                    break;
                }
            } else {
                return $previouspageid;
            }
        }
    }

    //---- DEBUGGING

    function dumpNodes($maxdepth, $depth = 0) {
        if ($depth == 0)
            $output.="\n---- Dump Nodes";
        $pad = str_repeat(' ', $depth * 2);
        $pad1 = str_repeat(' ', $depth * 2 + 1);
        $output.="\n" . $pad . '[' . $this->nodename . ']';
        if ($this->attributes) {
            foreach ($this->attributes as $attr_name => $attr_value) {
                $output.=" @{$attr_name}=\"{$attr_value}\"";
            }
        }
        if ($this->value)
            $output.=" = \"{$this->value}\"";
        $output.=' ds:' . $this->getDataSource()->id;
        if ($depth < $maxdepth) {
            if (sizeof($this->children) > 0) {
                foreach ($this->children as $child) {
                    $output.=$child->dumpnodes($maxdepth, $depth + 1);
                }
            }
        }
        return $output;
    }

}
