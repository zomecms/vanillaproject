<?php
 
/**
 * Core IDDI API functionality
 *
 * @author Tastic Multimedia
 * @copyright Tastic Multimedia 2010
 * @package IDDI
 */


// Change password

class iddiRequest_api_changepassword_change extends Api_Result{

    function getResponse(){
        $user_id = $_SESSION['userid'];
        $password = sha1($_POST['password']);
        $newpassword = sha1($_POST['newpassword']);

        //update DB replace old password
        $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$user_id and password='$password'");
        if($result->hasData){
            $this->message="Incorrect username or password";
            $this->success=false;
        }else{
            iddiMySQL::query("UPDATE {PREFIX}users SET password='$newpassword' where id=$user_id");
            $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$user_id and password='$newpassword'");
            if($result->hasData){
                $this->message="Congratulations You have changed your password";
                $this->success=true;
            }else{
                $this->message="Password was not changed - technical fault";
                $this->success=false;
            }
        }
        die($this->output());
    }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Plugin Mager//////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//list all plugins
class iddiRequest_api_list_installed_plugins extends Api_result{
     function getResponse(){
         $sql="SELECT id,name FROM {PREFIX}sys_plugins";
         $result=iddiMySql::Query($sql);

         foreach($result as $plugin){
             $p=new stdClass();
             $p->id=$plugin->id;
             $p->name=$plugin->name;
             $this->plugins[]=$p;
         }
         die($this->output());
     }
 }

//display the information of a chosen plugin
class iddiRequest_api_get_plugin_info extends Api_Result {

    function getResponse() {
         $sql="SELECT * FROM {PREFIX}sys_plugins WHERE id={$_POST['plugin_id']}";
         $result=iddiMySql::Query($sql);

         foreach($result as $plugin){
             $p=new stdClass();
             $p->id=$plugin->id;
             $p->name=$plugin->name;
             $sql="SELECT name FROM {PREFIX}author WHERE id={$plugin->author_id}";
             $result2=iddiMySql::Query($sql);
             //echo($result2->name);
             foreach($result2 as $author_name){
                $p->author=$author_name->name;
             }
             $p->enabled=$plugin->enabled;
             $p->description=$plugin->description;
             $p->price=number_format($plugin->price,2);
             $p->trial_period=$plugin->trial_period;
             $this->plugins[]=$p;
         }
         die($this->output());
    }
}

//change plugin enabled value in the db
class iddiRequest_api_enable_change extends Api_Result {

    function getResponse() {
        $sql="SELECT * FROM iddi_sys_plugins WHERE id={$_POST['plugin_id']}";
        $result = iddiMySql::Query($sql);
        $result = mysql_query($sql);

         if (!$result){
             echo "could not successfully run query ($sql) from DB: ".mysql_error();
             $this->success=false;
             exit;
         }
      
         if (mysql_num_rows($result)==0) {
             echo('no rows found');
             $this->success=false;
             exit;
         }

         while (mysql_fetch_assoc($result)){
            $sql="UPDATE {PREFIX}sys_plugins SET enabled={$_POST['enable']} WHERE id={$_POST['plugin_id']}";
            $result=iddiMySql::Query($sql);
            $this->success=true;
         }

        $this->list_enabled_plugins();
        die($this->output());        
    }
    
    function list_enabled_plugins(){
        $sql="SELECT name FROM iddi_sys_plugins WHERE enabled=1";
        $result = mysql_query($sql);

        if (!$result){
            echo "could not successfully run query ($sql) from DB: ".mysql_error();
            $this->success=false;
            exit;
        }

        if (mysql_num_rows($result)==0) {
            echo('no rows found');
            $this->success=false;
            exit;
        }

        $plugins = "W:\clients_iddi\cbc001.ncf10\www\iddi-project\config\plugins.txt";
        $fh = fopen($plugins,'w');

        //each enabled plugin name to be written on a new line in plugin.txt
        while ($row = mysql_fetch_assoc($result)){
            fwrite($fh,$row["name"]."\r\n");
        }       
        
        fclose($fh);
    }
}


// -------------------User Manager-----------------------

// User List--------------
class iddiRequest_api_get_user_data extends Api_Result{

        function getResponse(){

// get user id + name (as array)

        $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$user_id and firstname='$firstname' and lastname='$lastname'");
        if($result->hasData){
            $this->message="No users in database!";
            $this->success=false;
        }else{
         $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$user_id and firstname='$firstname' and lastname='$lastname'");
        if($result->hasData){
            $this->message="Poco Users List...";
            $this->success=true;
        }else{
             $this->message="List Not Built - technical fault";
             $this->success=false;
        }
     }
     die($this->output());
    }
}

//ger user info
class iddiRequest_api_get_user_info extends Api_Result {

    function getResponse() {
        try{

            $this->user=new iddiEntity('user',$_POST['user_id']);

            //Make sure the correct user has loaded
            if($this->user->id==$_POST['user_id']) {
                $this->success=true;
            }else{
                $this->success=false;
                $this->message='User not found';
            }
        }catch(Exception $e){
            $this->success=false;
            $this->message=$e->getMessage();
        }
        die($this->output());
    }
}

// after get_permissions list
class iddiRequest_api_after_get_permissions extends Api_Result{

        function getResponse(){
        $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$permissions_id and permission_name='$permission_name'");
        if($result->hasData){
            $this->message="No permissions in database!";
            $this->success=false;
        }else{
         $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$permissions_id and permission_name='$permission_name'");
        if($result->hasData){
            $this->message="Poco permissions List...";
            $this->success=true;
        }else{
             $this->message="Permissions List Not Built - technical fault";
             $this->success=false;
           }
        }
      die($this->output());
    }
 }


 class iddiRequest_api_get_users extends Api_result{
     function getResponse(){
         try{
             $sql="SELECT id,firstname,lastname FROM {PREFIX}user";
             $result=iddiMySql::Query($sql);
             foreach($result as $user){
                 $u=new stdClass();
                 $u->id=$user->id;
                 $u->value=$user->firstname.' '.$user->lastname;
                 $this->list[]=$u;
             }
             $this->success=true;
         }catch(Exception $e){
             $this->success=false;
             $this->message=$e->getMessage();
         }
         die($this->output());
     }
 }

// remove loader

// update user list
 class iddiRequest_api_update_user extends Api_Result{

     function getResponse(){
            iddiMySQL::query("UPDATE {PREFIX}users SET id=$user_id and name='$name'");
            $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$user_id and name='$name'");
            if($result->hasData){
                $this->message="User added!";
                $this->success=true;
            }else{
                $this->message="User Not Added! - technical fault";
                $this->success=false;

            }
        die($this->output());
     }
 }

// Get permisions------------

// add loader to permissions list
 
// get permissions
  class iddiRequest_api_get_permissimons extends Api_Result{

        function getResponse(){
        $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$user_id and permission_name='$permission_name'");
        if($result->hasData){
            $this->message="No permissions for this user!";
            $this->success=false;
        }else{
         $result = iddiMySql::query("SELECT id FROM {PREFIX}users WHERE id=$user_id and permission_name='$permission_name'");
        if($result->hasData){
            $this->message="Poco permissions List for [$user_id]";
            $this->success=true;
        }else{
             $this->message="Permissions Not Found - technical fault";
             $this->success=false;
             }
          }
             die($this->output());
        }
   }
// remove loader

// update list


//save---------------------
  class iddiRequest_api_save_user extends Api_Result {

      function getResponse() {

          try {
              if($_POST['user_id']>0){
                $this->user=new iddiEntity('user',$_POST['user_id']);
              }else{
                  $this->user=new iddiEntity();
                  $this->user->id=-1;
                  $this->user->entityname='user';
                  $this->user->virtualfilename='/users/'.$_POST['username'];
                  $this->user->template='templates/user_details.iddi';
                  $this->user->language='en-gb';
              }

              //Make sure the correct user has loaded
              if($this->user->id==$_POST['user_id']) {
                  $this->success=true;
                  $this->user->username=$_POST['user_name'];
                  if($_POST['password']!='') $this->user->password=sha1($_POST['password']);
                  $this->user->firstname=$_POST['firstname'];
                  $this->user->lastname=$_POST['lastname'];
                  $this->user->email=$_POST['email'];
                  $this->user->phone=$_POST['phone'];
                  $output=$this->user->save();
                  $this->success=true;
                  $this->message='User saved successfully';
              }else {
                  $this->success=false;
                  $this->message='User not found';
              }

          }catch(Exception $e) {
              $this->success=false;
              $this->message=$e->getMessage();
          }
          die($this->output());

      }
  }

//delete user-------------------

class iddiRequest_api_delete_user extends Api_Result {

    function getResponse() {

        if($_POST['user_id']>0) {
            $this->user=new iddiEntity('user',$_POST['user_id']);
            $this->user->deleted=1;
            $this->user->password='';
            $this->user->save();
            $this->success=true;
            $this->message='User deleted successfully';
        }else {
            $this->success=false;
            $this->message='User not deleted';
        }
        die($this->output());

    }
}

// -------------------Entity Manager-----------------------

//----Entity Management---------------------------------------------------------

class iddiRequest_api_entity extends Api_result{
    var $login_required=false;

    const ERROR_FILE_NOT_FOUND='iddi.api.entity.file_not_found';
    
    function call_get(){
        //Check input
        $this->validate_required('entity_id');
        $this->validate_numbers('entity_id');
        $this->validate_strings('entity_name');

        $this->item=new iddiEntity($this->_call->entity_name, $this->_call->entity_id);
    }

    function call_get_by_filename(){
        $this->validate_required('filename');
        $this->validate_strings('filename');

        $this->item=new iddiEntity();
        $this->item->loadByVirtualFilename($this->_call->filename);

        if($this->item->virtualfilename!=$this->_call->filename){
            unset($this->item);
            throw new iddiException($this->_call->filename.' not found',self::ERROR_FILE_NOT_FOUND);
        }
    }

    function call_get_children(){
        $this->validate_required('entity_id');
        $this->validate_numbers('entity_id');
        $items=iddiMySql::get_child_entity_list($this->_call->entity_id,$this->_call->include_deleted);
        $this->item_count=0;
        $this->items=array();
        foreach($items as $item){
            $this->items[]=$item->rawDbData;
            $this->item_count++;
        }
    }

    function call_count_children(){
        $this->validate_required('entity_id');
        $this->validate_numbers('entity_id');
        $this->count=iddiMySql::count_children($this->_call->entity_id);
    }

    function call_save(){

    }

    function call_rename(){
        
    }

    function call_delete(){

    }

    function call_get_definition(){
        $this->validate_required('entity');
        $this->entity_def=iddiEntityDefinition::GetEntity($this->_call->entity);
    }

}

class iddiRequest_api_list_items extends Api_result{
    function call_get(){
        $this->validate_required('entity');
        $sql=$this->_build_sql();
        $rs=iddiMySql::query($sql);
        foreach($rs as $record){
            $this->records[]=$record->dbfields;
        }
    }

    function call_search(){
        $this->validate_required('entity');
        $this->validate_required('search');
        $this->validate_required('field');
        $sql='SELECT id,`'.$this->_call->field.'` FROM `{PREFIX}'.$this->_call->entity.'` WHERE `'.$this->_call->field.'` like \''.$this->_call->search.'%\'';
        $this->sql=$sql;
        $rs=iddiMySql::query($sql);
        foreach($rs as $record){
            $this->records[]=$record->dbfields;
        }
    }


    function call_get_ids(){
        $this->validate_required('entity');
    }

    function _build_sql(){
        $sql='SELECT ';
        //Fieldlist
        if($this->_call->fields){
            $comma='';
            foreach($this->_call->fields as $field){
                if(substr_count('`',$field)>0) throw new iddiException('SQL Injection Detected','iddi.api.sql_injection.at_fields');
                $sql.=$comma.'`'.$field.'`';
                $comma=', ';
            }
        }else{
            $sql.='*';
        }

        //Tables
        $sql.=' FROM ';
        $sql.='`{PREFIX}'.$this->_call->entity.'`';
        if(substr_count('`',$this->_call->entity)>0) throw new iddiException('SQL Injection Detected','iddi.api.sql_injection.at_table');

        //Joins

        //Where
        if($this->_call->limit){
            $comma='';
            foreach($this->_call->limit as $limit){
                $sql.=$comma.$limit;
            }
        }

        //Group

        //Order

        //Limit
        $this->sql=$sql;
        return $sql;
    }

    function _get_items($entity,$fieldlist='*'){

    }

}
